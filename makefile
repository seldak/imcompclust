all:
	for dir in src misc; do \
		make -C $$dir all; \
	done

clean:
	for dir in src misc; do \
		make -C $$dir clean; \
	done

