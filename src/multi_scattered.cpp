/*
 * MIT License
 *
 * Copyright (c) 2015 Sameh Samir Hassan
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "enums.h"
#include "pos_2d.h"
#include "edge.h"
#include "compression_block.h"
#include "algo.h"
#include "aut.h"
#include "quadtree.h"
#include "scattered.h"
#include "multi_scattered.h"
#include <bitset>
#include <opencv2/highgui/highgui.hpp>
#include <boost/dynamic_bitset.hpp>

void ca_multi_scattered::pre_process_filter( )
{
	for( auto image : mats ) {
		image->pre_process_filter( );
	}
}

void ca_multi_scattered::find_best_scale_and_initialize_grid( )
{
	std::cerr << __PRETTY_FUNCTION__ << std::endl;

	assert( best_scale > 0 );

	for( auto image : mats ) {
		create_grid_for_scale( *image, image->square_grid, best_scale );
	}
}

void ca_multi_scattered::initialize_compression_blocks( )
{
	unsigned int x, y;

	std::cerr << __PRETTY_FUNCTION__ << std::endl;

	compression_blocks.clear( );
	std::list< compression_block * >::iterator cit;

	std::cout << " there are " << mats.size( ) << " images" << std::endl;

	for( auto image : mats ) {
		for( y = 0; y != image->square_grid.size( ); y++ ) {
			for( x = 0; x != image->square_grid[y].size( ); x++ ) {
				compression_block *cb = new compression_block( &image->mat, image->square_grid[y][x],
						image->verification_matrix );

				compression_blocks.push_back( cb );
				image->square_grid[y][x]->set_container( static_cast< void* >( cb ) );
			}
		}
		std::cout << "There are " << compression_blocks.size( ) << " unique blocks" << std::endl;
	}
}

std::string ca_multi_scattered::write_square_to_cb_map( )
{
	std::string map = "";
	std::vector< uint8_t > map_vector;
	unsigned int num_of_bits = ceil( log2( compression_blocks.size( ) ) );
	std::cout << __func__ << " num of bits" << num_of_bits << std::endl;

	assert( type == AUT_SCATTERED );
	for( auto it : compression_blocks ) {
		it->sort_squares( );
	}
	compression_blocks.sort( compression_block::less_blocks );

	int h = 0;
	for( auto image : mats ) {
		int i = 0;
		for( auto it : compression_blocks ) {
			it->set_order( i++ );
		}

		unsigned int max_span = quadtree::get_max_quadtree_span( best_scale, image->mat.rows, image->mat.cols );
		pos_2d pos;
		pos.y = pos.x = 0;
		quadtree *root = new quadtree( pos, max_span );
		root->initialize_tree_down_to_scale( image->square_grid, best_scale, image->mat.rows, image->mat.cols );
		quadtree::attempt_node_combining( root, quadtree::cb_check, quadtree::combine_nodes );
		cv::Mat new_mat = image->cv_mat.clone( );
		quadtree::draw_quadtree_node( new_mat, root );
		unsigned int qtree_length = root->count_bits_required_for_quadtree( num_of_bits );
		print_length( "length of qtree ", qtree_length );
		char buf[100];
		sprintf( buf, "CB Quadtree %d", h );
		cv::namedWindow( buf, CV_WINDOW_NORMAL );
		cv::imshow( buf, new_mat );
		h++;

		unsigned int blocks = 0;
		unsigned int x, y;
		for( y = 0; y != image->square_grid.size( ); y++ ) {
			for( x = 0; x != image->square_grid[y].size( ); x++ ) {
				compression_block *cb =
						static_cast< compression_block * >( image->square_grid[y][x]->get_container( ) );
				boost::dynamic_bitset< > bset( num_of_bits, cb->get_order( ) );
				map_vector.push_back( bset.to_ulong( ) );
				std::string str;
				to_string( bset, str );
				std::cout << str << " ";
				map += str;
				blocks++;
			}
			std::cout << std::endl;
		}
		std::cout << "blocks = " << blocks << std::endl;
		print_length( "map length = ", map.length( ) );
		std::string alternative_map = pmf::compress_huffman( map_vector );
		print_length( "alternative map length ", alternative_map.length( ) );

		if( map.length( ) > alternative_map.length( ) ) {
			std::cout << "original map length is bigger, picking alternative " << std::endl;
			compressed_length += alternative_map.length( );
		} else {
			std::cout << "original map length is smaller, staying with it " << std::endl;
			compressed_length += map.length( );
		}
	}

	cv::waitKey( 0 );

	return map;
}

void ca_multi_scattered::verify( )
{
	size_t number_of_blocks = 0;
	for( std::list< compression_block * >::iterator cbs_it = compression_blocks.begin( );
			cbs_it != compression_blocks.end( ); ++cbs_it ) {
		( *cbs_it )->verify( );
		number_of_blocks += ( *cbs_it )->get_number_of_blocks( );
	}

	size_t cb_number_of_blocks = 0;
	for( auto image : mats ) {
		std::cout << "total number of squares = " << number_of_blocks << ", should be "
				<< image->mat.rows * image->mat.cols / ( best_scale * best_scale ) << std::endl;
		cb_number_of_blocks += ( image->mat.rows / best_scale * image->mat.cols / best_scale );

		image->verify_cb_traversing( );

		std::cout << "number of compression blocks = " << compression_blocks.size( ) << " (down from "
				<< image->mat.rows * image->mat.cols / ( best_scale * best_scale ) << ")" << std::endl;
	}
	assert( number_of_blocks == cb_number_of_blocks );
}

void ca_multi_scattered::encode( )
{
	std::string dict_string;
	std::string cb_compressed_output;
	std::vector< uint8_t > dictionary_vector;
	std::vector< bool > huffman_nodes;

	compressed_length = 0;
	compressed_output += write_square_to_cb_map( );

	for( auto it : compression_blocks ) {

		std::cout << *it << std::endl;

		std::cout << "Number of square blocks in this sub-image " << it->get_number_of_blocks( ) << std::endl;
		if( coding == GOLOMB_RICE ) {
			unsigned int M;
			M = pow( 2, it->get_k( ) );
			std::cout << __func__ << " K = " << it->get_k( ) << std::endl;
			std::bitset< 3 > bset( it->get_k( ) );
			cb_compressed_output += bset.to_string( );
			std::cout << "M = " << M << std::endl;
			std::cout << __func__ << " K(bits) = " << bset.to_string( ) << std::endl;
//			stream << bset.to_string( ) << std::endl;
			cb_compressed_output += it->cb_compress_golomb_rice( M );
		} else if( coding == EXP_GOLOMB ) {

			std::cout << __func__ << " K = " << it->get_k( ) << std::endl;
			std::bitset< 3 > bset( it->get_k( ) );
			cb_compressed_output += bset.to_string( );
			std::cout << __func__ << " K(bits) = " << bset.to_string( ) << std::endl;
//			stream << bset.to_string( ) << std::endl;
			cb_compressed_output += it->cb_compress_exponential_golomb( it->get_k( ) );
		} else if( coding == HUFFMAN ) {
			cb_compressed_output += it->cb_compress_huffman( ( separate_dict_compression ) ? false : true );

			if( separate_dict_compression ) {
				it->write_huffman_tree( false );
				it->append_huffman_symbols_to_vector( dictionary_vector );
				it->append_huffman_nodes_to_vector( huffman_nodes );
			}
		}

//		stream.close( );
	}
	compressed_output += cb_compressed_output;
	compressed_length += cb_compressed_output.length( );

	if( separate_dict_compression && coding == HUFFMAN ) {
		std::cout << "num of values in dictionary vector = " << dictionary_vector.size( ) << std::endl;

		pmf temp_t = pmf( );
		temp_t.compute_pmf( dictionary_vector );
		std::cout << temp_t << std::endl;
		dict_string = compression_algorithm_under_test::encode_huffman_symbols( huffman_nodes );
		dict_string += temp_t.compress_huffman( dictionary_vector );
		compressed_length += dict_string.length( );
		compressed_output += dict_string;
	}

	print_length( "length of compressed output = ", cb_compressed_output.length( ) );
	print_length( "compressed dict length = ", dict_string.length( ) );
}
