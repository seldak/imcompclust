/*
 * MIT License
 *
 * Copyright (c) 2014 Sameh Samir Hassan
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef QUADTREE_H_
#define QUADTREE_H_

struct pos_2d;

namespace cv {
class Mat;
}

class quadtree {
public:

	quadtree( struct pos_2d pos, unsigned int s ) :
			is_leaf( false ), pos( pos ), span( s ), cb( NULL )
	{
		nw = ne = sw = se = parent = NULL;
	}

	quadtree( quadtree &tree );
	static quadtree *copy_node( quadtree *node );

	void set_cb( compression_block *block )
	{
		cb = block;
	}

	static unsigned int get_max_quadtree_span( unsigned int scale, unsigned int rows, unsigned cols );

	void initialize_tree_down_to_scale( std::vector< std::vector< square_block * > > &best_es_grid,
			const unsigned int span,
			const unsigned int rows,
			const unsigned int cols );

	static void combine_nodes( quadtree *node );
	static void combine_cbs( quadtree *node );

	static bool divergence_check( quadtree *tree );
	static bool scale_check( quadtree *tree );
	static bool k_check( quadtree *tree );
	static bool cb_check( quadtree *tree );

	static void attempt_node_combining( quadtree *tree,
			bool (*combination_check)( quadtree * ),
			void (*combination_action)( quadtree * ) );
	unsigned int get_number_of_children( );
	double check_divergence_of_children( );

	static void add_leafs_to_list( quadtree const *tree, std::list<compression_block *> &list );

	static void draw_quadtree_node( cv::Mat &mat, quadtree *node );

	unsigned int count_leafs( );
	unsigned int count_total_nodes( );
	unsigned int count_bits_required_for_quadtree( unsigned int bpl );

	unsigned int find_maximum_node_span( unsigned int current_max_span );

private:
	quadtree *nw;
	quadtree *ne;
	quadtree *sw;
	quadtree *se;

	quadtree *parent;

	bool is_leaf;

	struct pos_2d pos;

	void delete_cb( );

	unsigned int span;

	compression_block *cb;
};

#endif /* QUADTREE_H_ */
