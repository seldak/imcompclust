/*
 * MIT License
 *
 * Copyright (c) 2014 Sameh Samir Hassan
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <boost/dynamic_bitset.hpp>
#include <iostream>
#include <fstream>
#include <ostream>
#include <sstream>
#include <future>

#include <eigen3/Eigen/Dense>

#include "compression_block.h"
#include "pos_2d.h"
#include "edge.h"
#include "pmf.h"
#include "square_block.h"
#include "utils.h"

compression_block::compression_block( struct matrix *matrix, square_block * square, unsigned int *vmatrix )
{
	blocks.push_back( square );
	const rectangle &shape = square->get_shape( );
#ifdef USE_EDGES_IN_COMPRESSION_BLOCKS
	{
		unsigned int x = shape.point.x;
		unsigned int y = shape.point.y;
		edges.push_back( edge( pos_2d( x, y ), pos_2d( x + shape.w, y ) ) );
		edges.push_back( edge( pos_2d( x, y ), pos_2d( x, y + shape.h ) ) );
		edges.push_back( edge( pos_2d( x + shape.w, y ), pos_2d( x + shape.w, y + shape.h ) ) );
		edges.push_back( edge( pos_2d( x, y + shape.h ), pos_2d( x + shape.w, y + shape.h ) ) );
	}
#endif
	compute_pmf( matrix, shape, false );
	verification_matrix = vmatrix;
	centroid = NULL;
	max_dissimilarity = sum_dissimilarity = 0.0;
}

compression_block::compression_block( square_block * square, unsigned int *vmatrix, unsigned int ord )
{
	blocks.clear( );
	blocks.push_back( square );
#ifdef USE_EDGES_IN_COMPRESSION_BLOCKS
	{
		const rectangle &shape = square->get_shape( );
		unsigned int x = shape.point.x;
		unsigned int y = shape.point.y;
		edges.push_back( edge( pos_2d( x, y ), pos_2d( x + shape.w, y ) ) );
		edges.push_back( edge( pos_2d( x, y ), pos_2d( x, y + shape.h ) ) );
		edges.push_back( edge( pos_2d( x + shape.w, y ), pos_2d( x + shape.w, y + shape.h ) ) );
		edges.push_back( edge( pos_2d( x, y + shape.h ), pos_2d( x + shape.w, y + shape.h ) ) );
	}
#endif
	verification_matrix = vmatrix;
	order = ord;
	centroid = NULL;
	max_dissimilarity = sum_dissimilarity = 0.0;
}

void compression_block::set_combined( bool status )
{
	for( auto it = blocks.begin( ); it != blocks.end( ); it++ ) {
		( *it )->set_combined( status );
	}
}

#ifdef USE_EDGES_IN_COMPRESSION_BLOCKS
void compression_block::remove_inner_edges( )
{
	std::list< edge >::iterator first, second;
	for( first = edges.begin( ); first != edges.end( ); ) {
		second = first;
		second++;
		bool delete_first = false;
		while( second != edges.end( ) ) {
			if( ( *first ) == ( *second ) ) {
				delete_first = true;
				second = edges.erase( second );
			} else
				second++;
		}
		if( delete_first )
			first = edges.erase( first );
		else
			first++;
	}
}
#endif

void compression_block::add_cb( const compression_block &cb )
{
	for( auto sq = cb.blocks.begin( ); sq != cb.blocks.end( ); sq++ ) {
		( *sq )->set_container( this );
	}
	blocks.insert( blocks.end( ), cb.blocks.begin( ), cb.blocks.end( ) );
#ifdef USE_EDGES_IN_COMPRESSION_BLOCKS
	edges.insert( edges.end( ), cb.edges.begin( ), cb.edges.end( ) );
#endif

	combine_probability_distributions( cb );
}

void compression_block::add_square( square_block *sq )
{
	sq->set_container( this );
	blocks.push_back( sq );

#ifdef USE_EDGES_IN_COMPRESSION_BLOCKS
	{
		const rectangle &shape = sq->get_shape( );
		edges.push_back(
				edge( pos_2d( shape.point.x, shape.point.y ),
						pos_2d( shape.point.x + shape.w, shape.point.y ) ) );
		edges.push_back(
				edge( pos_2d( shape.point.x, shape.point.y ),
						pos_2d( shape.point.x, shape.point.y + shape.h ) ) );
		edges.push_back(
				edge( pos_2d( shape.point.x + shape.w, shape.point.y ),
						pos_2d( shape.point.x + shape.w, shape.point.y + shape.h ) ) );
		edges.push_back(
				edge( pos_2d( shape.point.x, shape.point.y + shape.h ),
						pos_2d( shape.point.x + shape.w, shape.point.y + shape.h ) ) );
	}
#endif
	combine_dummy_probability_distribution( *sq );
}

#ifdef USE_EDGES_IN_COMPRESSION_BLOCKS
std::list< edge > &compression_block::compression_block::get_edges( )
{
	return edges;
}
#endif

std::list< square_block * > compression_block::get_blocks( ) const
{
	return blocks;
}

void compression_block::sort_squares( )
{
	blocks.sort( square_block::closer_to_origin );
}

void compression_block::write_to_file( uint8_t *matrix, unsigned int rows, unsigned int cols )
{
	static int num = 0;
	char buf[100];
	sprintf( buf, "cb_%d.out", num++ );
	std::ofstream stream;
	stream.open( buf, std::ofstream::out | std::ofstream::trunc );

	for( auto sq : blocks ) {
		rectangle &rect = sq->get_shape( );
		unsigned int i, j;
		for( j = rect.point.y; j < ( rect.point.y + rect.h ); j++ ) {
			for( i = rect.point.x; i < ( rect.point.x + rect.w ); i++ ) {

				uint8_t val = matrix[j * cols + i];

				stream << ( int )val << std::endl;
			}
		}
	}
	stream.close( );
}

std::string compression_block::cb_compress_huffman( bool write_tree )
{
	std::string compressed_output = "";
	std::cout << "number of rects in blocks are " << blocks.size( ) << std::endl;
	assert( blocks.size( ) > 0 );

	//write_to_file( matrix, rows, cols );

	std::map< uint8_t, std::string > code_map = compute_huffman_codes( );
	std::cout << " huffman code map " << std::endl;
	for( auto c : code_map ) {
		std::cout << ( int )c.first << " " << c.second.length( ) << std::endl;
	}
	std::string codeword = get_huffman_tree_codeword( );
	if( write_tree )
		compressed_output += codeword;

	for( auto it : blocks ) {
		std::cout << __func__ << it->get_shape( ) << std::endl;
		it->write_huffman_tree( false );
		compressed_output += pmf::encode( it->get_matrix( ), code_map, it->get_shape( ) );
	}
	print_compressed_length( compressed_output.length( ) );

	return compressed_output;
}

std::size_t compression_block::cb_decompress_huffman( std::string &codeword, size_t p, struct matrix *mat )
{
	assert( blocks.size( ) > 0 );
	size_t pos = p;
	std::cout << " pos before " << pos << std::endl;
	std::map< uint8_t, std::string > code_map = pmf::read_huffman_codes( codeword, pos );
	std::cout << " pos after " << pos << std::endl;
	assert( pos != p );
	assert( code_map.size( ) > 0 );

	for( auto it : blocks ) {
		std::cout << __func__ << it->get_shape( ) << std::endl;
		pos = pmf::decompress_huffman( mat, code_map, codeword, pos, it->get_shape( ) );
	}
	return pos;
}

std::string compression_block::cb_compress_golomb_rice( unsigned int M )
{
	std::string compressed_output;
	std::cout << "number of rects in blocks are " << blocks.size( ) << std::endl;
	assert( blocks.size( ) > 0 );

	//write_to_file( matrix, rows, cols );

	for( auto it : blocks ) {
		std::cout << __func__ << it->get_shape( ) << std::endl;
		compressed_output += it->compress_golomb_rice( it->get_matrix( ), M, it->get_shape( ) );
	}
	print_compressed_length( compressed_output.length( ) );

	return compressed_output;
}

std::size_t compression_block::cb_decompress_golomb_rice( std::string &codeword,
		size_t pos,
		unsigned int K,
		struct matrix *mat )
{
	std::cout << __func__ << " number of rects in blocks are " << blocks.size( ) << std::endl;
	assert( blocks.size( ) > 0 );
	for( auto it : blocks ) {
		std::cout << __func__ << it->get_shape( ) << std::endl;
		pos = pmf::decompress_golomb_rice( mat, codeword, pos, K, it->get_shape( ) );
	}
	return pos;
}

std::string compression_block::cb_compress_exponential_golomb( unsigned int k )
{
	std::string compressed_output = "";
	std::cout << "number of rects in blocks are " << blocks.size( ) << std::endl;
	assert( blocks.size( ) > 0 );
	for( auto it : blocks ) {
		std::cout << __func__ << it->get_shape( ) << std::endl;
		compressed_output += it->compress_exponential_golomb( it->get_matrix( ), k, it->get_shape( ) );
	}
	print_compressed_length( compressed_output.length( ) );

	return compressed_output;
}

std::size_t compression_block::cb_decompress_exponential_golomb( std::string &codeword,
		size_t pos,
		unsigned int K,
		struct matrix *mat )
{
	std::cout << __func__ << " number of rects in blocks are " << blocks.size( ) << std::endl;
	assert( blocks.size( ) > 0 );
	for( auto it : blocks ) {
		std::cout << __func__ << it->get_shape( ) << std::endl;
		pos = pmf::decompress_exponential_golomb( mat, codeword, pos, K, it->get_shape( ) );
	}
	return pos;
}

void compression_block::verify( )
{
	for( square_block *it : blocks ) {
		it->verify( verification_matrix );
	}
}

size_t compression_block::get_number_of_blocks( ) const
{
	return blocks.size( );
}

pos_2d compression_block::get_first_block_pos( )
{
	square_block *first = *blocks.begin( );
	rectangle &shape = first->get_shape( );
	return shape.point;
}

std::string compression_block::encode_block_size( int num_of_bits )
{
	size_t num_of_blocks = get_number_of_blocks( );
	double squares = sqrt( num_of_blocks );
	while( fmod( squares, 1 ) != 0 ) {
		std::cout << "number of blocks " << squares << std::endl;
		num_of_blocks *= 2;
		squares = sqrt( num_of_blocks );
	}
	assert( fmod( squares, 1 ) == 0 );

	boost::dynamic_bitset< > bset( num_of_bits, squares - 1 );
	//set_in_range(bset, num_of_bits-1,num_of_bits);
	std::cout << "bits = " << bset << std::endl;
	std::string str;
	to_string( bset, str );
	return str;
}

std::ostream& operator<<( std::ostream& os, const compression_block& cb )
{
	double ev = cb.get_expected_value( );
	os << "Number of squares = " << cb.blocks.size( ) << std::endl;
	os << "K = " << cb.k << std::endl;
	os << "E[X] for CB = " << ev << ", it's log is " << log2( ev ) << " (rounded = " << round( log2( ev ) )
			<< "), it's floor is " << floor( log2( ev ) ) << std::endl;
	double p = 1 / ev;
	os << "p = " << p << ", 1/log2(1-p) = " << floor( -1 / log2( 1 - p ) ) << std::endl;

	for( auto it : cb.blocks ) {
		std::cout << it->get_shape( ) << std::endl;
	}

	return os;
}

bool compression_block::operator==( const compression_block &other ) const
{
	if( blocks.size( ) != other.blocks.size( ) ) {
		std::cout << __func__ << " sizes not equal " << std::endl;
		return false;
	}
	for( auto it = blocks.begin( ), oit = other.blocks.begin( );
			it != blocks.end( ) && oit != other.blocks.end( ); it++, oit++ ) {
		if( ( *it )->get_shape( ) != ( *oit )->get_shape( ) ) {
			std::cout << __func__ << " scales not equal " << std::endl;
			return false;
		}
	}
	return true;
}

void compression_block::print_compressed_length( const size_t &length )
{
	std::ostringstream sstream;
	sstream << "CB " << get_order( ) << " Length ";
	print_length( sstream.str( ).c_str( ), length );
}

double compression_block::find_modified_max_dissimilarity( compression_block *cb1,
		compression_block *cb2,
		Eigen::MatrixXd& proximity_matrix )
{
	double max_d = 0.0;
	if( cb1->get_number_of_blocks( ) == 1 && cb2->get_number_of_blocks( ) == 1 ) {
		int order1 = ( *cb1->blocks.begin( ) )->get_order( );
		int order2 = ( *cb2->blocks.begin( ) )->get_order( );
		max_d = proximity_matrix( order1, order2 );

	} else {
		max_d = std::max( cb1->max_dissimilarity, cb2->max_dissimilarity );

		for( auto it1 = cb1->blocks.begin( ); it1 != cb1->blocks.end( ); it1++ ) {
			int order1 = ( *it1 )->get_order( );

			for( auto it2 = cb2->blocks.begin( ); it2 != cb2->blocks.end( ); it2++ ) {
				int order2 = ( *it2 )->get_order( );

				max_d = std::max( max_d, ( double )proximity_matrix( order1, order2 ) );
				if( max_d == INFINITY ) {
					return std::numeric_limits< double >::max( );
				}
			}
		}
	}
	return max_d;
}

double compression_block::find_modified_max_dissimilarity( compression_block *cb1,
		compression_block *cb2,
		double (*dissimilarity)( const pmf * const p1, const pmf * const p2 ) )
{
	double max_d = 0.0;
	if( cb1->get_number_of_blocks( ) == 1 && cb2->get_number_of_blocks( ) == 1 ) {
		max_d = dissimilarity( *cb1->blocks.begin( ), *cb2->blocks.begin( ) );

	} else {
		max_d = std::max( cb1->max_dissimilarity, cb2->max_dissimilarity );

		for( auto it1 = cb1->blocks.begin( ); it1 != cb1->blocks.end( ); it1++ ) {
			for( auto it2 = cb2->blocks.begin( ); it2 != cb2->blocks.end( ); it2++ ) {
				max_d = std::max( max_d, dissimilarity( *it1, *it2 ) );
				if( max_d == INFINITY ) {
					return std::numeric_limits< double >::max( );
				}
			}
		}
	}
	return max_d;
}

#if 0
double compression_block::find_modified_average_dissimilarity( compression_block *cb1,
		compression_block *cb2,
		Eigen::MatrixXd& proximity_matrix )
{
	std::list< square_block * > combined_blocks;

	size_t combined_blocks_size = cb1->get_number_of_blocks( ) + cb2->get_number_of_blocks( );
	//double num_of_edges = combined_blocks_size * ( combined_blocks_size - 1 ) / 2.0;
	//double num_of_edges = cb1->get_number_of_blocks( ) *  cb2->get_number_of_blocks( );
	double num_of_edges = combined_blocks_size;

	combined_blocks.insert( combined_blocks.end( ), cb1->blocks.begin( ), cb1->blocks.end( ) );
	combined_blocks.insert( combined_blocks.end( ), cb2->blocks.begin( ), cb2->blocks.end( ) );

	double sum_dissimilarity = 0.0;
	for( auto it = combined_blocks.begin( ); it != combined_blocks.end( ); it++ ) {
		auto it2 = it;
		it2++;
		while( it2 != combined_blocks.end( ) ) {
			int order1 = ( *it )->get_order( );
			int order2 = ( *it2 )->get_order( );

			sum_dissimilarity += proximity_matrix( order1, order2 );
			it2++;
		}
	}
	return sum_dissimilarity / num_of_edges;
}
#else

double compression_block::find_modified_average_dissimilarity( compression_block *cb1,
		compression_block *cb2,
		Eigen::MatrixXd& proximity_matrix )
{
	size_t combined_blocks_size = cb1->get_number_of_blocks( ) + cb2->get_number_of_blocks( );
	//double num_of_edges = combined_blocks_size * ( combined_blocks_size - 1 ) / 2.0;
	//double num_of_edges = cb1->get_number_of_blocks( ) *  cb2->get_number_of_blocks( );
	double num_of_edges = combined_blocks_size;

	double sum_d = 0.0;
	if( cb1->get_number_of_blocks( ) == 1 && cb2->get_number_of_blocks( ) == 1 ) {
		int order1 = ( *cb1->blocks.begin( ) )->get_order( );
		int order2 = ( *cb2->blocks.begin( ) )->get_order( );
		sum_d = proximity_matrix( order1, order2 );

	} else {
		sum_d = cb1->sum_dissimilarity + cb2->sum_dissimilarity;

		for( auto it1 = cb1->blocks.begin( ); it1 != cb1->blocks.end( ); it1++ ) {
			int order1 = ( *it1 )->get_order( );

			for( auto it2 = cb2->blocks.begin( ); it2 != cb2->blocks.end( ); it2++ ) {
				int order2 = ( *it2 )->get_order( );

				sum_d += proximity_matrix( order1, order2 );
				if( sum_d == INFINITY ) {
					sum_d = std::numeric_limits< double >::max( );
					goto finish;
				}
			}
		}
	}
finish:
	//std::cout << "average_jsd = " << sum_jsd / num_of_edges << std::endl;
	return sum_d / num_of_edges;
}

double compression_block::find_sum_dissimilarities_stride( square_block *sq,
		compression_block const * const cb,
		double (*dissimilarity)( const pmf * const p1, const pmf * const p2 ) )
{
	double sum_d = 0.0;
	for( auto it2 = cb->blocks.begin( ); it2 != cb->blocks.end( ); it2++ ) {
		sum_d += dissimilarity( sq, *it2 );
	}
	assert( sum_d != INFINITY );
	return sum_d;
}

double compression_block::find_modified_average_dissimilarity( compression_block *cb1,
		compression_block *cb2,
		double (*dissimilarity)( const pmf * const p1, const pmf * const p2 ) )
{
	size_t combined_blocks_size = cb1->get_number_of_blocks( ) + cb2->get_number_of_blocks( );
	//double num_of_edges = combined_blocks_size * ( combined_blocks_size - 1 ) / 2.0;
	//double num_of_edges = cb1->get_number_of_blocks( ) *  cb2->get_number_of_blocks( );
	double num_of_edges = combined_blocks_size;

	double sum_dis = 0.0;
	if( cb1->get_number_of_blocks( ) == 1 && cb2->get_number_of_blocks( ) == 1 ) {
		sum_dis = dissimilarity( *cb1->blocks.begin( ), *cb2->blocks.begin( ) );

	} else {
		sum_dis = cb1->sum_dissimilarity + cb2->sum_dissimilarity;

		bool done = false;
		auto it1 = cb1->blocks.begin( );
		while( !done ) {
			unsigned int thread_counter = 0;
			std::vector< std::future< double > > futures;
			while( thread_counter < 16 ) {
				futures.push_back(
						std::async( std::launch::async, &find_sum_dissimilarities_stride, *it1, cb2,
								dissimilarity ) );
				thread_counter++;
				it1++;
				if( it1 == cb1->blocks.end( ) ) {
					done = true;
					break;
				}
			}

			//std::cout << "joining " << thread_counter << " threads" << std::endl;
			for( auto & f : futures ) {
				sum_dis += f.get( );
				if( sum_dis == INFINITY ) {
					sum_dis = std::numeric_limits< double >::max( );
					goto finish;
				}
			}
		}
	}
finish:
	//std::cout << "average_d = " << sum_d / num_of_edges << std::endl;
	return sum_dis / num_of_edges;
}
#endif

void compression_block::add_cb_and_update_max_dissimilarity( const compression_block &cb,
		const Eigen::MatrixXd& proximity_matrix )
{
	//std::cout << "max (before)" << max_d << std::endl;

	double new_max_d = 0.0;

	if( get_number_of_blocks( ) == 1 && cb.get_number_of_blocks( ) == 1 ) {
		int order1 = ( *blocks.begin( ) )->get_order( );
		int order2 = ( *cb.blocks.begin( ) )->get_order( );
		new_max_d = proximity_matrix( order1, order2 );

	} else {
		new_max_d = std::max( max_dissimilarity, cb.max_dissimilarity );
		for( auto it1 = blocks.begin( ); it1 != blocks.end( ); it1++ ) {
			int order1 = ( *it1 )->get_order( );

			for( auto it2 = cb.blocks.begin( ); it2 != cb.blocks.end( ); it2++ ) {
				int order2 = ( *it2 )->get_order( );

				new_max_d = std::max( new_max_d, proximity_matrix( order1, order2 ) );
			}
		}
	}

	add_cb( cb );

	max_dissimilarity = new_max_d;
	//std::cout << "max (after)" << max_d << std::endl;
}

void compression_block::add_cb_and_update_max_dissimilarity( const compression_block &cb,
		double (*dissimilarity)( const pmf * const p1, const pmf * const p2 ) )
{
	//std::cout << "max (before)" << max_jsd << std::endl;

	double new_max_d = 0.0;

	if( get_number_of_blocks( ) == 1 && cb.get_number_of_blocks( ) == 1 ) {
		new_max_d = dissimilarity( *blocks.begin( ), *cb.blocks.begin( ) );

	} else {
		new_max_d = std::max( max_dissimilarity, cb.max_dissimilarity );
		for( auto it1 = blocks.begin( ); it1 != blocks.end( ); it1++ ) {
			for( auto it2 = cb.blocks.begin( ); it2 != cb.blocks.end( ); it2++ ) {
				new_max_d = std::max( new_max_d, dissimilarity( *it1, *it2 ) );
			}
		}
	}

	add_cb( cb );

	max_dissimilarity = new_max_d;
	//std::cout << "max (after)" << max_jsd << std::endl;
}

void compression_block::calculate_max_dissimilarity( double (*dissimilarity)( const pmf * const p1,
		const pmf * const p2 ) )
{
	std::cout << "max before " << max_dissimilarity << std::endl;

	max_dissimilarity = 0.0;
	for( auto first = blocks.begin( ); first != blocks.end( ); ++first ) {
		std::list< square_block * >::iterator second = first;
		second++;
		for( ; second != blocks.end( ); second++ ) {
			max_dissimilarity = std::max( max_dissimilarity, dissimilarity( *first, *second ) );
		}
	}

	std::cout << "max after " << max_dissimilarity << std::endl;
}

void compression_block::add_cb_and_update_sum_dissimilarity( compression_block &cb,
		const Eigen::MatrixXd& proximity_matrix )
{
	//std::cout << "average (before)" << sum_jsd / ( double )get_number_of_blocks( ) << std::endl;
	//size_t combined_blocks_size = get_number_of_blocks( ) + cb.get_number_of_blocks( );

	double new_sum_d = 0.0;

	if( get_number_of_blocks( ) == 1 && cb.get_number_of_blocks( ) == 1 ) {
		int order1 = ( *blocks.begin( ) )->get_order( );
		int order2 = ( *cb.blocks.begin( ) )->get_order( );
		new_sum_d = proximity_matrix( order1, order2 );

	} else {
		new_sum_d = sum_dissimilarity + cb.sum_dissimilarity;
		for( auto it1 = blocks.begin( ); it1 != blocks.end( ); it1++ ) {
			int order1 = ( *it1 )->get_order( );

			for( auto it2 = cb.blocks.begin( ); it2 != cb.blocks.end( ); it2++ ) {
				int order2 = ( *it2 )->get_order( );

				new_sum_d += proximity_matrix( order1, order2 );
			}
		}
	}

	add_cb( cb );

	sum_dissimilarity = new_sum_d;
	//std::cout << "average (after)" << sum_jsd / (double)combined_blocks_size << std::endl;
}

void compression_block::calculate_sum_dissimilarity( double (*dissimilarity)( const pmf * const p1,
		const pmf * const p2 ) )
{
	std::cout << "sum before " << sum_dissimilarity << std::endl;

	sum_dissimilarity = 0.0;
	for( auto first = blocks.begin( ); first != blocks.end( ); ++first ) {
		std::list< square_block * >::iterator second = first;
		second++;
		for( ; second != blocks.end( ); second++ ) {
			sum_dissimilarity += dissimilarity( *first, *second );
		}
	}

	std::cout << "sum after " << sum_dissimilarity << std::endl;
}

void compression_block::add_cb_and_update_sum_dissimilarity( compression_block &cb,
		double (*dissimilarity)( const pmf * const p1, const pmf * const p2 ) )
{
	//std::cout << "average (before)" << sum_jsd / ( double )get_number_of_blocks( ) << std::endl;
	//size_t combined_blocks_size = get_number_of_blocks( ) + cb.get_number_of_blocks( );

	double new_sum_d = 0.0;

	if( get_number_of_blocks( ) == 1 && cb.get_number_of_blocks( ) == 1 ) {
		new_sum_d = dissimilarity( *blocks.begin( ), *cb.blocks.begin( ) );

	} else {
		new_sum_d = sum_dissimilarity + cb.sum_dissimilarity;
		for( auto it1 = blocks.begin( ); it1 != blocks.end( ); it1++ ) {
			for( auto it2 = cb.blocks.begin( ); it2 != cb.blocks.end( ); it2++ ) {
				new_sum_d += dissimilarity( *it1, *it2 );
			}
		}
	}

	add_cb( cb );

	sum_dissimilarity = new_sum_d;
	//std::cout << "average (after)" << sum_jsd / (double)combined_blocks_size << std::endl;
}

double compression_block::find_max_dissimilarity( compression_block *cb1,
		compression_block *cb2,
		Eigen::MatrixXd& proximity_matrix )
{
	double max_d = 0.0;
	if( cb1->get_number_of_blocks( ) == 1 && cb2->get_number_of_blocks( ) == 1 ) {
		int order1 = ( *cb1->blocks.begin( ) )->get_order( );
		int order2 = ( *cb2->blocks.begin( ) )->get_order( );
		max_d = proximity_matrix( order1, order2 );

	} else {
		for( auto it1 = cb1->blocks.begin( ); it1 != cb1->blocks.end( ); it1++ ) {
			int order1 = ( *it1 )->get_order( );

			for( auto it2 = cb2->blocks.begin( ); it2 != cb2->blocks.end( ); it2++ ) {
				int order2 = ( *it2 )->get_order( );

				max_d = std::max( max_d, ( double )proximity_matrix( order1, order2 ) );
				if( max_d == INFINITY ) {
					return std::numeric_limits< double >::max( );
				}
			}
		}
	}
	return max_d;
}

double compression_block::find_min_dissimilarity( compression_block *cb1,
		compression_block *cb2,
		Eigen::MatrixXd& proximity_matrix )
{
	double min_d = INFINITY;
	if( cb1->get_number_of_blocks( ) == 1 && cb2->get_number_of_blocks( ) == 1 ) {
		int order1 = ( *cb1->blocks.begin( ) )->get_order( );
		int order2 = ( *cb2->blocks.begin( ) )->get_order( );
		min_d = proximity_matrix( order1, order2 );

	} else {
		for( auto it1 = cb1->blocks.begin( ); it1 != cb1->blocks.end( ); it1++ ) {
			int order1 = ( *it1 )->get_order( );

			for( auto it2 = cb2->blocks.begin( ); it2 != cb2->blocks.end( ); it2++ ) {
				int order2 = ( *it2 )->get_order( );

				min_d = std::min( min_d, ( double )proximity_matrix( order1, order2 ) );
				if( min_d == 0.0 ) {
					return 0.0;
				}
			}
		}
	}
	return min_d;
}

double compression_block::find_average_dissimilarity( compression_block *cb1,
		compression_block *cb2,
		Eigen::MatrixXd& proximity_matrix )
{
	double num_of_edges = cb1->get_number_of_blocks( ) * cb2->get_number_of_blocks( );

	double sum_d = 0.0;
	if( cb1->get_number_of_blocks( ) == 1 && cb2->get_number_of_blocks( ) == 1 ) {
		int order1 = ( *cb1->blocks.begin( ) )->get_order( );
		int order2 = ( *cb2->blocks.begin( ) )->get_order( );
		sum_d = proximity_matrix( order1, order2 );

	} else {
		for( auto it1 = cb1->blocks.begin( ); it1 != cb1->blocks.end( ); it1++ ) {
			int order1 = ( *it1 )->get_order( );

			for( auto it2 = cb2->blocks.begin( ); it2 != cb2->blocks.end( ); it2++ ) {
				int order2 = ( *it2 )->get_order( );

				sum_d += proximity_matrix( order1, order2 );
				if( sum_d == INFINITY ) {
					sum_d = std::numeric_limits< double >::max( );
					goto finish;
				}
			}
		}
	}
finish:
	//std::cout << "average_jsd = " << sum_jsd / num_of_edges << std::endl;
	return sum_d / num_of_edges;
}

square_block *compression_block::find_centroid( const Eigen::MatrixXd& proximity_matrix )
{
	double min_d = INFINITY;
	square_block *best_center = NULL;

	if( blocks.size( ) == 1 )
		return *blocks.begin( );

	for( auto sq : blocks ) {
		double total_d_for_node = 0.0;
		for( auto other_sq : blocks ) {
			if( sq == other_sq ) {
				continue;
			}
			int order1 = sq->get_order( );
			int order2 = other_sq->get_order( );
			total_d_for_node += proximity_matrix( order1, order2 );
		}
		if( total_d_for_node < min_d ) {
			min_d = total_d_for_node;
			best_center = sq;
		}
	}
	return best_center;
}

double compression_block::find_centroid_dissimilarity( compression_block *cb1,
		compression_block *cb2,
		Eigen::MatrixXd& proximity_matrix )
{
	double centroid_d = 0.0;
	int order1;
	int order2;
	if( cb1->get_number_of_blocks( ) == 1 && cb2->get_number_of_blocks( ) == 1 ) {
		order1 = ( *cb1->blocks.begin( ) )->get_order( );
		order2 = ( *cb2->blocks.begin( ) )->get_order( );

		centroid_d = proximity_matrix( order1, order2 );
	} else {
		square_block *sq1 = ( cb1->get_number_of_blocks( ) == 1 ) ? ( *cb1->blocks.begin( ) ) : cb1->centroid;
		square_block *sq2 = ( cb2->get_number_of_blocks( ) == 1 ) ? ( *cb2->blocks.begin( ) ) : cb2->centroid;
		order1 = ( sq1 )->get_order( );
		order2 = ( sq2 )->get_order( );

		centroid_d = proximity_matrix( order1, order2 );
	}
	return centroid_d;
}

static square_block *find_centroid_( const std::vector< square_block * > &blocks,
		const Eigen::MatrixXd& proximity_matrix )
{
	double min_d = INFINITY;
	square_block *best_center = NULL;

	for( const auto sq : blocks ) {
		double total_d_for_node = 0.0;
		int order1 = sq->get_order( );
		for( const auto other_sq : blocks ) {
			if( sq == other_sq ) {
				continue;
			}
			int order1 = sq->get_order( );
			int order2 = other_sq->get_order( );
			total_d_for_node += proximity_matrix( order1, order2 );
		}
		if( total_d_for_node < min_d ) {
			min_d = total_d_for_node;
			best_center = sq;
		}
	}
	return best_center;
}

double compression_block::find_ward_dissimilarity( compression_block *cb1,
		compression_block *cb2,
		Eigen::MatrixXd& proximity_matrix )
{
	double ward_d = 0.0;
	int order1;
	int order2;
	if( cb1->get_number_of_blocks( ) == 1 && cb2->get_number_of_blocks( ) == 1 ) {
		order1 = ( *cb1->blocks.begin( ) )->get_order( );
		order2 = ( *cb2->blocks.begin( ) )->get_order( );

		ward_d = proximity_matrix( order1, order2 );
	} else {
		square_block *sq1 = ( cb1->get_number_of_blocks( ) == 1 ) ? ( *cb1->blocks.begin( ) ) : cb1->centroid;
		square_block *sq2 = ( cb2->get_number_of_blocks( ) == 1 ) ? ( *cb2->blocks.begin( ) ) : cb2->centroid;
		order1 = sq1->get_order( );
		order2 = sq2->get_order( );

		double d1 = 0.0;
		double d2 = 0.0;
		double d_all = 0.0;

		for( auto s : cb1->blocks ) {
			if( s == sq1 )
				continue;
			int order = s->get_order( );
			d1 += proximity_matrix( order1, order );
		}
		for( auto s : cb2->blocks ) {
			if( s == sq2 )
				continue;
			int order = s->get_order( );
			d2 += proximity_matrix( order2, order );
		}

		std::vector< square_block * > new_blocks;

		new_blocks.insert( new_blocks.end( ), cb1->blocks.begin( ), cb1->blocks.end( ) );
		new_blocks.insert( new_blocks.end( ), cb2->blocks.begin( ), cb2->blocks.end( ) );

		square_block *centroid_all = find_centroid_( new_blocks, proximity_matrix );
		for( auto s : new_blocks ) {
			if( s == centroid_all )
				continue;
			int c_order = centroid_all->get_order( );
			int order = s->get_order( );
			d_all += proximity_matrix( c_order, order );
		}

		//std::cout << "D all " << d_all << " d1 " << d1 << " d2 " << d2 << std::endl;

		ward_d = d_all - d1 - d2;
	}
	return ward_d;
}

void compression_block::add_cb_and_update_centroid( compression_block &cb,
		const Eigen::MatrixXd& proximity_matrix )
{
	add_cb( cb );
	centroid = find_centroid( proximity_matrix );
}

void compression_block::fill_proximity_matrix_for_blocks( Eigen::MatrixXd &proximity_matrix,
		double (*dissimilarity)( const pmf * const p1, const pmf * const p2 ) )
{
	for( auto it = blocks.begin( ); it != blocks.end( ); it++ ) {
		int order = ( *it )->get_order( );
		proximity_matrix( order, order ) = std::numeric_limits< double >::infinity( );
		auto other = it;
		other++;
		while( other != blocks.end( ) ) {
			double d = dissimilarity( *it, *other );
			int other_order = ( *other )->get_order( );
			proximity_matrix( order, other_order ) = d;
			proximity_matrix( other_order, order ) = d;
			other++;
		}
	}
	std::cout << "computed proximity matrix" << std::endl;

}

compression_block *compression_block::split_node_with_highest_average_dissimilarity( const Eigen::MatrixXd &proximity_matrix )
{
	double max_d = -INFINITY;
	std::list< square_block * >::iterator highest_node;

	for( auto sq = blocks.begin( ); sq != blocks.end( ); sq++ ) {
		double total_d_for_node = 0.0;
		for( auto other_sq : blocks ) {
			if( *sq == other_sq ) {
				continue;
			}
			int order1 = ( *sq )->get_order( );
			int order2 = other_sq->get_order( );
			total_d_for_node += proximity_matrix( order1, order2 );
		}
		if( total_d_for_node > max_d ) {
			std::cout << "total dissimilarity for node " << total_d_for_node << std::endl;
			max_d = total_d_for_node;
			highest_node = sq;
		}
	}

	compression_block *new_cluster = new compression_block( *highest_node, verification_matrix, 0 );
	blocks.erase( highest_node );

	return new_cluster;
}

void compression_block::migrate_nodes_macnaughton_smith( compression_block *new_cluster,
		const Eigen::MatrixXd &proximity_matrix,
		double (*dissimilarity)( const pmf * const p1, const pmf * const p2 ) )
{
	bool done = false;

	while( !done ) {
		std::list< square_block * >::iterator highest_node;
		double max_d = -INFINITY;
		double diff = -INFINITY;

		if( blocks.size( ) == 1 )
			return;

		square_block *current_centroid = find_centroid( proximity_matrix );
		square_block *other_centroid = new_cluster->find_centroid( proximity_matrix );

		for( auto sq = blocks.begin( ); sq != blocks.end( ); sq++ ) {

			double avg_d_for_current_cluster = 0.0;
#if 0
			for( auto other_sq : blocks ) {
				if( *sq == other_sq ) {
					continue;
				}
				int order1 = ( *sq )->get_order( );
				int order2 = other_sq->get_order( );
				avg_d_for_current_cluster += proximity_matrix( order1, order2 );
			}
			avg_d_for_current_cluster /= blocks.size( );
#endif

			avg_d_for_current_cluster = sqrt( dissimilarity( current_centroid, *sq ) );

			double avg_d_for_other_cluster = 0.0;
#if 0
			for( auto other_sq : new_cluster->blocks ) {
				int order1 = ( *sq )->get_order( );
				int order2 = other_sq->get_order( );
				avg_d_for_other_cluster += proximity_matrix( order1, order2 );
			}
			avg_d_for_other_cluster /= new_cluster->blocks.size( );
#endif
			avg_d_for_other_cluster = sqrt( dissimilarity( other_centroid, *sq ) );

			diff = avg_d_for_current_cluster - avg_d_for_other_cluster;

			if( diff > max_d ) {
				std::cout << "diff " << diff << "avg_for_current " << avg_d_for_current_cluster
						<< " avg_for_other " << avg_d_for_other_cluster << std::endl;

				max_d = diff;
				highest_node = sq;
			}
		}
		if( diff > 0.0 ) {
			std::cout << "moved" << std::endl;
			blocks.erase( highest_node );
			new_cluster->blocks.push_back( *highest_node );
		} else {
			done = true;
		}
	}
}
