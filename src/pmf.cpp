/*
 * MIT License
 *
 * Copyright (c) 2013 Sameh Samir Hassan
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdlib.h>
#include <iostream>
#include <stdint.h>
#include <cmath>
#include <map>
#include <unistd.h>
#include <fstream>
#include <bitset>
#include <deque>
#include <assert.h>
#include <algorithm>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <queue>

#include <boost/dynamic_bitset.hpp>

#include "pmf.h"
#include "pos_2d.h"
#include "utils.h"

#define BITS_IN_ONE_BYTE 8

#ifndef DEBUG
#undef assert
#define assert(...)
#endif

template< typename A, typename B >
inline std::pair< B, A > flip_pair( const std::pair< A, B > &p )
{
	return std::pair< B, A >( p.second, p.first );
}

template< typename A, typename B >
inline std::multimap< B, A > flip_map( const std::map< A, B > &src )
{
	std::multimap< B, A > dst;
	std::transform( src.begin( ), src.end( ), std::inserter( dst, dst.begin( ) ), flip_pair< A, B > );
	return dst;
}

void write_byte_if_ready( std::string & first, int fd );
void fill_rest_of_byte( std::string & first, int fd );

#define ADAPT_SCALE 1

template< typename type >
std::map< int, int > compute_histogram( const type *matrix,
		unsigned int rows,
		unsigned int cols,
		const rectangle &rect,
		bool center )
{
	int i, j, val;
	std::map< int, int > val_count_map;
	std::map< int, int >::iterator mit;
	int iterations;
	int x_start, x_end;
	int y_start, y_end;
	bool print = false;

	if( center ) {
		x_start = rect.point.x - ( rect.w / 2 );
		y_start = rect.point.y - ( rect.h / 2 );

		if( rect.w % 2 ) {
			/* odd number */
			iterations = rect.w / 2 + 1;
		} else {
			iterations = rect.w / 2;
		}
		x_end = rect.point.x + iterations;

		if( rect.h % 2 ) {
			/* odd number */
			iterations = rect.h / 2 + 1;
		} else {
			iterations = rect.h / 2;
		}
		y_end = rect.point.y + iterations;
	} else {
		x_start = rect.point.x;
		y_start = rect.point.y;

		x_end = rect.point.x + rect.w;
		y_end = rect.point.y + rect.h;
	}

	if( x_start < 0 ) {
		x_start = 0;
#ifdef ADAPT_SCALE
		x_end = x_start + rect.w;
#endif
	}
	if( y_start < 0 ) {
		y_start = 0;
#ifdef ADAPT_SCALE
		y_end = y_start + rect.h;
#endif
	}
	if( x_end > ( int )cols ) {
		x_end = cols;
#ifdef ADAPT_SCALE
		x_start = x_end - rect.w;
#endif
	}
	if( y_end > ( int )rows ) {
		y_end = rows;
#ifdef ADAPT_SCALE
		y_start = y_end - rect.h;
#endif
	}

	if( ( rect.w == 4 )
			&& ( ( ( rect.point.x == 0 ) && ( rect.point.y == 0 ) )
					|| ( ( rect.point.x == 20 ) && ( rect.point.y == 20 ) )
					|| ( ( rect.point.x == 39 ) && ( rect.point.y == 39 ) ) ) ) {
		print = true;
	}

	if( print ) {
		std::cout << __func__ << ": " << "x: " << rect.point.x << ", y: " << rect.point.y << ", w: " << rect.w
				<< ", h: " << rect.h << std::endl;
		std::cout << __func__ << ": " << "x_start: " << x_start << ", x_end: " << x_end << ", y_start: "
				<< y_start << ", y_end: " << y_end << std::endl;
	}

	for( j = y_start; j < y_end; j++ ) {
		for( i = x_start; i < x_end; i++ ) {

			bool found = false;
			val = matrix[j * cols + i];

			for( mit = val_count_map.begin( ); mit != val_count_map.end( ); mit++ ) {
				if( print )
					std::cout << __func__ << ": val = " << ( *mit ).first << ", count = " << ( *mit ).second
							<< std::endl;
				if( ( *mit ).first == val ) {
					( *mit ).second++; // increment count
					found = true;
					break;
				}
			}
			if( found == false ) {
				val_count_map[val] = 1;
			}
		}
	}

	return val_count_map;
}

template< typename type >
std::map< int, int > compute_histogram( const std::vector< type > &vector )
{
	type val;
	std::map< int, int > val_count_map;
	std::map< int, int >::iterator mit;
	bool print = false;

	for( typename std::vector< type >::const_iterator vit = vector.begin( ); vit != vector.end( ); vit++ ) {

		bool found = false;
		val = *vit;

		for( mit = val_count_map.begin( ); mit != val_count_map.end( ); mit++ ) {
			if( print )
				std::cout << __func__ << ": val = " << ( *mit ).first << ", count = " << ( *mit ).second
						<< std::endl;
			if( ( *mit ).first == val ) {
				( *mit ).second++; // increment count
				found = true;
				break;
			}
		}
		if( found == false ) {
			val_count_map[val] = 1;
		}

	}

	return val_count_map;
}

void pmf::compute_pmf( const struct matrix *mat, const rectangle &rect, bool center )
{
	std::map< int, int > count_map;

	total_pixel_count = rect.w * rect.h;
	assert( total_pixel_count > 0 );

	std::map< int, int >::iterator mit;

	etable.clear( );
	entropy = 0;

	count_map = compute_histogram( mat->data, mat->rows, mat->cols, rect, center );

	for( mit = count_map.begin( ); mit != count_map.end( ); mit++ ) {
		double probability;
		int val = ( *mit ).first;
		int count = ( *mit ).second;

		//std::cout << (*mit).first << ": " << (*mit).second << std::endl;

		probability = ( double )count / total_pixel_count;
		etable[val] = probability_record( count, probability );
		entropy += etable[val].get_information( );
	}

	write_to_file = false;
}

void pmf::compute_pmf( const std::vector< uint8_t > &vector )
{
	std::map< int, int > count_map;

	total_pixel_count = vector.size( );
	assert( total_pixel_count > 0 );

	std::map< int, int >::iterator mit;

	etable.clear( );
	entropy = 0;

	count_map = compute_histogram( vector );

	for( mit = count_map.begin( ); mit != count_map.end( ); mit++ ) {
		double probability;
		int val = ( *mit ).first;
		int count = ( *mit ).second;

		//std::cout << (*mit).first << ": " << (*mit).second << std::endl;

		probability = ( double )count / total_pixel_count;
		etable[val] = probability_record( count, probability );
		entropy += etable[val].get_information( );
	}
}

unsigned int pmf::get_number_of_modes( )
{
	unsigned int result = 0, max = 0;
	std::map< int, probability_record >::iterator it;
	for( it = etable.begin( ); it != etable.end( ); it++ ) {
		if( ( *it ).second.get_count( ) > max ) {
			max = ( *it ).second.get_count( );
		}
	}
	for( it = etable.begin( ); it != etable.end( ); it++ ) {
		if( ( *it ).second.get_count( ) == max ) {
			result++;
		}
	}
	return result;
}

double pmf::get_expected_value( ) const
{
	double ev = 0;
	for( auto it = etable.begin( ); it != etable.end( ); it++ ) {
		ev += ( *it ).first * ( *it ).second.get_probability( );
	}
	return ev;
}

int pmf::get_median( )
{
	unsigned int median = 0, i = 0;
	std::map< int, probability_record >::iterator it = etable.begin( );
	for( i = 0; i < etable.size( ) / 2; i++, it++ ) {
		median = ( *it ).first;
	}
	return median;
}

int pmf::get_mode( )
{
	unsigned int mode = 0, max = 0;
	std::map< int, probability_record >::iterator it;
	for( it = etable.begin( ); it != etable.end( ); it++ ) {
		if( ( *it ).second.get_count( ) > max ) {
			max = ( *it ).second.get_count( );
			mode = ( *it ).first;
		}
	}
	return mode;
}

int pmf::get_maximum_value( )
{
	int max = 0;
	max = ( *etable.rbegin( ) ).first;
	return max;
}

static bool nearly_equal( const double a, const double b )
{
#define EPSILON  0.0001
    return std::fabs( a - b ) < EPSILON; // or std::numeric_limits<double>::epsilon()
#undef EPSILON
}

void pmf::combine_probability_distributions( const pmf &table2 )
{
	total_pixel_count += table2.total_pixel_count;
	//std::cout << "new pixel count = " << total_pixel_count << std::endl;

	double probability_sum = 0.0;
//	for( auto const &it : etable ) {
//		probability_sum += it.second.get_probability( );
//		//std::cout << "val = " << it.first << " count " << it.second.get_count( ) << " probability "
//				//<< it.second.get_probability( ) << std::endl;
//	}
//	//std::cout << "old probability sum " << probability_sum << std::endl;

	for( auto const &it2 : table2.etable ) {
		//std::cout << __func__ << ": val2: " << it2.first << std::endl;
		std::map< int, probability_record >::iterator it1;
		if( ( it1 = etable.find( it2.first ) ) != etable.end( ) ) {
			//std::cout << __func__ << ": val1: " << ( *it1 ).first << ", val2: " << it2.first << std::endl;
			//std::cout << __func__ << ": count1: " << ( *it1 ).second.get_count( ) << ", count2: "
					//<< it2.second.get_count( ) << std::endl;

			unsigned int count = ( *it1 ).second.get_count( );
			count += it2.second.get_count( );
			( *it1 ).second.set_count( count );
		} else {
			etable[it2.first].set_count( it2.second.get_count( ) );
		}
	}

	unsigned int total_count = 0;

	probability_sum = 0.0;
	entropy = 0.0;
	for( auto &it : etable ) {
		double probability = it.second.get_count( ) / ( double )total_pixel_count;
		probability_sum += probability;
		assert( probability > 0 );
		total_count += it.second.get_count( );
		//std::cout << __func__ << ": count merged: " << it.second.get_count( ) << std::endl;
		it.second.set_probability( probability );
		assert( it.second.get_probability( ) > 0 );
//		std::cout << "val = " << it.first << " count " << it.second.get_count( ) << " probability "
//				<< it.second.get_probability( ) << " inf = " << it.second.get_information( ) << std::endl;
		entropy += it.second.get_information( );
	}
//	std::cout << " new entropy = " << entropy << std::endl;
//	std::cout << " total count = " << total_count << std::endl;
	assert( nearly_equal( probability_sum, 1.0 ) );

	assert( total_count == total_pixel_count );
	//abort();
}

template< typename type >
std::string mtf_code( std::deque< type > &last, type *matrix, int rows, int cols, rectangle &rect )
{
	typename std::deque< type >::iterator it;
	typename std::deque< type > s( last );
	std::string codeword;

	std::sort( s.begin( ), s.end( ) );
	s.erase( std::unique( s.begin( ), s.end( ) ), s.end( ) );
	std::cout << "unique" << std::endl;
	for( it = s.begin( ); it != s.end( ); ++it ) {
		std::cout << " " << ( int )( *it );
	}
	std::cout << std::endl;

	unsigned int i = rect.point.x;
	unsigned int j = rect.point.y;

	std::cout << "x = " << rect.point.x << ", y = " << rect.point.y << ", w = " << rect.w << ", h = "
			<< rect.h << std::endl;

	for( it = last.begin( ); it != last.end( ); it++ ) {
		type pos = 0;
		bool found = false;
		for( typename std::deque< type >::iterator dit = s.begin( ); dit != s.end( ); dit++, pos++ ) {
			if( *it == *dit ) {
				found = true;
				s.erase( dit );
				s.push_front( *it );
				break;
			}
		}
		assert( found == true );

		std::stringstream ss;
		ss << ( int )pos;
		if( it != last.end( ) - 1 )
			ss << ",";
		matrix[j * cols + i] = pos;

		i++;
		if( i == ( rect.point.x + rect.w ) ) {
			i = rect.point.x;
			j++;
		}

		codeword += ss.str( );

		std::cout << "unique" << std::endl;
		for( typename std::deque< type >::iterator i = s.begin( ); i != s.end( ); ++i ) {
			std::cout << " " << ( int )( *i );
		}
		std::cout << std::endl << "codeword:" << std::endl;
		std::cout << codeword << std::endl;
	}
	return codeword;
}

std::string pmf::compress_burrows_wheeler_transform( struct matrix *matrix, rectangle &rect )
{
	std::string codeword;
	std::multimap< probability_record, int > dst = flip_map( etable );
	unsigned int num_of_pixels = rect.w * rect.h;

	assert( num_of_pixels > 0 );

	std::vector< std::deque< uint8_t > > q( num_of_pixels );
	std::deque< uint8_t > temp;
	std::deque< uint8_t > last;

	std::map< probability_record, int >::iterator mit;
	probability_record eentry;

	for( mit = dst.begin( ); mit != dst.end( ); mit++ ) {
		std::cout << __func__ << "  " << ( *mit ).first.get_count( ) << "  " << ( *mit ).second << std::endl;
	}
	assert( dst.size( ) == etable.size( ) );
	std::bitset< 8 > bset( dst.size( ) );
	std::cout << "size in string = " << bset.to_string( ) << std::endl;
	codeword += bset.to_string( );

	for( mit = dst.begin( ); mit != dst.end( ); mit++ ) {
		int val = ( *mit ).second;
		eentry = ( *mit ).first;

		std::cout << eentry.get_count( ) << " times, Pt=" << std::setprecision( 4 ) << std::fixed
				<< eentry.get_probability( ) << ", Ht=" << eentry.get_information( ) << " : " << val
				<< std::endl;
		bset = val;
		std::cout << "value in string = " << bset.to_string( ) << std::endl;
		codeword += bset.to_string( );
	}

	unsigned int i, j;
	int pos = 0;

	for( j = rect.point.y; j < ( rect.point.y + rect.h ); j++ ) {
		for( i = rect.point.x; i < ( rect.point.x + rect.w ); i++ ) {
			uint8_t val = matrix->data[j * matrix->cols + i];
			q[0].push_back( val );
		}
	}

	temp = q[0];

	for( i = 1; i < num_of_pixels; i++ ) {
		q[i] = q[i - i];
		for( j = i; j > 0; j-- ) {
			uint8_t val = q[i].front( );
			q[i].pop_front( );
			q[i].push_back( val );
		}
	}

	std::deque< uint8_t >::iterator it;

	for( i = 0; i < num_of_pixels; i++ ) {
		for( it = q[i].begin( ); it != q[i].end( ); ++it ) {
			std::cout << " " << ( int )( *it );
		}
		std::cout << std::endl;
	}

	bool found = false;
	std::sort( q.begin( ), q.end( ) );

	std::cout << std::endl;

	for( i = 0; i < num_of_pixels; i++ ) {
		for( it = q[i].begin( ); it != q[i].end( ); ++it ) {
			std::cout << " " << ( int )( *it );
			if( it == q[i].end( ) - 1 )
				last.push_back( *it );
		}
		if( q[i] == temp ) {
			pos = i;
			found = true;
		}
		std::cout << std::endl;
	}
	std::cout << " found at position " << pos << std::endl;
	assert( found );

	std::cout << "last: " << std::endl;
	for( it = last.begin( ); it != last.end( ); ++it ) {
		std::cout << " " << ( int )( *it );
	}
	std::cout << std::endl;

	mtf_code( last, matrix->data, matrix->rows, matrix->cols, rect );

	return codeword;
}

std::multimap< probability_record, int > pmf::flip_entropy_table( )
{
	return flip_map( etable );
}

double pmf::calculate_squared_euclidean_distance( const pmf * const p1, const pmf * const p2 )
{
	assert( p1->vector.size( ) > 0 );
	assert( p2->vector.size( ) > 0 );
	assert( p1->vector.size( ) == p2->vector.size( ) );
	int distance = 0;
	for( unsigned int i = 0; i < p1->vector.size( ); i++ ) {
		int diff = p1->vector[i] - p2->vector[i];
		distance += ( diff * diff );
	}
	return double( distance );
}

double pmf::calculate_kullback_leibler_distance( const pmf * const p1, const pmf * const p2 )
{
	double kld = 0.0;
	for( auto const &entry : p1->etable ) {
		int val = entry.first;
		std::map< int, probability_record >::const_iterator it;
		if( ( it = p2->etable.find( val ) ) != p2->etable.end( ) ) {
			double p = entry.second.get_probability( );
			double q = ( *it ).second.get_probability( );
			//std::cout << p << " " << q << " " << p / q << std::endl;
			if( p == 0 ) {
				std::cout << " " << entry.second.get_count( ) << " " << entry.second.get_information( ) << std::endl;
				abort( );
			}
			if( q == 0 ) {
				std::cout << " " << (*it).second.get_count( ) << " " << (*it).second.get_information( ) << std::endl;
				abort( );
			}
			kld += p * log2( p / q );
		} else {
			return INFINITY;
		}
	}
	//std::cout << "kld " << kld << std::endl;
	assert( kld >= 0.0 );
	return kld;
}

bool pmf::is_kullback_leibler_distance_infinity( const pmf &other )
{
	std::map< int, double > prob1_map, prob2_map;
	std::map< int, double >::iterator it1, it2;

	for( auto const &entry : etable ) {
		int val = entry.first;
		std::map< int, probability_record >::const_iterator it;
		if( ( it = other.etable.find( val ) ) != other.etable.end( ) ) {
		} else {
			return true;
		}
	}

	return false;
}

#if 0
static void merge_probability_distributions( const std::map< int, probability_record > &table1,
		const std::map< int, probability_record > &table2,
		std::map< int, double > &prob1_map,
		std::map< int, double > &prob2_map )
{
	std::vector< double > prob_vector;
	std::map< int, probability_record >::const_iterator it1, it2;

	assert( prob1_map.empty( ) );
	assert( prob2_map.empty( ) );

	for( it1 = table1.begin( ), it2 = table2.begin( ); ( it1 != table1.end( ) ) && ( it2 != table2.end( ) );
			) {
		bool increment_first = false;
		bool increment_second = false;

		//std::cout << __func__ << ": val1: " << ( *it1 ).first << ", val2: " << ( *it2 ).first << std::endl;
		if( ( *it1 ).first == ( *it2 ).first ) {

			/* By convention the std::map is by default sorted via key. */
			prob1_map[( *it1 ).first] = ( *it1 ).second.get_probability( );
			prob2_map[( *it1 ).first] = ( *it2 ).second.get_probability( );

			//std::cout << "increment both" << std::endl;
			increment_first = true;
			increment_second = true;
		} else if( ( *it1 ).first < ( *it2 ).first ) {

			prob1_map[( *it1 ).first] = ( *it1 ).second.get_probability( );
			prob2_map[( *it1 ).first] = 0.0;

			//std::cout << "increment first" << std::endl;
			increment_first = true;
		} else {

			prob1_map[( *it2 ).first] = 0.0;
			prob2_map[( *it2 ).first] = ( *it2 ).second.get_probability( );

			//std::cout << "increment second" << std::endl;
			increment_second = true;
		}

		if( increment_first )
			it1++;
		if( increment_second )
			it2++;

		if( it1 == table1.end( ) ) {
			for( ; it2 != table2.end( ); it2++ ) {
				//std::cout << __func__ << ": val1: " << 0 << ", val2: " << ( *it2 ).first << std::endl;
				prob1_map[( *it2 ).first] = 0.0;
				prob2_map[( *it2 ).first] = ( *it2 ).second.get_probability( );
			}
			break;
		} else if( it2 == table2.end( ) ) {
			for( ; it1 != table1.end( ); it1++ ) {
				//std::cout << __func__ << ": val1: " << ( *it1 ).first << ", val2: " << 0 << std::endl;
				prob1_map[( *it1 ).first] = ( *it1 ).second.get_probability( );
				prob2_map[( *it1 ).first] = 0.0;
			}
			break;
		}

	}
}

static double kl_distance_private( std::map< int, double > prob1_map, std::map< int, double > prob2_map )
{
	double kl = 0.0;
	std::map< int, double >::iterator it1, it2;

	for( it1 = prob1_map.begin( ), it2 = prob2_map.begin( ); it1 != prob1_map.end( ); it1++, it2++ ) {
		assert( ( *it1 ).first == ( *it2 ).first );
		double prob1 = ( *it1 ).second;
		double prob2 = ( *it2 ).second;
		//std::cout << __func__ << ": prob1 = " << prob1 << ", prob2 = " << prob2 << std::endl;
		if( prob1 == 0.0 ) {
			continue;
		} else if( prob2 == 0.0 ) {
			return INFINITY;
		} else {
			kl += prob1 * log2( prob1 / prob2 );
		}
	}
	kl = ( kl < 0 ) ? 0 : kl;

	return kl;
}

double pmf::calculate_jensen_shannon_divergence( const pmf &other )
{
	double jsd = 0.0;

	std::map< int, double > prob1_map, prob2_map;
	std::map< int, double > mixture_prob_dist;
	std::map< int, double >::iterator it1, it2;

	/* prob1_vector and prob2_vector could be considered histograms */
	merge_probability_distributions( etable, other.etable, prob1_map, prob2_map );

	assert( prob1_map.size() == prob2_map.size() );

	for( it1 = prob1_map.begin( ), it2 = prob2_map.begin( );
			it1 != prob1_map.end( ) && it2 != prob2_map.end( ); it1++, it2++ ) {
		int val = ( *it1 ).first;
		double prob1 = ( *it1 ).second;
		double prob2 = ( *it2 ).second;
//		std::cout << __func__ << ": prob1: " << prob1 << ", prob2: " << prob2 << ", mixture prob: "
//				<< ( prob1 + prob2 ) / 2.0 << std::endl;
		mixture_prob_dist[val] = ( prob1 + prob2 ) / 2.0;
	}

	assert( prob1_map.size() == mixture_prob_dist.size() );

	jsd = ( kl_distance_private( prob1_map, mixture_prob_dist )
			+ kl_distance_private( prob2_map, mixture_prob_dist ) ) / 2.0;

	return jsd;
}
#endif

/*
 * Equal weights (pi_i) are used (which makes it a bit faster).
 */
double pmf::get_mixture_map( std::vector< pmf * > &distribution_vector, std::map< int, probability_record > *mixture )
{
	std::map< int, probability_record > merged_distribution;

	assert( likely( distribution_vector.size( ) > 1 ) );

	const double weight = ( double )distribution_vector.size( );

	//unsigned int sum = 0;
	unsigned int total_sum = 0;

	for( auto distribution_iterator : distribution_vector ) {
		double probability_sum = 0.0;
		total_sum += distribution_iterator->total_pixel_count;
		for( auto const &it : distribution_iterator->etable ) {
			const int value = it.first;
			const probability_record &entry = it.second;
			probability_sum += entry.get_probability( );

			probability_record &previous_entry = merged_distribution[value];
			double probability = previous_entry.get_probability( );
			probability += entry.get_count( ) / double( weight * distribution_iterator->total_pixel_count );
			previous_entry.set_probability( probability );
		}
		//std::cout << "probability sum = " << probability_sum << std::endl;
		assert( likely( nearly_equal( probability_sum, 1.0 ) ) );
	}

	//std::cout << "sum = " <<  pixel_count_total << std::endl;

	double probability_sum = 0.0;
	double mixture_entropy = 0.0;

	//unsigned int new_sum = 0;

	for( auto const &it : merged_distribution ) {
		probability_record const &eentry = it.second;

		mixture_entropy += eentry.get_information( );
		probability_sum += eentry.get_probability( );

	}
	//std::cout << "sum = " << sum << std::endl;

	//assert( likely(new_sum == sum) );
	//std::cout << "mixture probability sum = " << probability_sum << std::endl;
	assert( likely(nearly_equal(probability_sum, 1.0)) );
	assert( likely( 0.0 <= mixture_entropy ) );

	if( mixture )
		*mixture = merged_distribution;

	return mixture_entropy;
}

double pmf::get_mixture_entropy( std::vector< pmf * > &distribution_vector )
{
	return get_mixture_map( distribution_vector, NULL );
}

double pmf::calculate_jensen_shannon_divergence( std::vector< pmf * > &distribution_vector )
{
	const double mixture_entropy = get_mixture_entropy( distribution_vector );

	//std::cout << "mixture entropy " << mixture_entropy << std::endl;

	double entropy_sum = 0.0;
	for( auto dit : distribution_vector ) {
		entropy_sum += dit->get_entropy( );
	}

	const double jsd = mixture_entropy - entropy_sum / ( double )distribution_vector.size( );
	//std::cout << "JSD = " << jsd << std::endl;

	assert( likely( 0.0 <= jsd ) );

	return jsd;
}

double pmf::get_mixture_entropy( pmf const * const p1, pmf const * const p2 )
{
	std::map< int, probability_record > merged_distribution;

	//unsigned int sum = 0;
	unsigned int total_sum = 0;

	{
		//double probability_sum = 0.0;
		total_sum += p1->total_pixel_count;
		for( auto const &it : p1->etable ) {
			const int value = it.first;
			const probability_record &entry = it.second;
			//probability_sum += entry.get_probability( );

			probability_record &previous_entry = merged_distribution[value];
			double probability = previous_entry.get_probability( );
			probability += entry.get_count( ) / double( 2.0 * p1->total_pixel_count );
			previous_entry.set_probability( probability );
		}
		//std::cout << "probability sum = " << probability_sum << std::endl;
		//assert( likely( nearly_equal( probability_sum, 1.0 ) ) );
	}
	{
		//double probability_sum = 0.0;
		total_sum += p2->total_pixel_count;
		for( auto const &it : p2->etable ) {
			const int value = it.first;
			const probability_record &entry = it.second;
			//probability_sum += entry.get_probability( );

			probability_record &previous_entry = merged_distribution[value];
			double probability = previous_entry.get_probability( );
			probability += entry.get_count( ) / double( 2.0 * p2->total_pixel_count );
			previous_entry.set_probability( probability );
		}
		//std::cout << "probability sum = " << probability_sum << std::endl;
		//assert( likely( nearly_equal( probability_sum, 1.0 ) ) );
	}


	//std::cout << "sum = " <<  pixel_count_total << std::endl;

	//double probability_sum = 0.0;
	double mixture_entropy = 0.0;

	//unsigned int new_sum = 0;

	for( auto const &it : merged_distribution ) {
		probability_record const &eentry = it.second;

		mixture_entropy += eentry.get_information( );
		//probability_sum += eentry.get_probability( );

	}
	//std::cout << "sum = " << sum << std::endl;

	//assert( likely(new_sum == sum) );
	//std::cout << "mixture probability sum = " << probability_sum << std::endl;
	//assert( likely(nearly_equal(probability_sum, 1.0)) );
	assert( likely( 0.0 <= mixture_entropy ) );

	return mixture_entropy;
}

double pmf::calculate_jensen_shannon_divergence( pmf const * const p1, pmf const * const p2 )
{
	const double mixture_entropy = get_mixture_entropy( p1, p2 );

	//std::cout << "mixture entropy " << mixture_entropy << std::endl;

	double entropy_sum = p1->get_entropy( ) + p2->get_entropy( );

	const double jsd = mixture_entropy - entropy_sum / 2.0;
	//std::cout << "JSD = " << jsd << std::endl;

	assert( likely( 0.0 <= jsd ) );

	return jsd;
}

double pmf::calculate_normalized_jensen_shannon_divergence( std::vector< pmf * > &distribution_vector )
{
	double jsd = 0.0;

	double mixture_entropy = get_mixture_entropy( distribution_vector );

	//std::cout << "mixture entropy " << mixture_entropy << std::endl;

	double entropy_sum = 0.0;
	for( auto dit : distribution_vector ) {
		entropy_sum += dit->get_entropy( );
	}

	double weight = 1 / ( double )distribution_vector.size( );

	jsd = ( mixture_entropy - weight * entropy_sum ) / double( mixture_entropy );
	//std::cout << "JSD = " << jsd << std::endl;

	assert( likely( 0.0 <= jsd ) );

	return jsd;
}

double pmf::calculate_single_jensen_shannon_divergence( const pmf * const p1, const pmf * const p2 )
{
	assert( p1->total_pixel_count == 1 && p2->total_pixel_count == 1 );
	auto it1 = p1->etable.begin( );
	auto it2 = p2->etable.begin( );
	if( (*it1).first==(*it2).first )
		return 0.0;
	else
		return 1.0;
}

double pmf::calculate_bhattacharyya_distance( const pmf * const p1, const pmf * const p2 )
{
	double distance = 0.0;
	for( auto const &it1 : p1->etable ) {
		const int value = it1.first;

		auto const & it2 = p2->etable.find( value );
		if( it2 == p2->etable.end( ) )
			continue;

		const double probability1 = it1.second.get_probability( );
		const double probability2 = ( *it2 ).second.get_probability( );
		distance += std::sqrt( probability1 * probability2 );
	}

	distance = -1 * std::log2( distance );

	return distance;
}

bool pmf::do_tables_match( const pmf &table1, const pmf &table2 )
{
	if( table1.etable.size( ) != table2.etable.size( ) )
		return false;
	auto it1 = table1.etable.begin( );
	auto it2 = table2.etable.begin( );
	while( it1 != table1.etable.end( ) ) {
		if( ( *it1 ).first != ( *it2 ).first )
			return false;
		it1++;
		it2++;
	}
	return true;
}

bool pmf::do_blocks_coincide( const pmf &p1, const pmf &p2 )
{
	return ( calculate_squared_euclidean_distance( &p1, &p2 ) == 0.0 );
}

bool pmf::do_pmfs_exactly_match( const pmf &table1, const pmf &table2 )
{
	if( table1.etable.size( ) != table2.etable.size( ) )
		return false;
	auto it1 = table1.etable.begin( );
	auto it2 = table2.etable.begin( );
	while( it1 != table1.etable.end( ) ) {
		if( ( *it1 ).first == ( *it2 ).first ) {
			const double p1 = ( *it1 ).second.get_probability( );
			const double p2 = ( *it2 ).second.get_probability( );
			if( p1 != p2 )
				return false;
		} else {
			return false;
		}
		it1++;
		it2++;
	}
	return true;
}

void write_byte_if_ready( std::string & first, int fd )
{
	std::string last;
	while( first.length( ) > 8 ) {
		last = first.substr( 8, first.length( ) - 8 );
		first = first.substr( 0, 8 );
		//std::cout << first << std::endl;
		write( fd, first.c_str( ), 1 );
		first = last;
	}
}

void fill_rest_of_byte( std::string & first, int fd )
{
	if( first.length( ) ) {
		while( first.length( ) < 8 ) {
			first += "1";
		}
		write( fd, first.c_str( ), 1 );
	}
}

/** Compare this huffman_node and other for priority ordering.
 * Smaller count means higher priority.
 * Use node symbol for deterministic tiebreaking
 */
template< typename type >
bool huffman_node< type >::operator<( huffman_node< type > const & other ) const
{
	return count > other.count;
}

template< typename type >
class huffman_node_ptr_comp {
public:
	bool operator()( huffman_node< type > * & lhs, huffman_node< type > * & rhs ) const
	{
		// dereference the pointers and use operator<
		return *lhs < *rhs;
	}
};

template< typename type >
void store_huffman_tree( huffman_node< type > *node, std::string &codeword )
{
	if( node->is_leaf ) {
		std::bitset< 8 > bset( node->symb );
		codeword += "1";
		codeword += bset.to_string( );
	} else {
		codeword += "0";
		store_huffman_tree( node->right, codeword );
		store_huffman_tree( node->left, codeword );
	}
	//std::cout << "dict is now " << codeword << std::endl;
}

template< typename type >
void free_huffman_tree( huffman_node< type > *node )
{
	if( node->is_leaf ) {
		delete node;
	} else {
		free_huffman_tree( node->right );
		free_huffman_tree( node->left );
		delete node;
	}
}

template< typename type >
huffman_node< type > * construct_huffman_tree( std::vector< huffman_node< type > * > &leafs )
{
	std::priority_queue< huffman_node< type > *, std::vector< huffman_node< type > * >,
			huffman_node_ptr_comp< type > > pq;

	for( typename std::vector< huffman_node< type > * >::const_iterator it = leafs.begin( );
			it != leafs.end( ); it++ ) {
		pq.push( *it );
	}

	while( pq.size( ) > 1 ) {
		huffman_node< type > *node = new huffman_node< type >( false );
		huffman_node< type > *left = pq.top( );
		std::cout << "pq: left\t" << int( left->symb ) << " (" << left->count << ")" << std::endl;
		pq.pop( );
		huffman_node< type > *right = pq.top( );
		std::cout << "pq: right\t" << int( right->symb ) << " (" << right->count << ")" << std::endl;
		pq.pop( );
		left->is_left = true;
		right->is_left = false;
		node->left = left;
		node->right = right;
		left->parent = right->parent = node;
		node->count = left->count + right->count;
		pq.push( node );
	}

	huffman_node< type > *root = pq.top( );
	pq.pop( );
	return root;
}

template< typename type >
std::map< type, std::string > get_huffman_codes_from_tree( const huffman_node< type > *root,
		const std::vector< huffman_node< type > * > &leafs )
{
	std::string str = "0";
	std::map< type, std::string > code_map;

	assert( leafs.size( )>0 );

	if( leafs.size( ) == 1 ) {
		typename std::vector< huffman_node< type > * >::const_iterator it = leafs.begin( );
		code_map[( *it )->symb] = str;
	} else {
		for( typename std::vector< huffman_node< type > * >::const_iterator it = leafs.begin( );
				it != leafs.end( ); it++ ) {
			huffman_node< type > *node = ( *it );
			std::string codeword;
			while( node != root ) {
				if( node->is_left ) {
					codeword.insert( 0, "1" );
				} else {
					codeword.insert( 0, "0" );
				}
				node = node->parent;
			}
			std::cout << " " << ( int )( *it )->symb << "(" << ( *it )->count << ") -> " << codeword
					<< std::endl;
			//code_map.push_back( codeword );
			code_map[( *it )->symb] = codeword;
		}
	}
	return code_map;
}

std::string pmf::huffman_private( const std::vector< uint8_t > &vector )
{
	std::string first;
	std::string last;
	std::string codeword;
	std::map< uint8_t, std::string > codes = compute_huffman_codes( );

	if( write_huffman_tree_with_coded_bits ) {
		codeword += huffman_tree_string;
	}

	assert( vector.size() > 0 );

	for( typename std::vector< uint8_t >::const_iterator vit = vector.begin( ); vit != vector.end( );
			vit++ ) {

		uint8_t val = *vit;

		std::map< uint8_t, std::string >::iterator temp_it;
		temp_it = codes.find( val );
		assert( temp_it != codes.end( ) );

		codeword += ( *temp_it ).second; // concatenate code

		first += ( *temp_it ).second;
		if( write_to_file )
			write_byte_if_ready( first, output_fd );

	}

	/*if( write_to_file )
	 fill_rest_of_byte( first, output_fd );*/

	return codeword;
}

std::map< uint8_t, std::string > pmf::compute_huffman_codes( )
{
	std::string codeword = "";
	std::map< uint8_t, std::string > code_map;
	std::vector< huffman_node< uint8_t > * > leafs;

	for( auto const &it : etable ) {
		uint8_t number = it.first;
		std::cout << "\t" << it.second.get_count( ) << " " << it.first << std::endl;
		huffman_node< uint8_t > *node = new huffman_node< uint8_t >( it.second.get_count( ), number, true );
		leafs.push_back( node );
	}

	huffman_node< uint8_t > *root = construct_huffman_tree( leafs );

	huffman_node_vector.clear( );
	huffman_symbol_vector.clear( );
	store_huffman_tree( root, codeword );
	huffman_tree_string = codeword;
	store_huffman_tree_in_separate_vector( root );

	code_map = get_huffman_codes_from_tree( root, leafs );

	free_huffman_tree( root );

	return code_map;
}

void pmf::store_huffman_tree_in_separate_vector( huffman_node< uint8_t > *node )
{
	if( node->is_leaf ) {
		huffman_node_vector.push_back( true );
		huffman_symbol_vector.push_back( node->symb );
	} else {
		huffman_node_vector.push_back( false );
		store_huffman_tree_in_separate_vector( node->right );
		store_huffman_tree_in_separate_vector( node->left );
	}
}

template< typename type >
huffman_node< type > * reconstruct_huffman_tree( const std::string &codeword,
		size_t &pos,
		std::vector< huffman_node< type > * > &leafs )
{
	huffman_node< type > *node;
	std::bitset< 1 > node_type = std::bitset< 1 >( codeword, pos, 1 );
	pos++;
	bool is_leaf;

	unsigned long int val = node_type.to_ulong( );
	//stream << node_type.to_string( );

	if( val == 1 ) {
		is_leaf = true;

		std::bitset< 8 > bset = std::bitset< 8 >( codeword, pos, BITS_IN_ONE_BYTE );
		//stream << bset.to_string( );
		pos += BITS_IN_ONE_BYTE;

		node = new huffman_node< type >( bset.to_ulong( ), is_leaf );
		leafs.push_back( node );

	} else if( val == 0 ) {
		is_leaf = false;

		node = new huffman_node< type >( 0, is_leaf );
		node->right = reconstruct_huffman_tree( codeword, pos, leafs );
		node->left = reconstruct_huffman_tree( codeword, pos, leafs );
		node->left->is_left = true;

		node->left->parent = node->right->parent = node;

	} else {
		std::cerr << "ERROR: value not allowed " << std::endl;
		abort( );
	}

	return node;

}

std::map< uint8_t, std::string > pmf::read_huffman_codes( const std::string &codeword, size_t &pos )
{
	huffman_node< uint8_t > *root;
	std::vector< huffman_node< uint8_t > * > leafs;
	std::map< uint8_t, std::string > map;

	root = reconstruct_huffman_tree( codeword, pos, leafs );

	map = get_huffman_codes_from_tree( root, leafs );

	std::cout << "leafs.size " << leafs.size( ) << std::endl;
	std::cout << "code_map.size " << map.size( ) << std::endl;

	assert( leafs.size() == map.size() );

	free_huffman_tree( root );

	return map;
}

std::string pmf::compress_huffman( const struct matrix *image, rectangle &rect )
{
	std::string first;
	std::string codeword = "";
	std::map< uint8_t, std::string > codes = compute_huffman_codes( );

	if( write_huffman_tree_with_coded_bits ) {
		codeword += huffman_tree_string;
	}

	codeword += pmf::encode( image, codes, rect );

	return codeword;
}

std::string pmf::encode( const struct matrix *image,
		std::map< uint8_t, std::string > &code_map,
		rectangle &rect )
{
	std::string codeword;

	unsigned int i, j;
	std::string first;

	assert( rect.w > 0 );
	assert( rect.h > 0 );

	for( j = rect.point.y; j < ( rect.point.y + rect.h ); j++ ) {
		for( i = rect.point.x; i < ( rect.point.x + rect.w ); i++ ) {

			uint8_t val = image->data[j * image->cols + i];

			std::map< uint8_t, std::string >::iterator temp_it;
			temp_it = code_map.find( val );
			assert( temp_it != code_map.end( ) );

			codeword += ( *temp_it ).second; // concatenate code
			//stream << ( *temp_it ).second << " " << ( int )val << std::endl;

			first += ( *temp_it ).second;
			////if( write_to_file )
			///write_byte_if_ready( first, output_fd );
		}
	}

	/*if( write_to_file )
	 fill_rest_of_byte( first, output_fd );*/

	return codeword;
}

std::string pmf::compress_huffman( const std::vector< uint8_t > &vector )
{
	std::string codeword;
	pmf temp_t = pmf( );
	temp_t.compute_pmf( vector );
	std::cout << temp_t << std::endl;

	codeword += temp_t.huffman_private( vector );
	return codeword;
}

size_t decode( struct matrix *image,
		std::string &codeword,
		size_t p,
		std::map< uint8_t, std::string > code_map,
		rectangle &rect )
{
	size_t pos = p;

	unsigned int i = rect.point.x;
	unsigned int j = rect.point.y;
	unsigned int x_end = rect.point.x + rect.w;
	unsigned int y_end = rect.point.y + rect.h;

	while( pos != codeword.length( ) ) {
		bool found = false;
		uint8_t val = 0;
		for( auto const &mit : code_map ) {
			if( codeword.compare( pos, mit.second.length( ), mit.second ) == 0 ) {
				found = true;
				val = mit.first;
				pos += mit.second.length( );
				std::cout << "pixel in string = " << mit.second << ", and in ulong " << ( int )mit.first
						<< std::endl;
				break;
			}
		}
		assert( found == true );
		image->data[j * image->cols + i] = val;
		i++;
		if( i == x_end ) {
			i = rect.point.x;
			j++;
		}
		if( j == y_end )
			break;
	}
	return pos;
}

size_t pmf::decompress_huffman( struct matrix *image, std::string &codeword, size_t p, rectangle &rect )
{

	size_t pos = p;
	std::map< uint8_t, std::string > code_map = pmf::read_huffman_codes( codeword, pos );

	return decode( image, codeword, pos, code_map, rect );
}

size_t pmf::decompress_huffman( struct matrix *image,
		std::map< uint8_t, std::string > &code_map,
		std::string &codeword,
		size_t p,
		rectangle &rect )
{
	size_t pos = p;

	unsigned int i = rect.point.x;
	unsigned int j = rect.point.y;
	unsigned int x_end = rect.point.x + rect.w;
	unsigned int y_end = rect.point.y + rect.h;

	while( pos != codeword.length( ) ) {
		bool found = false;
		uint8_t val;
		for( auto const &mit : code_map ) {
			if( codeword.compare( pos, mit.second.length( ), mit.second ) == 0 ) {
				found = true;
				val = mit.first;
				//stream << mit.second << " " << ( int )val << std::endl;
				pos += mit.second.length( );
				std::cout << "pixel in string = " << mit.second << ", and in ulong " << ( int )mit.first
						<< std::endl;
				break;
			}
		}
		assert( found == true );
		image->data[j * image->cols + i] = val;
		i++;
		if( i == x_end ) {
			i = rect.point.x;
			j++;
		}
		if( j == y_end )
			break;
	}
	return pos;
}

inline unsigned int log2_of_power_of_2_int( unsigned int n )
{
	if( n == 1 )
		return 0;
	unsigned int log = 0;
	for( ; ( ( n & 1 ) == 0 ) && n != 1; n >>= 1 )
		log++;
	return log;
}

static std::string encode_unary( int number )
{
	std::string str = "0";
	while( number > 0 ) {
		str.insert( 0, "1" );
		number--;
	}
	return str;
}

static std::string get_golomb_rice_code( int number, unsigned int M )
{
	std::string str;

	assert( M > 0 && !(M & (M - 1 ) ) );
	int q = number / M;
	int r = number % M;
	std::string q_str = encode_unary( q );
	std::string r_str = "";
	if( M > 1 ) {
		unsigned int log2_M = log2_of_power_of_2_int( M );
		boost::dynamic_bitset< > bset( log2_M, r );
		to_string( bset, r_str );
	}

	str = q_str + r_str;

	return str;
}

std::map< uint8_t, std::string > pmf::compute_golomb_rice_codes( int M )
{
	std::string str;
	std::map< uint8_t, std::string > code_map;
	for( auto const &it : etable ) {
		uint8_t number = it.first;
		str = get_golomb_rice_code( number, M );

		code_map[number] = str;

		std::cout << str << std::endl;
	}
	return code_map;
}

std::string pmf::compress_golomb_rice( const struct matrix *image, unsigned int M, rectangle &rect )
{
	std::map< uint8_t, std::string > code_map = compute_golomb_rice_codes( M );

	return pmf::encode( image, code_map, rect );
}

unsigned int decode_unary( std::string &codeword, size_t &pos )
{
	unsigned int number = 0;
	while( codeword.at( pos ) == '1' ) {
		number++;
		pos++;

		if( pos >= codeword.length( ) )
			abort( );
	}
	pos++;
	return number;
}

size_t pmf::decompress_golomb_rice( struct matrix *image,
		std::string &codeword,
		size_t p,
		unsigned int K,
		rectangle &rect )
{
	unsigned int M = pow( 2, K );

	unsigned int i = rect.point.x;
	unsigned int j = rect.point.y;

	unsigned int x_end = rect.point.x + rect.w;
	unsigned int y_end = rect.point.y + rect.h;

	size_t pos = p;

	while( pos < codeword.length( ) ) {

		std::string str = "";
		uint8_t val;
		unsigned int q = decode_unary( codeword, pos );

		if( pos >= codeword.length( ) )
			abort( );

		unsigned int r = 0;
		std::string r_str = "";
		if( K > 0 ) {
			r_str = codeword.substr( pos, K );
			r = boost::dynamic_bitset< unsigned char >( r_str ).to_ulong( );
			str += codeword.substr( pos, K );
			pos += K;
		} else {
			abort( );
		}

		val = q * M + r;

		//stream << encode_unary( q ) + r_str << " " << ( int )val << std::endl;

		image->data[j * image->cols + i] = val;
		i++;
		if( i == x_end ) {
			i = rect.point.x;
			j++;
		}
		if( j == y_end )
			break;

	}
	return pos;
}

static unsigned int get_golomb_bits_for_number( int number, unsigned int M )
{
	int q = number / M;
	unsigned int qbits = 1;
	while( q > 0 ) {
		qbits++;
		q--;
	}
	unsigned int rbits = log2_of_power_of_2_int( M );
	return ( qbits + rbits );
}

unsigned int pmf::get_num_of_golomb_rice_compressed_bits( unsigned int M )
{
	unsigned int total_bits = 0;
	for( auto it = etable.begin( ); it != etable.end( ); it++ ) {
		total_bits += get_golomb_bits_for_number( ( *it ).first, M ) * ( *it ).second.get_count( );
	}
	return total_bits;
}

static unsigned int get_exponential_golomb_bits_for_number( int number, unsigned int k );

static std::string get_exponential_golomb_code( int number, unsigned int k )
{
	std::string str;

	double fraction = number / ( double )pow( 2, k );
	unsigned int w = 1 + floor( fraction );
	unsigned int f_k = floor( log2( 1 + fraction ) );
	//std::cerr << "fraction " << fraction << " w " << w << " f_k " << f_k << std::endl;
	std::string q_str = encode_unary( f_k );
	std::string r_str;
	std::string k_str;

	boost::dynamic_bitset< > rset( f_k, w );
	to_string( rset, r_str );

	boost::dynamic_bitset< > bset( k, number );
	to_string( bset, k_str );

	//std::cerr << q_str << " " << r_str << " " << k_str << std::endl;

	str = q_str + r_str + k_str;

	//std::cerr << number << " " << str << " # of bits " << get_exponential_golomb_bits_for_number( number, k ) << std::endl;

	return str;
}

std::map< uint8_t, std::string > pmf::compute_exponential_golomb_codes( int k )
{
	std::string str;
	std::map< uint8_t, std::string > code_map;
	for( auto const &it : etable ) {
		uint8_t number = it.first;
		str = get_exponential_golomb_code( number, k );

		code_map[number] = str;

		std::cout << str << std::endl;
	}
	return code_map;
}

std::string pmf::compress_exponential_golomb( const struct matrix *image, unsigned int k, rectangle &rect )
{
	std::map< uint8_t, std::string > code_map = compute_exponential_golomb_codes( k );

	return pmf::encode( image, code_map, rect );
}

size_t pmf::decompress_exponential_golomb( struct matrix *image,
		std::string &codeword,
		size_t p,
		unsigned int K,
		rectangle &rect )
{
	unsigned int i = rect.point.x;
	unsigned int j = rect.point.y;

	unsigned int x_end = rect.point.x + rect.w;
	unsigned int y_end = rect.point.y + rect.h;

	size_t pos = p;

	while( pos < codeword.length( ) ) {

		std::string str = "";
		uint8_t val;

		unsigned int group_id = decode_unary( codeword, pos );

		if( pos >= codeword.length( ) )
			abort( );

		unsigned int w = 0;
		if( group_id > 0 ) {
			w = boost::dynamic_bitset< unsigned char >( codeword.substr( pos, group_id ) ).to_ulong( );
			str += codeword.substr( pos, K );
			pos += group_id;
		}

		unsigned int index = 0;
		if( K > 0 ) {
			index = boost::dynamic_bitset< unsigned char >( codeword.substr( pos, K ) ).to_ulong( );
			str += codeword.substr( pos, K );
			pos += K;
		}

		unsigned int base = pow( 2, K ) * ( pow( 2, group_id ) - 1 );
		unsigned int middle_base = pow( 2, K ) * ( w );
		//std::cerr << "base " << base << " middle base " << middle_base <<" index " << index << std::endl;
		val = base + middle_base + index;

		image->data[j * image->cols + i] = val;
		i++;
		if( i == x_end ) {
			i = rect.point.x;
			j++;
		}
		if( j == y_end )
			break;

	}
	return pos;
}

static unsigned int get_exponential_golomb_bits_for_number( int number, unsigned int k )
{
	return ( 1 + 2 * floor( log2( 1 + number / double( pow( 2, k ) ) ) ) + k );
}

unsigned int pmf::get_num_of_exponential_golomb_compressed_bits( unsigned int k )
{
	unsigned int total_bits = 0;
	for( auto it = etable.begin( ); it != etable.end( ); it++ ) {
		total_bits += get_exponential_golomb_bits_for_number( ( *it ).first, k ) * ( *it ).second.get_count( );
	}
	return total_bits;
}

unsigned int pmf::calculate_min_k_golomb_rice( )
{
	unsigned int min_bits = UINT_MAX, min_k = UINT_MAX, k;
	for( k = 0; k < 8; k++ ) {
		unsigned int m = pow( 2, k );
		unsigned int bits = get_num_of_golomb_rice_compressed_bits( m );
		unsigned int rbits1 = 0;
		for( auto it = etable.begin( ); it != etable.end( ); it++ ) {
			rbits1 += log2_of_power_of_2_int( m ) * (*it).second.get_count();
		}
		unsigned int rbits2 = total_pixel_count * k;
		assert( rbits1 == rbits2 );

		unsigned int alternative_bits = total_pixel_count * ( 1 + k );
		for( auto it = etable.begin( ); it != etable.end( ); it++ ) {
			alternative_bits += (*it).second.get_count( ) * ( ( *it ).first / m );
		}
		assert( alternative_bits == bits );

		if( bits < min_bits ) {
			min_bits = bits;
			min_k = k;
		}
	}
	assert( 0 <= min_k && min_k <= 7 );
	return min_k;
}

unsigned int pmf::calculate_min_k_exponential_golomb( )
{
	unsigned int min_bits = UINT_MAX, min_k = UINT_MAX, k;
	for( k = 0; k < 8; k++ ) {
		unsigned int bits = get_num_of_exponential_golomb_compressed_bits( k );
		if( bits < min_bits ) {
			min_bits = bits;
			min_k = k;
		}
	}
	assert( 0 <= min_k && min_k < 8 );
	return min_k;
}

double pmf::calculate_probability_diff( pmf &other, bool first_time )
{
	double prob_diff = 0.0;

	for( std::map< int, probability_record >::iterator entropy_table_it = this->etable.begin( );
			entropy_table_it != this->etable.end( ); entropy_table_it++ ) {
		double Probability_1 = ( *entropy_table_it ).second.get_probability( );
		double Probability_2 = 0.0;

		if( other.etable.find( ( *entropy_table_it ).first ) != other.etable.end( ) ) {
			Probability_2 = other.etable[( *entropy_table_it ).first].get_probability( );
		}
		if( first_time )
			std::cout << "1 val = " << ( *entropy_table_it ).first << ", P1 = " << Probability_1 << ", P2 = "
					<< Probability_2 << std::endl;
		prob_diff += fabs( Probability_1 - Probability_2 );
	}

	for( std::map< int, probability_record >::iterator entropy_table_it = other.etable.begin( );
			entropy_table_it != other.etable.end( ); entropy_table_it++ ) {
		double Probability_2 = ( *entropy_table_it ).second.get_probability( );
		double Probability_1 = 0.0;

		/* Only extract if not found in first probability table, since it was extracted in last iteration */
		if( this->etable.find( ( *entropy_table_it ).first ) == this->etable.end( ) ) {
			if( first_time )
				std::cout << "2 val = " << ( *entropy_table_it ).first << ", P1 = " << Probability_1
						<< ", P2 = " << Probability_2 << std::endl;
			Probability_1 = this->etable[( *entropy_table_it ).first].get_probability( );
			prob_diff += fabs( Probability_2 - Probability_1 );
		}
	}
	return prob_diff;
}

void pmf::append_huffman_symbols_to_vector( std::vector< uint8_t > &vector )
{
	assert( write_huffman_tree_with_coded_bits == false );
	vector.insert( vector.end( ), huffman_symbol_vector.begin( ), huffman_symbol_vector.end( ) );
}

void pmf::append_huffman_nodes_to_vector( std::vector< bool > &vector )
{
	assert( write_huffman_tree_with_coded_bits == false );
	vector.insert( vector.end( ), huffman_node_vector.begin( ), huffman_node_vector.end( ) );
}

std::ostream& operator<<( std::ostream& os, const pmf& etable )
{
	int sum = 0;
	std::map< int, probability_record >::const_iterator it;
	probability_record eentry;

	for( it = etable.etable.begin( ); it != etable.etable.end( ); it++ ) {
		int val = ( *it ).first;
		eentry = ( *it ).second;

		//std::cout << (*it).first << ": " << (*it).second << std::endl;
		os << " " << val << ": " << eentry.get_count( ) << " times, Pt=" << std::setprecision( 4 )
				<< std::fixed << eentry.get_probability( ) << ", Ht=" << eentry.get_information( )
				<< std::endl;
		sum += eentry.get_count( );
	}
	os << "total count = " << sum << std::endl;
	return os;
}

void pmf::write_values_to_file( struct matrix *matrix, rectangle &rect, const char *filename )
{
	std::ofstream stream;
	stream.open( filename, std::ofstream::out | std::ofstream::trunc );

	unsigned int i, j;
	for( j = rect.point.y; j < ( rect.point.y + rect.h ); j++ ) {
		for( i = rect.point.x; i < ( rect.point.x + rect.w ); i++ ) {

			uint8_t val = matrix->data[j * matrix->cols + i];

			stream << val;
		}
	}

	stream.close( );
}


void pmf::print_histogram( const char *filename )
{
	probability_record eentry;
	std::map< int, probability_record >::const_iterator it;

	std::ofstream stream;

	remove( filename );

	stream.open( filename, std::ios_base::app );

	std::map< int, probability_record >::iterator eit;
	for( it = etable.begin( ); it != etable.end( ); it++ ) {
		int val = ( *it ).first;
		eentry = ( *it ).second;
		stream << val << " " << eentry.get_count( ) << std::endl;
	}
	stream.close( );
}

void pmf::print_histogram_from_vector( const char *filename, const std::vector< uint8_t > &vector )
{
	std::string codeword;
	pmf temp_t = pmf( );
	temp_t.compute_pmf( vector );
	std::cout << temp_t << std::endl;
	temp_t.print_histogram( filename );
}

std::vector< uint8_t > pmf::get_huffman_symbol_vector( )
{
	return huffman_symbol_vector;
}

void pmf::combine_dummy_probability_distribution( const pmf &table2 )
{
	unsigned int table1_count = 0;
	for( auto const &it1 : etable ) {
		table1_count += it1.second.get_count( );
	}
	assert( table1_count == total_pixel_count );

	unsigned int table2_count = 0;
	for( auto const &it2 : table2.etable ) {
		table2_count += it2.second.get_count( );
	}
	assert( table2_count == table2.total_pixel_count );

	total_pixel_count += table2.total_pixel_count;

	double probability_sum = 0.0;
	for( auto it : etable ) {
		probability_sum += it.second.get_probability( );
		//std::cout << "val = " << it.first << " count " << it.second.get_count( ) << " probability "
		//		<< it.second.get_probability( ) << std::endl;
	}
	//std::cout << "old probability sum " << probability_sum << std::endl;

	for( auto const &it2 : table2.etable ) {
		//std::cout << __func__ << ": val2: " << it2.first << std::endl;
		std::map< int, probability_record >::iterator it1;
		if( ( it1 = etable.find( it2.first ) ) != etable.end( ) ) {
			//std::cout << __func__ << ": val1: " << ( *it1 ).first << ", val2: " << it2.first << std::endl;
			//std::cout << __func__ << ": count1: " << ( *it1 ).second.get_count( ) << ", count2: "
					//<< it2.second.get_count( ) << std::endl;

			unsigned int count = ( *it1 ).second.get_count( );
			count += it2.second.get_count( );
			( *it1 ).second.set_count( count );
		} else {
			etable[it2.first].set_count( it2.second.get_count( ) );
		}
	}

	unsigned int total_count = 0;

	entropy = 0;
	for( auto &it : etable ) {
		//double probability = ( double )( it.second.get_count( ) / ( double )total_pixel_count );
		total_count += it.second.get_count( );
	}
	//std::cout << " total count = " << total_count << std::endl;
	assert( total_count == total_pixel_count );
}

