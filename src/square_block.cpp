/*
 * MIT License
 *
 * Copyright (c) 2013 Sameh Samir Hassan
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <string.h>
#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <map>
#include <assert.h>

#include "pmf.h"
#include "square_block.h"

square_block::square_block( rectangle &sh, struct matrix *matrix, bool combine ) :
		shape( sh ), matrix( matrix ), order( 0 )
{
	if( shape.point.x + shape.w >= matrix->cols ) {
		shape.w = matrix->cols - shape.point.x;
	}
	if( shape.point.y + shape.h >= matrix->rows ) {
		shape.h = matrix->rows - shape.point.y;
	}

	is_combined = combine;
	compute_pmf( matrix, shape, false );

	vector.clear();
	int i, j;
	int x_start = shape.point.x;
	int y_start = shape.point.y;
	int x_end = x_start + shape.w;
	int y_end = y_start + shape.h;
	unsigned int num_of_pixels = ( y_end - y_start ) * ( x_end - x_start );

	for( j = y_start; j < y_end; j++ ) {
		for( i = x_start; i < x_end; i++ ) {
			vector.push_back( matrix->data[j * matrix->cols + i] );
		}
	}
	if( shape.w * shape.h < sh.w * sh.h ) {
		unsigned int np = num_of_pixels;
		while ( np < sh.w * sh.h ) {
			vector.push_back( 0 );
			np++;
		}
	}
}

square_block::square_block( rectangle &sh ) :
		shape( sh ), order( 0 )
{
}

int square_block::calculate_mean( )
{
	int i, j;
	int x_start = shape.point.x;
	int y_start = shape.point.y;
	int x_end = x_start + shape.w;
	int y_end = y_start + shape.h;
	unsigned int sum = 0;
	unsigned int num_of_pixels = ( y_end - y_start ) * ( x_end - x_start );

	/*std::cout << "x start = " << x_start << ", x end = " << x_end << std::endl;
	 std::cout << "y start = " << y_start << ", y end = " << y_end << std::endl;
	 std::cout << "num_of_pixels = " << num_of_pixels << std::endl;*/

	for( j = y_start; j < y_end; j++ ) {
		for( i = x_start; i < x_end; i++ ) {
			//std::cout << " " << ( int )matrix[j * cols + i] << std::endl;
			sum += matrix->data[j * matrix->cols + i];
		}
	}

	return std::min( ( unsigned int )255, sum / num_of_pixels );
}

void square_block::recalculate_pmf( )
{
	compute_pmf( matrix, shape, false );
}

void square_block::subtract_mean( )
{
	uint8_t mean;

	mean = calculate_mean( );
	std::cout << "mean = " << ( int )mean << std::endl;
	int i, j;
	int x_start = shape.point.x;
	int y_start = shape.point.y;
	int x_end = x_start + shape.w;
	int y_end = y_start + shape.h;

	for( j = y_start; j < y_end; j++ ) {
		for( i = x_start; i < x_end; i++ ) {
			matrix->data[j * matrix->cols + i] = ( uint8_t )( matrix->data[j * matrix->cols + i] - mean );
		}
	}
}

void square_block::subtract_mode( )
{
	uint8_t mode, num_of_modes;

	num_of_modes = get_number_of_modes( );
	mode = get_mode( );
	std::cout << "number of modes = " << ( int )num_of_modes << std::endl;
	std::cout << "mode = " << ( int )mode << std::endl;
	int i, j;
	int x_start = shape.point.x;
	int y_start = shape.point.y;
	int x_end = x_start + shape.w;
	int y_end = y_start + shape.h;

	for( j = y_start; j < y_end; j++ ) {
		for( i = x_start; i < x_end; i++ ) {
			//if( num_of_modes > 1 ) {
			//}
			matrix->data[j * matrix->cols + i] = ( uint8_t )( matrix->data[j * matrix->cols + i] - mode );
			std::cout << std::setw( 3 ) << " " << ( int )matrix->data[j * matrix->cols + i];

		}
		//if( num_of_modes > 1 )
		std::cout << std::endl;
	}
}

void square_block::subtract_median( )
{
	uint8_t median;

	median = get_median( );
	std::cout << "mode = " << ( int )median << std::endl;
	int i, j;
	int x_start = shape.point.x;
	int y_start = shape.point.y;
	int x_end = x_start + shape.w;
	int y_end = y_start + shape.h;

	for( j = y_start; j < y_end; j++ ) {
		for( i = x_start; i < x_end; i++ ) {
			std::cout << std::setw( 3 ) << " " << ( int )matrix->data[j * matrix->cols + i];
			matrix->data[j * matrix->cols + i] = ( uint8_t )( matrix->data[j * matrix->cols + i] - median );
		}
		std::cout << std::endl;
	}
}

void square_block::subtract_diff( )
{
	//bool first = true;
	uint8_t last_val = 0;

	int i, j;
	int x_start = shape.point.x;
	int y_start = shape.point.y;
	int x_end = x_start + shape.w;
	int y_end = y_start + shape.h;

	for( j = y_start; j < y_end; j++ ) {
		//if( !first )
		//last_val = matrix[j * cols + i];
		//last_val = 0;
		for( i = x_start + 1; i < x_end; i++ ) {
			uint8_t val = matrix->data[j * matrix->cols + i];
			matrix->data[j * matrix->cols + i] = ( uint8_t )( val - last_val );
			last_val = val;
		}
	}
}

void square_block::encode_bwt( )
{
	compress_burrows_wheeler_transform( matrix, shape );
}

void square_block::verify( unsigned int *verification_matrix )
{
	int i;
	int j;
	int x_start = shape.point.x;
	int y_start = shape.point.y;
	int x_end = x_start + shape.w;
	int y_end = y_start + shape.h;

	for( j = y_start; j < y_end; j++ ) {
		for( i = x_start; i < x_end; i++ ) {
			verification_matrix[j * matrix->cols + i]++;
		}
	}

}

#ifdef ENABLE_SQUARE_NEIGHBOUR
void square_block::add_neighbour( square_block *square )
{
	entropy_neighbour nbour;
	std::cout << __func__ << ": x = " << square->shape.point.x << ", y = " << square->shape.point.y
			<< std::endl;
	nbour.neighbour = square;
	std::vector< pmf *> pvec;
	pvec.push_back( square );
	pvec.push_back( this );
	nbour.jsd = pmf::calculate_jensen_shannon_divergence( pvec );
	neighbours.push_back( nbour );
	assert( square->shape.point.x < matrix->cols );
}
#endif

rectangle &square_block::get_shape( )
{
	return shape;
}

struct matrix *square_block::get_matrix( )
{
	assert( matrix != NULL );
	return matrix;
}

void square_block::set_combined( bool combine )
{
	is_combined = combine;
}

bool square_block::get_combined( ) const
{
	return is_combined;
}

void square_block::set_container( void *ptr )
{
	container = ptr;
}

void *square_block::get_container( )
{
	return container;
}

