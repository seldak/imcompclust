/*
 * MIT License
 *
 * Copyright (c) 2014 Sameh Samir Hassan
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef COMPRESSION_BLOCK_H_
#define COMPRESSION_BLOCK_H_

#include <list>
#include <string>
#include "pmf.h"
#include "edge.h"
#include "square_block.h"

#include <eigen3/Eigen/Dense>

class pos_2d;
class pmf;

class compression_block: public pmf {
public:
	compression_block( struct matrix *matrix, square_block * square, unsigned int *vmatrix );
	compression_block( square_block * square, unsigned int *vmatrix, unsigned int order );

	void set_combined( bool status );

#ifdef USE_EDGES_IN_COMPRESSION_BLOCKS
	void remove_inner_edges( );
#endif

	void add_cb( const compression_block &cb );
	void add_square( square_block *sq );

#ifdef USE_EDGES_IN_COMPRESSION_BLOCKS
	std::list< edge > &get_edges( );
#endif

	std::list< square_block * > get_blocks( ) const;

	void sort_squares( );
	void write_to_file( uint8_t *matrix, unsigned int rows, unsigned int cols );

	std::string cb_compress_huffman( bool write_tree );
	std::size_t cb_decompress_huffman( std::string &codeword, size_t pos, struct matrix *image );

	std::string cb_compress_golomb_rice( unsigned int M );
	std::size_t cb_decompress_golomb_rice( std::string &codeword,
			size_t pos,
			unsigned int K,
			struct matrix *image );

	std::string cb_compress_exponential_golomb( unsigned int k );
	std::size_t cb_decompress_exponential_golomb( std::string &codeword,
			size_t pos,
			unsigned int K,
			struct matrix *mat );

	void verify( );

	size_t get_number_of_blocks( ) const;

	pos_2d get_first_block_pos( );

	std::string encode_block_size( int num_of_bits );

	void set_k( unsigned int K )
	{
		k = K;
	}
	unsigned int get_k( ) const
	{
		return k;
	}

	friend std::ostream& operator<<( std::ostream& os, const compression_block& cb );

	bool operator==( const compression_block &other ) const;

	static inline bool less_blocks( const compression_block *l, const compression_block *r )
	{
		if( l->get_number_of_blocks( ) < r->get_number_of_blocks( ) ) {
			return true;
		} else if( l->get_number_of_blocks( ) == r->get_number_of_blocks( ) ) {
			square_block *ls = *( l->get_blocks( ).begin( ) );
			square_block *rs = *( r->get_blocks( ).begin( ) );
			pos_2d lp = ls->get_shape( ).point;
			pos_2d rp = rs->get_shape( ).point;
			if( lp < rp )
				return true;
		}
		return false;
	}

	void set_order( unsigned int o )
	{
		order = o;
		if( blocks.size( ) == 1 ) {
			auto b = blocks.begin( );
			( *b )->set_order( o );
		}
	}

	void set_internal_order( )
	{
		int order = 0;
		for( auto sq : blocks ) {
			sq->set_order( order );
			order++;
		}
	}

	unsigned int get_order( )
	{
		return order;
	}

	static double find_modified_max_dissimilarity( compression_block *cb1,
			compression_block *cb2,
			Eigen::Matrix< double, Eigen::Dynamic, Eigen::Dynamic > &proximity_matrix );
	static double find_modified_max_dissimilarity( compression_block *cb1,
			compression_block *cb2,
			double (*dissimilarity)( const pmf * const p1, const pmf * const p2 ) );
	static double find_modified_average_dissimilarity( compression_block *cb1,
			compression_block *cb2,
			Eigen::MatrixXd& proximity_matrix );
	static double find_modified_average_dissimilarity( compression_block *cb1,
			compression_block *cb2,
			double (*dissimilarity)( const pmf * const p1, const pmf * const p2 ) );

	void add_cb_and_update_max_dissimilarity( const compression_block &cb,
			const Eigen::MatrixXd& proximity_matrix );
	void add_cb_and_update_sum_dissimilarity( compression_block &cb,
			const Eigen::MatrixXd& proximity_matrix );

	void add_cb_and_update_max_dissimilarity( const compression_block &cb,
			double (*dissimilarity)( const pmf * const p1, const pmf * const p2 ) );
	void add_cb_and_update_sum_dissimilarity( compression_block &cb,
			double (*dissimilarity)( const pmf * const p1, const pmf * const p2 ) );

	void calculate_max_dissimilarity( double (*dissimilarity)( const pmf * const p1, const pmf * const p2 ) );
	void calculate_sum_dissimilarity( double (*dissimilarity)( const pmf * const p1, const pmf * const p2 ) );

	square_block *find_centroid( const Eigen::MatrixXd& proximity_matrix );

	static double find_min_dissimilarity( compression_block *cb1,
			compression_block *cb2,
			Eigen::MatrixXd& proximity_matrix );
	static double find_max_dissimilarity( compression_block *cb1,
			compression_block *cb2,
			Eigen::MatrixXd& proximity_matrix );
	static double find_average_dissimilarity( compression_block *cb1,
			compression_block *cb2,
			Eigen::MatrixXd& proximity_matrix );
	static double find_centroid_dissimilarity( compression_block *cb1,
			compression_block *cb2,
			Eigen::MatrixXd& proximity_matrix );
	static double find_ward_dissimilarity( compression_block *cb1,
			compression_block *cb2,
			Eigen::MatrixXd& proximity_matrix );
	void add_cb_and_update_centroid( compression_block &cb, const Eigen::MatrixXd& proximity_matrix );

	void fill_proximity_matrix_for_blocks( Eigen::MatrixXd &proximity_matrix,
			double (*dissimilarity)( const pmf * const p1, const pmf * const p2 ) );
	compression_block *split_node_with_highest_average_dissimilarity( const Eigen::MatrixXd &proximity_matrix );

	void migrate_nodes_macnaughton_smith( compression_block *new_cluster,
			const Eigen::MatrixXd &proximity_matrix,
			double (*dissimilarity)( const pmf * const p1, const pmf * const p2 ) );

private:
	void print_compressed_length( const size_t &length );
	static double find_sum_dissimilarities_stride( square_block *sq,
			compression_block const * const cb,
			double (*dissimilarity)( const pmf * const p1, const pmf * const p2 ) );

	unsigned int k;

	std::list< square_block * > blocks;
#ifdef USE_EDGES_IN_COMPRESSION_BLOCKS
	std::list< edge > edges;
#endif
	unsigned int *verification_matrix;

	unsigned int order;

	union {
		double max_dissimilarity;
		double sum_dissimilarity;
		square_block *centroid;
	};
};

struct less_than_blocks {
	inline bool operator()( const compression_block& l, const compression_block& r )
	{
		if( l.get_number_of_blocks( ) < r.get_number_of_blocks( ) ) {
			return true;
		} else if( l.get_number_of_blocks( ) == r.get_number_of_blocks( ) ) {
			square_block *ls = *( l.get_blocks( ).begin( ) );
			square_block *rs = *( r.get_blocks( ).begin( ) );
			pos_2d lp = ls->get_shape( ).point;
			pos_2d rp = rs->get_shape( ).point;
			if( lp < rp )
				return true;
		}
		return false;
	}
};

#endif /* COMPRESSION_BLOCK_H_ */
