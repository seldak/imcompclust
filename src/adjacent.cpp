/*
 * MIT License
 *
 * Copyright (c) 2014 Sameh Samir Hassan
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <vector>
#include <iostream>
#include <fstream>

#include <opencv2/highgui/highgui.hpp>

#include "pmf.h"
#include "square_block.h"
#include "pos_2d.h"
#include "edge.h"
#include "compression_block.h"
#include "algo.h"
#include "aut.h"
#include "adjacent.h"
#include "enums.h"
#include "quadtree.h"

void compression_algorithm_adjacent::cluster_squares_raster( )
{
	unsigned int x, y;

	for( y = 0; y != image.square_grid.size( ); y++ ) {
		for( x = 0; x != image.square_grid[y].size( ); x++ ) {

			if( image.square_grid[y][x]->get_combined( ) ) {
				std::cout << "this block is already combined, skipping" << std::endl;
				continue;
			}
			compression_block *cb =
					static_cast< compression_block * >( image.square_grid[y][x]->get_container( ) );
			attempt_square_combining( cb, &image.mat, x, y );
		}
	}
}

void compression_algorithm_adjacent::cluster( )
{
	compressed_length = 0;

	unsigned int max_span = quadtree::get_max_quadtree_span( best_scale, image.mat.rows, image.mat.cols );
	pos_2d pos;
	pos.y = pos.x = 0;
	quadtree *root = new quadtree( pos, max_span );
	root->initialize_tree_down_to_scale( image.square_grid, best_scale, image.mat.rows, image.mat.cols );
	quadtree::attempt_node_combining( root, quadtree::divergence_check, quadtree::combine_cbs );
	std::list< compression_block *> list;
	quadtree::add_leafs_to_list( root, list );
	std::cout << "leafs = " << root->count_leafs( ) << " num of cbrs = " << list.size( ) << std::endl;
	//compressed_output += list.size( );

	compression_blocks = list;

	quadtree *new_root = quadtree::copy_node( root );
	quadtree::attempt_node_combining( new_root, quadtree::scale_check, quadtree::combine_nodes );
	cv::Mat new_mat = image.cv_mat.clone( );
	quadtree::draw_quadtree_node( new_mat, new_root );
	unsigned int leaf_count = new_root->count_leafs( );
	std::cout << "new leafs = " << leaf_count << std::endl;
	unsigned int max_tree_span = new_root->find_maximum_node_span( 0 );
	std::cout << "maximum_span = " << max_tree_span / best_scale << " log = "
			<< ceil( log2( ( max_tree_span - 1 ) / best_scale ) ) << std::endl;
	std::cout << "total nodes =" << new_root->count_total_nodes( ) << std::endl;

	if( coding == GOLOMB_RICE || coding == EXP_GOLOMB ) {
		for( auto it : compression_blocks ) {
			unsigned int min_k;
			if( coding == GOLOMB_RICE ) {
				min_k = it->calculate_min_k_golomb_rice( );
			} else {
				min_k = it->calculate_min_k_exponential_golomb( );
			}
			it->set_k( min_k );
		}
	}

	quadtree *k_root = quadtree::copy_node( root );
	quadtree::attempt_node_combining( new_root, quadtree::k_check, quadtree::combine_nodes );
	cv::Mat k_mat = image.cv_mat.clone( );
	quadtree::draw_quadtree_node( k_mat, k_root );

	cv::namedWindow( "Quadtree", CV_WINDOW_NORMAL );
	cv::imshow( "Quadtree", new_mat );
	cv::namedWindow( "K Quadtree", CV_WINDOW_NORMAL );
	cv::imshow( "K Quadtree", k_mat );
	cv::imwrite( "adjacent_quadtree_size_golomb.png", new_mat );

#if 0
	cv::Mat other_mat = in_mat.clone( );
	for( y = 0; y < rows; y++ ){
		cv::Point start = cv::Point( 0, y*best_scale );
		cv::Point end = cv::Point( cols, y*best_scale );
		cv::line( other_mat, start, end, cv::Scalar( 0, 0, 255 ), 1, 8 );
	}
	for( x = 0; x < rows; x++ ){
		cv::Point start = cv::Point( x*best_scale, 0 );
		cv::Point end = cv::Point( x*best_scale, rows );
		cv::line( other_mat, start, end, cv::Scalar( 0, 0, 255 ), 1, 8 );
	}
	cv::namedWindow( "Partition", CV_WINDOW_NORMAL );
	cv::imshow( "Partition", other_mat );
	cv::imwrite( "partitioned.png", other_mat );
#endif

	cv::waitKey( 0 );
}

void compression_algorithm_adjacent::add_cb_to_list_if_unique( std::list< compression_block * > &cb_list,
		compression_block *cb )
{
	for( std::list< compression_block * >::iterator cit = cb_list.begin( ); cit != cb_list.end( ); cit++ ) {
		if( cb == *cit ) {
			std::cout << "Found" << std::endl;
			return;
		}
	}
	cb_list.push_back( cb );
}

void compression_algorithm_adjacent::attempt_square_combining( compression_block *cb,
		struct matrix *matrix,
		unsigned int i,
		unsigned int j )
{
	bool done = false;
	square_block *square = image.square_grid[j][i];
	const rectangle &shape = square->get_shape( );
	int level = 1;
	std::cout << square->get_shape( ) << std::endl;
	std::cout << "number of values = " << square->get_histogram_bins( ) << std::endl;

	std::cout << "[" << i << "][" << j << "]" << std::endl;

	unsigned int low_x = shape.point.x + shape.w;
	unsigned int low_y = shape.point.y + shape.h;

	while( !done ) {
		std::list< compression_block * > cb_list;
		std::vector< pmf * > distribution_vec;

		if( low_x >= image.mat.cols - 1 || low_y >= image.mat.rows - 1 ) {
			std::cout << "boundaries are exceeded" << std::endl;
			return;
		}

		/*
		 * X ||
		 * --||
		 * ---|
		 *
		 * es_grid[j  ][i+1]
		 * es_grid[j+1][i+1]
		 * es_grid[j+1][i  ]
		 *
		 * es_grid[j  ][i+2]
		 * es_grid[j+1][i+2]
		 * es_grid[j+2][i+2]
		 * es_grid[j+2][i+1]
		 * es_grid[j+2][i  ]
		 *
		 */

		for( int j_it = 0; j_it < level; j_it++ ) {
			square_block *neighbour = image.square_grid[j + j_it][i + level];
			if( neighbour->get_combined( ) ) {
				std::cout << "this neighbour block is already combined, skipping" << std::endl;
				return;
			}
			distribution_vec.push_back( neighbour );
			add_cb_to_list_if_unique( cb_list,
					static_cast< compression_block* >( neighbour->get_container( ) ) );
			std::cout << "[" << i + j_it << "][" << j + level << "]" << std::endl;
		}

		{
			square_block *neighbour = image.square_grid[j + level][i + level];
			if( neighbour->get_combined( ) ) {
				std::cout << "this neighbour block is already combined, skipping" << std::endl;
				return;
			}
			distribution_vec.push_back( neighbour );
			add_cb_to_list_if_unique( cb_list,
					static_cast< compression_block* >( neighbour->get_container( ) ) );
			std::cout << "[" << i + level << "][" << j + level << "]" << std::endl;
		}

		for( int i_it = 0; i_it < level; i_it++ ) {
			square_block *neighbour = image.square_grid[j + level][i + i_it];
			if( neighbour->get_combined( ) ) {
				std::cout << "this neighbour block is already combined, skipping" << std::endl;
				return;
			}
			distribution_vec.push_back( neighbour );
			add_cb_to_list_if_unique( cb_list,
					static_cast< compression_block* >( neighbour->get_container( ) ) );
			std::cout << "[" << i + i_it << "][" << j + level << "]" << std::endl;
		}

		distribution_vec.push_back( cb );
		if( pmf::calculate_jensen_shannon_divergence( distribution_vec ) < JSD_THRESHOLD ) {
			//if( square.do_tables_match( *neighbour ) ) {
			std::cout << "JSD meets criteria, trying to add" << std::endl;

			std::cout << "number of blocks " << cb_list.size( ) << std::endl;
			for( auto cit = cb_list.begin( ); cit != cb_list.end( ); cit++ ) {
				std::cout << "number of squares " << ( *cit )->get_number_of_blocks( ) << std::endl;
				cb->add_cb( *( *cit ) );
				std::list< compression_block * >::iterator other = std::find( compression_blocks.begin( ),
						compression_blocks.end( ), *cit );
				if( other == compression_blocks.end( ) ) {
					std::cerr << "ERROR, could not find pointer in compression blocks" << std::endl;
				}
				compression_blocks.erase( other );
				delete *cit;
			}

			cb->set_combined( true );

			level++;

			/* XXX */
			low_x += shape.w;
			low_y += shape.h;

		} else {
			std::cout << "JSD doesn't meet criteria, skipping" << std::endl;
			done = true;
		}
	}
}

