/*
 * MIT License
 *
 * Copyright (c) 2014 Sameh Samir Hassan
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef ALGO_H_
#define ALGO_H_

#include "enums.h"
#include "utils.h"
#include <opencv2/core/core.hpp>

extern void copy_original_matrix_contents( uint8_t *dest_matrix,
		uint8_t *src_matrix,
		unsigned int rows,
		unsigned int cols );
extern void copy_opencv_matrix_into_array( struct matrix *matrix, cv::Mat &input );

extern void subtract_left( struct matrix *matrix, bool encode_for_geometric_distribution );
extern void a_b_average_filter( struct matrix *matrix, bool encode_for_geometric_distribution );
extern void paeth_filter( struct matrix *matrix, bool encode_for_geometric_distribution );

extern void reverse_subtract_left( struct matrix *mat, bool revert_for_geometric_distribution );
extern void reverse_a_b_average_filter( struct matrix *mat, bool revert_for_geometric_distribution );
extern void reverse_paeth_filter( struct matrix *mat, bool revert_for_geometric_distribution );

class image_data {
public:
	image_data( ) :
			mat( )
	{
		verification_matrix = NULL;
		square_grid.clear( );
	}

	image_data( unsigned int rows, unsigned int cols, compression_filter pre_filter ) :
			mat( rows, cols ), pre_filter( pre_filter )
	{
		cv_mat.create( rows, cols, CV_8UC1 );
		size_t size = rows * cols * sizeof(unsigned int);
		assert( posix_memalign( (void **)&verification_matrix, 4096, size ) == 0 );
		memset( verification_matrix, 0, size );

		square_grid.clear( );
	}

	image_data( cv::Mat &cv_mat, compression_filter pre_filter ) :
			cv_mat( cv_mat.clone( ) ), mat( cv_mat.rows, cv_mat.cols ), pre_filter( pre_filter )
	{
		copy_opencv_matrix_into_array( &mat, cv_mat );

		size_t size = mat.rows * mat.cols * sizeof(unsigned int);
		assert( posix_memalign( (void **)&verification_matrix, 4096, size ) == 0 );
		memset( verification_matrix, 0, size );

		square_grid.clear( );
	}

	~image_data( )
	{
		if( verification_matrix != NULL ) {
			free( verification_matrix );
			verification_matrix = NULL;
		}
	}

	void pre_process_filter( )
	{
		if( pre_filter == FILTER_NONE ) {
			return;
		} else if( pre_filter == FILTER_SUBTRACT_LEFT ) {
			subtract_left( &mat, true );
		} else if( pre_filter == FILTER_MEAN ) {
			a_b_average_filter( &mat, true );
		} else if( pre_filter == FILTER_PAETH ) {
			paeth_filter( &mat, true );
		}

		//print_matrix( std::cout, mat.data, mat.rows, mat.cols );
	}

	void revert_filter( )
	{
		if( pre_filter == FILTER_NONE ) {
			return;
		} else if( pre_filter == FILTER_SUBTRACT_LEFT ) {
			reverse_subtract_left( &mat, true );
		} else if( pre_filter == FILTER_MEAN ) {
			reverse_a_b_average_filter( &mat, true );
		} else if( pre_filter == FILTER_PAETH ) {
			reverse_paeth_filter( &mat, true );
		}

		//print_matrix( std::cout, decompressed_output, rows, cols );
	}

	void verify_cb_traversing( )
	{
		unsigned int uncovered = 0;
		unsigned int overcovered = 0;
		unsigned int covered = 0;
		for( unsigned int j = 0; j < mat.rows; j++ ) {
			for( unsigned int i = 0; i < mat.cols; i++ ) {
				if( verification_matrix[j * mat.cols + i] == 0 )
					uncovered++;
				else if( verification_matrix[j * mat.cols + i] > 1 )
					overcovered++;
				else
					covered++;
			}
		}

		std::cout << "covered " << covered << ", uncovered  = " << uncovered << ", overcovered = " << overcovered
				<< std::endl;
		//assert( uncovered == 0 );
		//assert( overcovered == 0 );
	}

	cv::Mat cv_mat;
	struct matrix mat;
	std::vector< std::vector< square_block* > > square_grid;
	compression_filter pre_filter;
	unsigned int *verification_matrix;
};

class compression_algorithm {
public:
	compression_algorithm( cv::Mat &in_mat, compression_filter pre_filter ) :
			image( in_mat, pre_filter )
	{
	}
	~compression_algorithm( )
	{
	}

	virtual std::string compress( ) = 0;

	void set_filename( std::string name )
	{
		filename = name;
	}
	const std::string &get_filename( )
	{
		return filename;
	}

	uint8_t *get_matrix( )
	{
		return image.mat.data;
	}

protected:
	class image_data image;
	std::string filename;
};

#endif /* ALGO_H_ */
