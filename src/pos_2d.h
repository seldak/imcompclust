/*
 * MIT License
 *
 * Copyright (c) 2013 Sameh Samir Hassan
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef POS_2D_H_
#define POS_2D_H_

#include <ostream>

struct pos_2d {
	unsigned int x;
	unsigned int y;

	pos_2d( ) :
			x( 0 ), y( 0 )
	{
	}
	pos_2d( const pos_2d &pos ) :
			x( pos.x ), y( pos.y )
	{
	}
	pos_2d( unsigned int x, unsigned int y ) :
			x( x ), y( y )
	{
	}

	inline bool operator==( const pos_2d& other )
	{
		return ( ( x == other.x ) && ( y == other.y ) );
	}
	inline bool operator!=( const pos_2d& other )
	{
		return !operator==( other );
	}
	inline bool operator<( const pos_2d& other ) const
	{
		if( y < other.y )
			return true;
		else if( y == other.y && x < other.x )
			return true;
		return false;
	}

	friend std::ostream& operator<<( std::ostream& os, const pos_2d& pos )
	{
		os << "[" << pos.x << "," << pos.y << "] ";
		return os;
	}
};

struct rectangle {
	pos_2d point;
	unsigned int w;
	unsigned int h;

	rectangle( ): w( 0 ), h( 0 )
	{
	}

	rectangle( const pos_2d &p, unsigned int w, unsigned int h ) :
			point( p ), w( w ), h( h )
	{

	}

	inline bool operator==( const rectangle& other )
	{
		return ( point == other.point && w == other.w && h == other.h );
	}
	inline bool operator!=( const rectangle& other )
	{
		return !operator==( other );
	}
	friend std::ostream& operator<<( std::ostream& os, const rectangle& shape )
	{
		os << shape.point << " w=" << shape.w << ", h=" << shape.h;
		return os;
	}
};

#endif /* POS_2D_H_ */
