/*
 * MIT License
 *
 * Copyright (c) 2014 Sameh Samir Hassan
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef UTILS_H_
#define UTILS_H_

#include <ostream>
#include <iomanip>
#include <ctime>

template< class type >
void print_matrix( std::ostream &ostream, type *matrix, unsigned int rows, unsigned int cols )
{
	ostream << __func__ << std::endl;
	size_t x, y;
	ostream << "\t";
	for( x = 0; x < cols; x++ )
		ostream << std::setw( 2 ) << x << " ";
	ostream << std::endl << std::endl;
	for( y = 0; y < rows; y++ ) {
		ostream << y << "\t";
		for( x = 0; x < cols; x++ ) {
			ostream << std::setw( 2 ) << ( int )matrix[y * rows + x] << " ";
		}
		ostream << std::endl;
	}
}

extern struct timespec timespec_diff( struct timespec &start, struct timespec &end );
extern std::ostream& operator<<( std::ostream& os, const struct timespec& t );

#define START_PROFILING \
do {  \
	clock_gettime( CLOCK_MONOTONIC, &start ); \
} while(0)

#define END_PROFILING(str) \
do{ \
	clock_gettime( CLOCK_MONOTONIC, &end );\
	diff = timespec_diff( start, end );\
	std::cout << str << " " << diff << std::endl;\
} while( 0 )


void print_length( const char *str, size_t length );
void print_length_with_bpp( const char *str, size_t length, size_t orig_length );

#define likely(x)      __builtin_expect(!!(x), 1)
#define unlikely(x)    __builtin_expect(!!(x), 0)

#endif /* UTILS_H_ */
