/*
 * MIT License
 *
 * Copyright (c) 2014 Sameh Samir Hassan
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <iostream>
#include <stdint.h>
#include <bitset>
#include <boost/dynamic_bitset.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <fstream>
#include <time.h>
#include <boost/filesystem.hpp>

#include "pos_2d.h"
#include "edge.h"
#include "pmf.h"
#include "square_block.h"
#include "compression_block.h"
#include "aut.h"
#include "quadtree.h"
#include "utils.h"

struct timespec timespec_diff( struct timespec &start, struct timespec &end )
{
	struct timespec temp;
	if( ( end.tv_nsec - start.tv_nsec ) < 0 ) {
		temp.tv_sec = end.tv_sec - start.tv_sec - 1;
		temp.tv_nsec = 1000000000 + end.tv_nsec - start.tv_nsec;
	} else {
		temp.tv_sec = end.tv_sec - start.tv_sec;
		temp.tv_nsec = end.tv_nsec - start.tv_nsec;
	}
	return temp;
}

std::ostream& operator<<( std::ostream& os, const struct timespec& t )
{
	os << "secs " << t.tv_sec << ", nsecs " << t.tv_nsec;
	return os;
}


extern void copy_array_into_opencv_matrix( cv::Mat &mat, struct matrix *matrix );
extern double calculate_s_weight( std::vector< std::vector< square_block * > > & es_grid,
		unsigned int s,
		struct matrix *matrix );

#define HORIZONTAL_NEIGHBOR 1
#define SKEW_NEIGHBOR    1
#define VERTICAL_NEIGHBOR 1

#ifdef ENABLE_SQUARE_NEIGHBOUR
static void add_neighbors( std::vector< std::vector< square_block * > > & grid,
		const unsigned int scale,
		unsigned int rows,
		unsigned int cols )
{
	unsigned int x;
	unsigned int y;

	for( y = 0; y != grid.size( ); y++ ) {
		for( x = 0; x != grid[y].size( ); x++ ) {
			std::cout << " x = " << x * scale << ", y = " << y * scale << std::endl;

			square_block *current_square = grid[y][x];
			rectangle &current_shape = current_square->get_shape( );
			if( current_shape.point.y == 0 && current_shape.point.x == 0 ) {
				/* top left corner */
#ifdef HORIZONTAL_NEIGHBOR
				current_square->add_neighbour( grid[y + 1][x] );
#endif
#ifdef SKEW_NEIGHBOR
				current_square->add_neighbour( grid[y + 1][x + 1] );
#endif
#ifdef VERTICAL_NEIGHBOR
				current_square->add_neighbour( grid[y][x + 1] );
#endif

			} else if( ( current_shape.point.y + scale ) >= rows - 1
					&& ( current_shape.point.x + scale ) >= cols - 1 ) {
				/* bottom right corner */
#ifdef HORIZONTAL_NEIGHBOR
				current_square->add_neighbour( grid[y - 1][x] );
#endif
#ifdef SKEW_NEIGHBOR
				current_square->add_neighbour( grid[y - 1][x - 1] );
#endif
#ifdef VERTICAL_NEIGHBOR
				current_square->add_neighbour( grid[y][x - 1] );
#endif

			} else if( current_shape.point.y == 0 && ( current_shape.point.x + scale ) >= cols - 1 ) {
				/* bottom left corner */
#ifdef VERTICAL_NEIGHBOR
				current_square->add_neighbour( grid[y][x - 1] );
#endif
#ifdef SKEW_NEIGHBOR
				current_square->add_neighbour( grid[y + 1][x - 1] );
#endif
#ifdef HORIZONTAL_NEIGHBOR
				current_square->add_neighbour( grid[y + 1][x] );
#endif

			} else if( ( current_shape.point.y + scale ) >= rows - 1 && current_shape.point.x == 0 ) {
				/* top right corner */
#ifdef HORIZONTAL_NEIGHBOR
				current_square->add_neighbour( grid[y - 1][x] );
#endif
#ifdef SKEW_NEIGHBOR
				current_square->add_neighbour( grid[y - 1][x + 1] );
#endif
#ifdef VERTICAL_NEIGHBOR
				current_square->add_neighbour( grid[y][x + 1] );
#endif
			} else if( current_shape.point.y == 0 ) {
#ifdef VERTICAL_NEIGHBOR
				current_square->add_neighbour( grid[y][x - 1] );
#endif
#ifdef SKEW_NEIGHBOR
				current_square->add_neighbour( grid[y + 1][x - 1] );
#endif
#ifdef HORIZONTAL_NEIGHBOR
				current_square->add_neighbour( grid[y + 1][x] );
#endif
#ifdef SKEW_NEIGHBOR
				current_square->add_neighbour( grid[y + 1][x + 1] );
#endif
#ifdef VERTICAL_NEIGHBOR
				current_square->add_neighbour( grid[y][x + 1] );
#endif

			} else if( current_shape.point.x == 0 ) {
#ifdef HORIZONTAL_NEIGHBOR
				current_square->add_neighbour( grid[y - 1][x] );
#endif
#ifdef SKEW_NEIGHBOR
				current_square->add_neighbour( grid[y - 1][x + 1] );
#endif
#ifdef VERTICAL_NEIGHBOR
				current_square->add_neighbour( grid[y][x + 1] );
#endif
#ifdef SKEW_NEIGHBOR
				current_square->add_neighbour( grid[y + 1][x + 1] );
#endif
#ifdef HORIZONTAL_NEIGHBOR
				current_square->add_neighbour( grid[y + 1][x] );
#endif
			} else if( ( current_shape.point.y + scale ) >= rows - 1 ) {
#ifdef VERTICAL_NEIGHBOR
				current_square->add_neighbour( grid[y][x - 1] );
#endif
#ifdef SKEW_NEIGHBOR
				current_square->add_neighbour( grid[y - 1][x - 1] );
#endif
#ifdef HORIZONTAL_NEIGHBOR
				current_square->add_neighbour( grid[y - 1][x] );
#endif
#ifdef SKEW_NEIGHBOR
				current_square->add_neighbour( grid[y - 1][x + 1] );
#endif
#ifdef VERTICAL_NEIGHBOR
				current_square->add_neighbour( grid[y][x + 1] );
#endif
			} else if( ( current_shape.point.x + scale ) >= cols - 1 ) {
#ifdef HORIZONTAL_NEIGHBOR
				current_square->add_neighbour( grid[y - 1][x] );
#endif
#ifdef SKEW_NEIGHBOR
				current_square->add_neighbour( grid[y - 1][x - 1] );
#endif
#ifdef VERTICAL_NEIGHBOR
				current_square->add_neighbour( grid[y][x - 1] );
#endif
#ifdef SKEW_NEIGHBOR
				current_square->add_neighbour( grid[y + 1][x - 1] );
#endif
#ifdef HORIZONTAL_NEIGHBOR
				current_square->add_neighbour( grid[y + 1][x] );
#endif
			} else {
#ifdef SKEW_NEIGHBOR
				current_square->add_neighbour( grid[y - 1][x - 1] );
#endif
#ifdef VERTICAL_NEIGHBOR
				current_square->add_neighbour( grid[y][x - 1] );
#endif
#ifdef SKEW_NEIGHBOR
				current_square->add_neighbour( grid[y + 1][x - 1] );
#endif
#ifdef HORIZONTAL_NEIGHBOR
				current_square->add_neighbour( grid[y + 1][x] );
#endif
#ifdef SKEW_NEIGHBOR
				current_square->add_neighbour( grid[y + 1][x + 1] );
#endif
#ifdef VERTICAL_NEIGHBOR
				current_square->add_neighbour( grid[y][x + 1] );
#endif
#ifdef SKEW_NEIGHBOR
				current_square->add_neighbour( grid[y - 1][x + 1] );
#endif
#ifdef HORIZONTAL_NEIGHBOR
				current_square->add_neighbour( grid[y - 1][x] );
#endif
			}
		}
	}
}
#endif

void compression_algorithm_under_test::find_best_scale_and_initialize_grid( )
{
	unsigned int s;
	double min_weight = INFINITY;

	if( best_scale > 0 ) {
		s = best_scale;

		//assert( rows % s == 0 );

		create_grid_for_scale( image, image.square_grid, s );

#ifdef ENABLE_SQUARE_NEIGHBOUR
		add_neighbors( image.square_grid, s, image.mat.rows, image.mat.cols );
#endif

		double s_weight = calculate_s_weight( image.square_grid, s, &image.mat );

		min_weight = s_weight;

	} else {
		std::map< unsigned int, std::vector< std::vector< square_block* > > > es_grid;

		rectangle rect;
		rect.point.x = 0;
		rect.point.y = 0;
		rect.w = image.mat.cols;
		rect.h = image.mat.rows;
		pmf ts2;
		ts2.compute_pmf( &image.mat, rect, false );

		for( s = MIN_SCALE; s <= MAX_SCALE; s++ ) {

			/*copy_original_matrix_contents( temp_matrix, this->matrix, this->rows, this->cols );
			 if( this->bfiltering_method == MEAN_BLOCK_FILTERING )
			 copy_original_matrix_contents( averaged_matrix, this->matrix, this->rows, this->cols );*/

			std::string rstr = ( image.mat.rows % s ) ? "1" : "0";
			std::string cstr = ( image.mat.cols % s ) ? "1" : "0";

			std::cout << "scale: " << s << " there are " << image.mat.rows / s
					<< " horizontal segments, with " << rstr << " remaining block of width "
					<< image.mat.rows % s << " and " << image.mat.cols / s << " vertical segments, with "
					<< cstr << " remaining block of height " << image.mat.cols % s << std::endl;

			if( image.mat.rows % s == 0 ) {
				create_grid_for_scale( image, es_grid[s], s );

				int num_of_blocks = image.mat.rows * image.mat.cols / ( s * s );
				//int max_num_of_blocks = rows * cols / ( MIN_SCALE * MIN_SCALE );
				int max_num_of_blocks = image.mat.rows * image.mat.cols /*/ ( MIN_SCALE * MIN_SCALE )*/;

				//add_neighbors( es_grid, s, rows, cols );

				double s_weight = calculate_s_weight( es_grid[s], s, &image.mat );
				std::cerr << "total entropy " << ts2.get_entropy( ) << std::endl;
				std::cerr << "weight for " << s << " = " << s_weight << " JSD = "
						<< ts2.get_entropy( ) - s_weight << std::endl;
				std::cerr << num_of_blocks * 6 << " bits " << std::endl;
				//s_weight += num_of_blocks / double( max_num_of_blocks ) * ts2.get_entropy( ) * 6;
				//s_weight += ts2.get_entropy( ) / ( s );
				double factor = 1 + 1.0 / ( s );
				std::cerr << "factor " << factor << std::endl;
				//s_weight *= factor;
				std::cerr << "modified weight " << s_weight << std::endl;

				if( s_weight <= min_weight ) {
					min_weight = s_weight;
					best_scale = s;
				}
			}
		}

		std::cerr << "Best scale was " << best_scale << " and weight was " << min_weight << std::endl;
		exit( EXIT_SUCCESS );
	}
}

void compression_algorithm_under_test::initialize_compression_blocks( )
{
	unsigned int x, y;

	compression_blocks.clear( );
	std::list< compression_block * >::iterator cit;

	for( y = 0; y != image.square_grid.size( ); y++ ) {
		for( x = 0; x != image.square_grid[y].size( ); x++ ) {
			compression_block *cb = new compression_block( &image.mat, image.square_grid[y][x], image.verification_matrix );

			compression_blocks.push_back( cb );
			image.square_grid[y][x]->set_container( static_cast< void* >( cb ) );
		}
	}
	std::cout << "There are " << compression_blocks.size( ) << " unique blocks" << std::endl;
}

std::string compression_algorithm_under_test::write_square_to_cb_map( )
{
	std::string map = "";
	std::vector< uint8_t > map_vector;
	unsigned int num_of_bits = ceil( log2( compression_blocks.size( ) ) );
	std::cout << __func__ << " num of bits" << num_of_bits << std::endl;

	if( type == AUT_SCATTERED ) {
		for( auto it : compression_blocks ) {
			it->sort_squares( );
		}
		compression_blocks.sort( compression_block::less_blocks );

		int i = 0;
		for( auto it : compression_blocks ) {
			it->set_order( i++ );
		}

		unsigned int max_span = quadtree::get_max_quadtree_span( best_scale, image.mat.rows, image.mat.cols );
		pos_2d pos;
		pos.y = pos.x = 0;
		quadtree *root = new quadtree( pos, max_span );
		root->initialize_tree_down_to_scale( image.square_grid, best_scale, image.mat.rows, image.mat.cols );
		quadtree::attempt_node_combining( root, quadtree::cb_check, quadtree::combine_nodes );
		cv::Mat new_mat = image.cv_mat.clone( );
		quadtree::draw_quadtree_node( new_mat, root );
		unsigned int qtree_length = root->count_bits_required_for_quadtree( num_of_bits );
		print_length( "length of qtree ", qtree_length );
		cv::namedWindow( "CB Quadtree", CV_WINDOW_NORMAL );
		cv::imshow( "CB Quadtree", new_mat );

		unsigned int blocks = 0;
		unsigned int x, y;
		for( y = 0; y != image.square_grid.size( ); y++ ) {
			for( x = 0; x != image.square_grid[y].size( ); x++ ) {
				compression_block *cb =
						static_cast< compression_block * >( image.square_grid[y][x]->get_container( ) );
				boost::dynamic_bitset< > bset( num_of_bits, cb->get_order( ) );
				map_vector.push_back( bset.to_ulong( ) );
				std::string str;
				to_string( bset, str );
				std::cout << str << " ";
				map += str;
				blocks++;
			}
			std::cout << std::endl;
		}
		std::cout << "blocks = " << blocks << std::endl;
		print_length( "map length = ", map.length( ) );
		std::string alternative_map = pmf::compress_huffman( map_vector );
		print_length( "alternative map length ", alternative_map.length( ) );

		if( map.length( ) > alternative_map.length( ) ) {
			std::cout << "original map length is bigger, picking alternative " << std::endl;
			compressed_length += alternative_map.length( );
		} else {
			std::cout << "original map length is smaller, staying with it " << std::endl;
			compressed_length += map.length( );
		}

		delete root;
	} else if( type == AUT_ADJACENT ) {

	}

	return map;
}

std::string compression_algorithm_under_test::encode_huffman_symbols( std::vector< bool > &huffman_nodes )
{
	std::string dict_string = "";
	for( auto b = huffman_nodes.begin( ); b != huffman_nodes.end( ); b++ ) {
		if( *b == true )
			dict_string += "1";
		else
			dict_string += "0";
	}
	return dict_string;
}

void compression_algorithm_under_test::verify( )
{
	size_t number_of_blocks = 0;
	for( std::list< compression_block * >::iterator cbs_it = compression_blocks.begin( );
			cbs_it != compression_blocks.end( ); ++cbs_it ) {
		( *cbs_it )->verify( );
		number_of_blocks += ( *cbs_it )->get_number_of_blocks( );
	}
	std::cout << "total number of squares = " << number_of_blocks << ", should be "
			<< image.mat.rows * image.mat.cols / ( best_scale * best_scale ) << std::endl;
	assert(
			number_of_blocks
					== size_t(
							ceil( image.mat.rows / ( double )best_scale )
									* ceil( image.mat.cols / ( double )best_scale ) ) );
	image.verify_cb_traversing( );

	std::cout << "number of compression blocks = " << compression_blocks.size( ) << " (down from "
			<< image.mat.rows * image.mat.cols / ( best_scale * best_scale ) << ")" << std::endl;
}

void compression_algorithm_under_test::print_length_of_compressed_output( )
{
	const size_t pixels = image.mat.rows * image.mat.cols;
	std::cout << "pixels " << pixels << std::endl;
	print_length_with_bpp( "total length of output = ", compressed_length, pixels );
	print_length_with_bpp( "real length of output = ", compressed_output.length( ), pixels );
}

std::string compression_algorithm_under_test::compress( )
{
	struct timespec start, end, diff;

	START_PROFILING;

	pre_process_filter( );

	find_best_scale_and_initialize_grid( );

	initialize_compression_blocks( );

	cluster( );

	//verify( );

	compressed_output = encode_header( );
	encode( );

	print_length_of_compressed_output( );

	END_PROFILING("compressing ");

	return compressed_output;
}

std::string compression_algorithm_under_test::encode_header( )
{
	std::string str = "";

	std::bitset< 16 > r( static_cast< uint16_t >( image.mat.rows ) );
	str += r.to_string( );

	std::bitset< 16 > c( static_cast< uint16_t >( image.mat.cols ) );
	str += c.to_string( );

	std::bitset< 8 > s( static_cast< uint8_t >( best_scale ) );
	str += s.to_string( );

	std::bitset< 8 > f( static_cast< uint8_t >( image.pre_filter ) );
	str += f.to_string( );

	std::bitset< 8 > e( static_cast< uint8_t >( coding ) );
	str += e.to_string( );

	std::bitset< 16 > n( static_cast< uint16_t >( compression_blocks.size( ) ) );
	str += n.to_string( );

	std::cout << __func__ << str << std::endl;

	return str;
}

void compression_algorithm_under_test::display_image_with_cbs( )
{
	if( !image.cv_mat.data ) {
		return;
	}

	std::list< compression_block * >::iterator largest_cb = std::max_element( compression_blocks.begin( ),
			compression_blocks.end( ), compression_block::less_blocks );

	cv::Scalar black( 0, 0, 0 );
	draw_cbs_on_mat( image.cv_mat, *largest_cb, black, false );

	cv::namedWindow( "Original with blocks", CV_WINDOW_NORMAL );
	cv::imshow( "Original with blocks", image.cv_mat );
//	if( filename.empty( ) ) {
//		cv::imwrite( "scattered_golomb.png", in_mat );
//	} else {
//		std::ostringstream sstream;
//		std::string codingstr;
//		codingstr = ( coding == HUFFMAN ) ? "_huffman" :
//					( coding == GOLOMB_RICE ) ? "_golomb_rice" : "_exp_golomb";
//		sstream << "scattered_" << boost::filesystem::basename( filename ) << codingstr << ".png";
//		cv::imwrite( sstream.str( ), in_mat );
//	}

	if( type != AUT_ADJACENT ) {
		cv::Mat filtered_mat = image.cv_mat.clone( );
		copy_array_into_opencv_matrix( filtered_mat, &image.mat );
		cv::Scalar white( 255, 255, 255 );
//		draw_cbs_on_mat( filtered_mat, *largest_cb, black, false );
		draw_colored_cbs_on_mat( filtered_mat, black );
		cv::namedWindow( "Filtered with blocks", CV_WINDOW_NORMAL );
		cv::imshow( "Filtered with blocks", filtered_mat );
		if( filename.empty( ) ) {
			cv::imwrite( "scattered_golomb_decorr.png", filtered_mat );
		} else {
			std::ostringstream sstream;
			std::string codingstr;
			codingstr = ( coding == HUFFMAN ) ? "_huffman" :
						( coding == GOLOMB_RICE ) ? "_golomb_rice" : "_exp_golomb";
			sstream << "scattered_" << boost::filesystem::basename( filename ) << codingstr << "_decorr.png";
			cv::imwrite( sstream.str( ), filtered_mat );
		}
	}

	cv::waitKey( 0 );
}

void compression_algorithm_under_test::create_grid_for_scale( image_data &image,
		std::vector< std::vector< square_block* > > & es_grid,
		const unsigned int s )
{
	unsigned int x;
	unsigned int y;

	for( y = 0; y < image.mat.rows; y += s ) {
		std::vector< square_block* > rowvec;
		for( x = 0; x < image.mat.cols; x += s ) {
			struct rectangle shape( pos_2d( x, y ), s, s );
			square_block *sq = new square_block( shape, &image.mat );
			if( bfiltering_method == MEAN_BLOCK_FILTERING ) {
				sq->subtract_diff( );
				sq->recalculate_pmf( );
			}

			rowvec.push_back( sq );
		}
		es_grid.push_back( rowvec );
	}

	if( bfiltering_method == MEAN_BLOCK_FILTERING ) {
		std::cout << "matrix of averages:" << std::endl;
		print_matrix( std::cout, image.mat.data, image.mat.rows, image.mat.cols );
	}
}

void compression_algorithm_under_test::encode( )
{
	std::string dict_string;
	std::string cb_compressed_output;
	std::vector< uint8_t > dictionary_vector;
	std::vector< bool > huffman_nodes;
	//int h = 0;

	compressed_length = 0;
	compressed_output += compression_algorithm_under_test::write_square_to_cb_map( );

	for( auto it : compression_blocks ) {

		std::cout << *it << std::endl;

//		char buf[100];
//		//sprintf( buf, "hist_%d.out", it->get_k( ) );
//		//it->print_histogram( buf );
//		sprintf( buf, "enc_%d.txt", h );
//		std::ofstream stream;
//		stream.open( buf, std::ofstream::out | std::ofstream::trunc );
//		h++;

		std::cout << "Number of square blocks in this sub-image " << it->get_number_of_blocks( ) << std::endl;
		if( coding == GOLOMB_RICE ) {
			unsigned int M;
			M = pow( 2, it->get_k( ) );
			std::cout << __func__ << " K = " << it->get_k( ) << std::endl;
			std::bitset< 3 > bset( it->get_k( ) );
			cb_compressed_output += bset.to_string( );
			std::cout << "M = " << M << std::endl;
			std::cout << __func__ << " K(bits) = " << bset.to_string( ) << std::endl;
//			stream << bset.to_string( ) << std::endl;
			cb_compressed_output += it->cb_compress_golomb_rice( M );
		} else if( coding == EXP_GOLOMB ) {

			std::cout << __func__ << " K = " << it->get_k( ) << std::endl;
			std::bitset< 3 > bset( it->get_k( ) );
			cb_compressed_output += bset.to_string( );
			std::cout << __func__ << " K(bits) = " << bset.to_string( ) << std::endl;
//			stream << bset.to_string( ) << std::endl;
			cb_compressed_output += it->cb_compress_exponential_golomb( it->get_k( ) );
		} else if( coding == HUFFMAN ) {
			cb_compressed_output += it->cb_compress_huffman( ( separate_dict_compression ) ? false : true );

			if( separate_dict_compression ) {
				it->write_huffman_tree( false );
				it->append_huffman_symbols_to_vector( dictionary_vector );
				it->append_huffman_nodes_to_vector( huffman_nodes );
			}
		}

//		stream.close( );
	}
	compressed_output += cb_compressed_output;
	compressed_length += cb_compressed_output.length( );

	if( separate_dict_compression && coding == HUFFMAN ) {
		std::cout << "num of values in dictionary vector = " << dictionary_vector.size( ) << std::endl;

		pmf temp_t = pmf( );
		temp_t.compute_pmf( dictionary_vector );
		std::cout << temp_t << std::endl;
		dict_string = compression_algorithm_under_test::encode_huffman_symbols( huffman_nodes );
		dict_string += temp_t.compress_huffman( dictionary_vector );
		compressed_length += dict_string.length( );
		compressed_output += dict_string;
	}

	print_length( "length of compressed output = ", cb_compressed_output.length( ) );
	print_length( "compressed dict length = ", dict_string.length( ) );
}

void compression_algorithm_under_test::draw_cbs_on_mat( cv::Mat &mat,
		compression_block *largest_cb,
		cv::Scalar &line_col,
		bool draw_largest_cb )
{
	std::list< edge > edge_vec;
	std::list< square_block * > square_vec;

#ifdef USE_EDGES_IN_COMPRESSION_BLOCKS
	for( std::list< compression_block * >::iterator it = compression_blocks.begin( );
			it != compression_blocks.end( ); it++ ) {
		( *it )->remove_inner_edges( );
		edge_vec = ( *it )->get_edges( );

		for( std::list< edge >::iterator edge_it = edge_vec.begin( ); edge_it != edge_vec.end( );
				edge_it++ ) {
			std::cout << " " << ( *edge_it ).start << ", " << ( *edge_it ).end << std::endl;
		}

		for( std::list< edge >::iterator edge_it = edge_vec.begin( ); edge_it != edge_vec.end( );
				edge_it++ ) {
			cv::Point start = cv::Point( ( *edge_it ).start.x, ( *edge_it ).start.y );
			cv::Point end = cv::Point( ( *edge_it ).end.x, ( *edge_it ).end.y );
			cv::line( mat, start, end, line_col, 1, 8 );
		}
	}
#endif

	// Draw largest CB
	if( draw_largest_cb ) {
		square_vec = largest_cb->get_blocks( );

		for( std::list< square_block * >::iterator square_it = square_vec.begin( );
				square_it != square_vec.end( ); square_it++ ) {
			rectangle &shape = ( *square_it )->get_shape( );
			cv::Point start = cv::Point( shape.point.x, shape.point.y );
			cv::Point end = cv::Point( shape.point.x + shape.w, shape.point.y + shape.h );
			cv::rectangle( mat, start, end, cv::Scalar( 255, 255, 255 ), -1, 8 );
		}

#ifdef USE_EDGES_IN_COMPRESSION_BLOCKS
		largest_cb->remove_inner_edges( );
		edge_vec = largest_cb->get_edges( );

		for( std::list< edge >::iterator edge_it = edge_vec.begin( ); edge_it != edge_vec.end( );
				edge_it++ ) {
			cv::Point start = cv::Point( ( *edge_it ).start.x, ( *edge_it ).start.y );
			cv::Point end = cv::Point( ( *edge_it ).end.x, ( *edge_it ).end.y );
			cv::line( mat, start, end, line_col, 1, 8 );
		}
#endif
	}
}

void compression_algorithm_under_test::draw_colored_cbs_on_mat( cv::Mat &mat, cv::Scalar &line_col )
{
	std::list< square_block * > square_vec;

	unsigned int step = 10;

	unsigned int col1 = 10;
	unsigned int col2 = 240;

	int i = 0;

	for( std::list< compression_block * >::iterator it = compression_blocks.begin( );
			it != compression_blocks.end( ); it++ ) {
		square_vec = ( *it )->get_blocks( );

		cv::Scalar col;
		if( i % 2 == 0 ) {
			col = cv::Scalar( col1, col1, col1 );
			col1 += step;
		} else {
			col = cv::Scalar( col2, col2, col2 );
			col2 -= step;
		}
		for( std::list< square_block * >::iterator square_it = square_vec.begin( );
				square_it != square_vec.end( ); square_it++ ) {
			rectangle &shape = ( *square_it )->get_shape( );
			cv::Point start = cv::Point( shape.point.x, shape.point.y );
			cv::Point end = cv::Point( shape.point.x + shape.w, shape.point.y + shape.h );
			cv::rectangle( mat, start, end, col, -1, 8 );
		}
	}


#ifdef USE_EDGES_IN_COMPRESSION_BLOCKS
	std::list< edge > edge_vec;

	for( std::list< compression_block * >::iterator it = compression_blocks.begin( );
			it != compression_blocks.end( ); it++ ) {
		( *it )->remove_inner_edges( );
		edge_vec = ( *it )->get_edges( );

		for( std::list< edge >::iterator edge_it = edge_vec.begin( ); edge_it != edge_vec.end( );
				edge_it++ ) {
			std::cout << " " << ( *edge_it ).start << ", " << ( *edge_it ).end << std::endl;
		}

		for( std::list< edge >::iterator edge_it = edge_vec.begin( ); edge_it != edge_vec.end( );
				edge_it++ ) {
			cv::Point start = cv::Point( ( *edge_it ).start.x, ( *edge_it ).start.y );
			cv::Point end = cv::Point( ( *edge_it ).end.x, ( *edge_it ).end.y );
			cv::line( mat, start, end, line_col, 1, 8 );
		}
	}
#endif
}

void decompression_algorithm_under_test::decode_header( )
{
	unsigned int rows;
	unsigned int cols;
	compression_filter pre_filter;

#define DECODE_COMPRESSION_HEADER_ENTRY( entry, bitwidth )\
header.entry = boost::dynamic_bitset<unsigned char>( this->compressed_output.substr( pos, bitwidth ) ).to_ulong(); \
pos += bitwidth;

	pos = 0;
	compression_header header = { 0 };

	DECODE_COMPRESSION_HEADER_ENTRY( rows, 16 );
	DECODE_COMPRESSION_HEADER_ENTRY( cols, 16 );
	DECODE_COMPRESSION_HEADER_ENTRY( scale, 8 );
	DECODE_COMPRESSION_HEADER_ENTRY( pre_filter, 8 );
	DECODE_COMPRESSION_HEADER_ENTRY( coding, 8 );
	DECODE_COMPRESSION_HEADER_ENTRY( num_of_cbs, 16 );

#undef DECODE_COMPRESSION_HEADER_ENTRY

	rows = header.rows;
	cols = header.cols;
	best_scale = header.scale;
	pre_filter = static_cast< compression_filter >( header.pre_filter );
	coding = static_cast< entropy_coding >( header.coding );
	num_of_cbs = header.num_of_cbs;
	std::cout << __func__ << " rows = " << rows << std::endl;
	std::cout << __func__ << " cols = " << cols << std::endl;
	std::cout << __func__ << " scale = " << best_scale << std::endl;
	std::cout << __func__ << " pre_filter = " << pre_filter << std::endl;
	std::cout << __func__ << " coding = " << coding << std::endl;
	std::cout << __func__ << " num_of_cbs = " << num_of_cbs << std::endl;

	decompressed_image = new image_data( rows, cols, pre_filter );
}

void decompression_algorithm_under_test::read_cbs_to_square_map( )
{
	unsigned int x;
	unsigned int y;

	unsigned int num_of_bits = ceil( log2( num_of_cbs ) );
	std::cout << __func__ << " num of bits" << num_of_bits << std::endl;

	unsigned int i = 0;
	unsigned int j = 0;

	for( y = 0; y < decompressed_image->mat.rows; y += best_scale, j++ ) {
		std::vector< square_block * > rowvec;

		for( x = 0; x < decompressed_image->mat.cols; x += best_scale, i++ ) {
			std::cout << " x = " << x << " y = " << y << std::endl;
			rectangle shape( pos_2d( x, y ), best_scale, best_scale );
			square_block *sq = new square_block( shape );
			assert( sq != NULL );

			unsigned int cb_num = boost::dynamic_bitset< unsigned char >(
					this->compressed_output.substr( pos, num_of_bits ) ).to_ulong( );
			std::cout << "CB num = " << cb_num << std::endl;
			assert( cb_num < num_of_cbs );
			std::vector< compression_block >::iterator it;
			if( ( it = std::find_if( compression_blocks.begin( ), compression_blocks.end( ),
					[&](compression_block& cb
					) {	return cb.get_order() == cb_num;} ) ) == compression_blocks.end( ) ) {
				std::cout << "allocated new CB" << std::endl;
				compression_block cb( sq, decompressed_image->verification_matrix, cb_num );
				compression_blocks.push_back( cb );
				order_vector.push_back( cb_num );
			} else {
				( *it ).add_square( sq );
			}
			pos += num_of_bits;
			rowvec.push_back( sq );
		}
		decompressed_image->square_grid.push_back( rowvec );
	}
	std::cout << "compression_block size " << compression_blocks.size( ) << std::endl;
}

void decompression_algorithm_under_test::decode_cbs( )
{
	assert( compression_blocks.size( ) > 0 );

	for( auto cb : compression_blocks ) {
		cb.sort_squares( );
	}
	std::sort( compression_blocks.begin( ), compression_blocks.end( ), less_than_blocks( ) );

	//int h = 0;
	for( auto it : compression_blocks ) {
		/*char buf[100];
		sprintf( buf, "dec_%d.txt", h );
		std::ofstream stream;
		stream.open( buf, std::ofstream::out | std::ofstream::trunc );
		h++;*/

		if( coding == GOLOMB_RICE || coding == EXP_GOLOMB ) {
			std::bitset< 3 > bset( compressed_output, pos, 3 );
			unsigned int K = bset.to_ulong( );
			pos += 3;
			std::cout << __func__ << " K = " << bset.to_string( ) << std::endl;
			std::cout << it << std::endl;

			//stream << bset.to_string( ) << std::endl;

			if( coding == GOLOMB_RICE ) {
				pos = it.cb_decompress_golomb_rice( compressed_output, pos, K, &decompressed_image->mat );
			} else {
				pos = it.cb_decompress_exponential_golomb( compressed_output, pos, K,
						&decompressed_image->mat );
			}
		} else {
			pos = it.cb_decompress_huffman( compressed_output, pos, &decompressed_image->mat );
		}
		//stream.close( );
	}
}

uint8_t *decompression_algorithm_under_test::decompress( )
{
	decode_header( );
	struct timespec start, end, diff;

	START_PROFILING;
	read_cbs_to_square_map( );
	decode_cbs( );
	decompressed_image->revert_filter( );
	END_PROFILING( "decompressing " );

#if 0
	out_mat = cv::Mat( rows, cols, CV_8UC1, decompressed_output );

	cv::namedWindow( "Decompressed output", CV_WINDOW_NORMAL );
	cv::imshow( "Decompressed output", out_mat );
	cv::waitKey( 0 );
#endif

	return decompressed_image->mat.data;
}

bool decompression_algorithm_under_test::compare_cbs( const std::list< compression_block * > &list )
{
	if( list.size( ) != compression_blocks.size( ) ) {
		std::cout << "Sizes not equal " << std::endl;
		return false;
	}
	auto lit = list.begin( );
	auto vit = compression_blocks.begin( );
	for( ; lit != list.end( ) && vit != compression_blocks.end( ); lit++, vit++ ) {
		if( *( *lit ) == ( *vit ) ) {
			std::cout << "CB equal " << std::endl;
		} else {
			std::cout << "CB not equal " << std::endl;
			return false;
		}
	}
	std::cout << __func__ << " equal " << std::endl;
	return true;
}
