/*
 * MIT License
 *
 * Copyright (c) 2014 Sameh Samir Hassan
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef ENUMS_H_
#define ENUMS_H_

typedef enum __entropy_coding {
	HUFFMAN,
	GOLOMB_RICE,
	EXP_GOLOMB,
} entropy_coding;

typedef enum __block_filtering_method {
	NO_BLOCK_FILTERING,
	MEAN_BLOCK_FILTERING,
	BWT_BLOCK_FILTERING,
} block_filtering_method;

typedef enum __compression_filter {
	FILTER_NONE,
	FILTER_SUBTRACT_LEFT,
	FILTER_MEAN,
	FILTER_PAETH,
} compression_filter;

typedef enum __dissimilarity_measure {
	NO_DISSIMILARITY_MEASURE,
	DISSIMILARITY_SQUARED_EUCLIDEAN,
	DISSIMILARITY_JENSEN_SHANNON,
	DISSIMILARITY_KULLBACK_LEIBLER,
	DISSIMILARITY_BHATTACHARYYA,
} dissimilarity_measure;

#if 1  /* For higher resolution images */
#define MIN_SCALE             8
#define MAX_SCALE             64
#else
#define MIN_SCALE             4
#define MAX_SCALE             32
#endif

#define JSD_THRESHOLD         0.03


#endif /* ENUMS_H_ */
