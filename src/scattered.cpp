/*
 * MIT License
 *
 * Copyright (c) 2014 Sameh Samir Hassan
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <iostream>
#include <list>
#include <unistd.h>
#include <pthread.h>
#include <bitset>
#include <fstream>
#include <thread>
#include <mutex>
#include <eigen3/Eigen/Dense>

#include "enums.h"
#include "pos_2d.h"
#include "edge.h"
#include "compression_block.h"
#include "algo.h"
#include "aut.h"
#include "scattered.h"

class graph_edge {
public:
	graph_edge( compression_block* v1, compression_block* v2, double weight1 ) :
			weight( weight1 )
	{
		edge = std::make_pair( v1, v2 );
		c = NULL;
		n = NULL;
	}
	inline bool operator<( const graph_edge& other ) const
	{
		if( weight < other.weight ) {
			return true;
		} else {
			return false;
		}
	}
	double get_weight( ) const
	{
		return weight;
	}
	void update_weight( double w1 )
	{
		weight = w1;
	}
	std::pair< compression_block *, compression_block * > &get_pair( )
	{
		return edge;
	}
	void set_cluster( cluster *cl )
	{
		c = cl;
	}
	void set_node( compression_block* node )
	{
		n = node;
	}
	cluster *get_cluster( )
	{
		return c;
	}

	compression_block *get_node( )
	{
		return n;
	}

private:
	std::pair< compression_block*, compression_block* > edge; // edge between two compression blocks
	double weight;
	double avg_entropy;
	cluster *c;
	compression_block *n;
};

struct best_center_parameter {
	std::list< compression_block * > *nodes;
	compression_block *cb;
	double total_jsd_for_node;
	double (*dissimilarity)( const pmf * const p1, const pmf * const p2 );
};

struct row_scan_parameter {
	unsigned int i;
	std::list< compression_block * > *nodes;
	std::list< compression_block * >::iterator cb1;
	Eigen::ArrayXXd *m;
};

class cluster {
public:
	cluster( compression_block *center, dissimilarity_measure dmeasure ) :
			center( center ), dmeasure( dmeasure ), total_jsd( 0 )
	{
		nodes.clear( );
		switch( dmeasure ) {
			case DISSIMILARITY_SQUARED_EUCLIDEAN:
				dissimilarity = pmf::calculate_squared_euclidean_distance;
				break;

			case DISSIMILARITY_JENSEN_SHANNON:
				dissimilarity = pmf::calculate_jensen_shannon_divergence;
				break;

			case DISSIMILARITY_KULLBACK_LEIBLER:
				dissimilarity = pmf::calculate_kullback_leibler_distance;
				break;

			case DISSIMILARITY_BHATTACHARYYA:
				dissimilarity = pmf::calculate_bhattacharyya_distance;
				break;

			default:
				abort( );
		}
	}

	compression_block *get_center( ) const
	{
		return center;
	}
	void add_node( compression_block *node )
	{
		nodes.push_back( node );
	}

	void add_edge( compression_block *node )
	{
		//std::cerr << __func__ << " " << edges.size( ) << std::endl;
		graph_edge e( center, node, calculate_dissimilarity( node ) );
		e.set_cluster( this );
		e.set_node( node );
		edges.push_back( e );
		assert( edges.size( ) != 0 );
	}

	void clear_edges( )
	{
		//std::cerr << " " << __func__ << std::endl;
		edges.clear( );
	}

	size_t get_num_edges( ) const
	{
		return edges.size( );
	}

	void add_node_to_cluster_center( compression_block *block )
	{
		center->add_cb( *block );
	}

	graph_edge get_minimum_edge_weight( )
	{
		assert( edges.size( ) != 0 );
		std::list< graph_edge >::iterator min = std::min_element( edges.begin( ), edges.end( ) );
		if( min == edges.end( ) ) {
			std::cout << "no more edges!" << std::endl;
			abort( );
		}
		return *min;
	}

	void combine_cluster()
	{
		for( auto it = nodes.begin( ); it != nodes.end( ); it = nodes.erase( it ) ) {
			// remove all edges that have pair.second
			center->add_cb( *(*it) );
			delete *it;
		}
		nodes.clear( );
	}

	double calculate_dissimilarity( compression_block *other )
	{
		assert( center->get_number_of_blocks( ) == 1 );
		square_block *center_sq = ( * center->get_blocks( ).begin( ) );
		assert( other->get_number_of_blocks( ) == 1 );
		square_block *other_sq = ( * other->get_blocks( ).begin( ) );

		return dissimilarity( center_sq, other_sq );
	}

	void set_total_jsd( double jsd )
	{
		total_jsd = jsd;
	}

	size_t get_number_of_nodes( )
	{
		return nodes.size( );
	}

	static inline bool highest_jsd_first( const cluster &l, const cluster &r )
	{
		if( l.total_jsd > r.total_jsd )
			return true;
		return false;
	}

	static void *find_best_center_thread( void *parameter )
	{
		struct best_center_parameter *p = static_cast< struct best_center_parameter * >( parameter );
		compression_block *cb = p->cb;
		double total_jsd_for_node = 0.0;
		for( auto other_cb = p->nodes->begin( ); other_cb != p->nodes->end( ); other_cb++ ) {
			if( cb == ( *other_cb ) ) {
				continue;
			}
			total_jsd_for_node += p->dissimilarity( cb, *other_cb );
		}
		p->total_jsd_for_node = total_jsd_for_node;
		return NULL;
	}

	static void *scan_row_for_divergence( void *parameter )
	{
		struct row_scan_parameter *p = static_cast< struct row_scan_parameter * >( parameter );

		unsigned int i = p->i;
		int j = 0;
		auto cb2 = p->cb1;
		cb2++;
		for( ; cb2 != p->nodes->end( ); cb2++ ) {
			std::vector< pmf * > pvec;
			pvec.push_back( (*p->cb1) );
			pvec.push_back( *cb2 );
			p->m->coeffRef( i, i + j ) = pmf::calculate_jensen_shannon_divergence( pvec );
			j++;
		}
		return NULL;
	}

	compression_block *find_best_center( )
	{
		if( nodes.size( ) == 0 || nodes.size( ) == 1 ) {
			//std::cerr << "WARNING: zero nodes" << std::endl;
			return center;
		}
		nodes.push_back( center );
		double min_dis = INFINITY;
		compression_block *best_center = NULL;
#if 0
		size_t node_size = nodes.size( ) - 1;
		assert( node_size > 0 );
		struct timespec start, end, diff;

		START_PROFILING;
		Eigen::ArrayXXd m = Eigen::ArrayXXd::Zero( node_size, node_size );
		END_PROFILING( "allocating array" );
		int i = 0;
		auto rit = nodes.rbegin( );
		auto cb1 = nodes.begin( );

#define NUM_OF_CENTER_THREADS   8
		bool done = false;
		while( !done ) {
			int thread_counter = 0;
			std::vector< pthread_t > threads( NUM_OF_CENTER_THREADS );
			std::vector< row_scan_parameter > parameters( NUM_OF_CENTER_THREADS );
			for( int t = 0; t < NUM_OF_CENTER_THREADS; t++ ) {
				if( cb1 != rit.base( ) ) {
					parameters[thread_counter].i = i;
					parameters[thread_counter].m = &m;
					parameters[thread_counter].nodes = &nodes;
					parameters[thread_counter].cb1 = cb1;
					pthread_create( &threads[thread_counter], NULL, cluster::scan_row_for_divergence,
							static_cast< void * >( &parameters[thread_counter] ) );
					thread_counter++;
					cb1++;
					i++;
				} else {
					done = true;
					break;
				}
			}

			for( int i = 0; i < thread_counter; i++ ) {
				pthread_join( threads[i], NULL );
			}
		}

//		for( ; cb1 != rit.base( ); cb1++ ) {
//			int j = 0;
//			auto cb2 = cb1;
//			cb2++;
//			for( ; cb2 != nodes.end( ); cb2++ ) {
//				std::vector< pmf * > pvec;
//				pvec.push_back( *cb1 );
//				pvec.push_back( *cb2 );
//				m( i, i+j ) = pmf::calculate_jensen_shannon_divergence( pvec );
//				j++;
//			}
//			i++;
//		}
		i = 0;
		for( auto cb = nodes.begin( ); cb != nodes.end( ); cb++ ) {
			double total_jsd_for_node = 0.0;
			if( cb == nodes.begin( ) ) {
				total_jsd_for_node = m.row( 0 ).sum( );
			} else if( std::distance( cb, nodes.end( ) ) == 1 ) {
				total_jsd_for_node = m.col( node_size - 1 ).sum( );
			} else {
				total_jsd_for_node = m.row( i ).sum( ) + m.col( i - 1 ).sum( );
			}
			std::cout << "total_jsd_for_node = " << total_jsd_for_node << std::endl;
			if( total_jsd_for_node < min_dis ) {
				min_dis = total_jsd_for_node;
				best_center = *cb;
			}
			i++;
		}

#endif

#if 0
#define NUM_OF_CENTER_THREADS   2
		bool done = false;
		auto cb_it = nodes.begin( );
		while( !done ) {
			int thread_counter = 0;
			std::vector< pthread_t > threads( NUM_OF_CENTER_THREADS );
			std::vector< best_center_parameter > parameters( NUM_OF_CENTER_THREADS );
			for( int i = 0; i < NUM_OF_CENTER_THREADS; i++ ) {
				if( cb_it != nodes.end( ) ) {
					parameters[thread_counter].nodes = &nodes;
					parameters[thread_counter].cb = ( *cb_it );
					parameters[thread_counter].dissimilarity = dissimilarity;
					pthread_create( &threads[thread_counter], NULL, cluster::find_best_center_thread,
							static_cast< void * >( &parameters[thread_counter] ) );
					thread_counter++;
					cb_it++;
				} else {
					done = true;
					break;
				}
			}

			for( int i = 0; i < thread_counter; i++ ) {
				pthread_join( threads[i], NULL );

				if( parameters[i].total_jsd_for_node < min_dis ) {
					min_dis = parameters[i].total_jsd_for_node;
					best_center = parameters[i].cb;
				}
			}
		}
#endif

#if 1
		for( auto cb : nodes ) {
			assert( cb->get_number_of_blocks( ) == 1 );
			square_block *sq = ( * cb->get_blocks( ).begin( ) );
			double total_dis_for_node = 0.0;
			for( auto other_cb : nodes ) {
				if( cb == other_cb ) {
					continue;
				}
				assert( other_cb->get_number_of_blocks( ) == 1 );
				square_block *other_sq = ( * other_cb->get_blocks( ).begin( ) );
				total_dis_for_node += dissimilarity( sq, other_sq );
			}
			if( total_dis_for_node < min_dis ) {
				min_dis = total_dis_for_node;
				best_center = cb;
			}
		}

#endif
		nodes.pop_back( );
		return best_center;
	}

private:
	compression_block *center;
	std::list< compression_block * > nodes;
	double total_jsd;

	std::list< graph_edge > edges;

	dissimilarity_measure dmeasure;
	double (*dissimilarity)( const pmf * const p1, const pmf * const p2 );
};

static std::mutex best_cluster_mutex;

void clustering_algorithm_scattered::find_best_cluster( std::list< cluster > &cluster_list,
		compression_block *node )
{
	double min_div = INFINITY;
	std::list< cluster >::iterator cluster;
	compression_block *other = node;
	for( auto c = cluster_list.begin( ); c != cluster_list.end( ); c++ ) {
		double jsd = ( *c ).calculate_dissimilarity( other );
		if( jsd < min_div ) {
			min_div = jsd;
			cluster = c;
		}
		//std::cout << "min jsd =" << min_div << std::endl;
	}
	assert( cluster != cluster_list.end( ) );

	std::lock_guard < std::mutex > block_threads_until_finish_this_job( best_cluster_mutex );

	( *cluster ).add_node( other );
}

void cluster_using_minimum_divergence( std::list< cluster > &clusters,
		std::list< compression_block * > &others )
{
	bool done = false;
	auto other = others.begin( );
	while( !done ) {
		unsigned int thread_counter = 0;
		std::thread threads[8];
		for( int i = 0; i < 8; i++ ) {
			if( other != others.end( ) ) {
				threads[thread_counter] = std::thread(
						clustering_algorithm_scattered::find_best_cluster, std::ref( clusters ), *other );
				thread_counter++;
				other++;
			} else {
				done = true;
				break;
			}
		}

		for( unsigned int i = 0; i < thread_counter; i++ ) {
			threads[i].join( );
		}
	}
}

void cluster_using_minimum_divergence_for_edges( std::list< cluster > &clusters,
		std::list< compression_block * > &others )
{
	assert( clusters.size( ) != 0 );
	assert( others.size( ) != 0 );

	std::list< compression_block * > other_nodes = others;
	while( other_nodes.size( ) != 0 ) {
		graph_edge min_edge( NULL, NULL, INFINITY );

		for( auto c : clusters ) {
			c.clear_edges( );
			for( auto i : other_nodes ) {
				c.add_edge( i );
			}
			std::cerr << " num of edges " << c.get_num_edges( ) << std::endl;

			graph_edge temp_edge = c.get_minimum_edge_weight( );
			if( temp_edge.get_weight( ) < min_edge.get_weight( ) ) {
				min_edge = temp_edge;
			}
		}
		assert( min_edge.get_weight( ) != INFINITY );
		cluster *c = min_edge.get_cluster( );
		assert( c != NULL );
		assert( min_edge.get_node( ) != NULL );
		c->add_node_to_cluster_center( min_edge.get_node( ) );
		for( auto o = other_nodes.begin( ); o != other_nodes.end( ); ) {
			if( ( *o ) == min_edge.get_node( ) ) {
				delete *o;
				other_nodes.erase( o );
				break;
			} else {
				o++;
			}
		}
	}
}


void clustering_algorithm_scattered::combine_clusters_and_assign_to_cb_list( std::list< cluster > &clusters )
{
	std::cout << "clusters = " << clusters.size( ) << std::endl;
	std::list< compression_block* > new_list;
	size_t total_number_of_nodes = 0;
	for( auto cluster : clusters ) {
		size_t number_of_nodes = cluster.get_number_of_nodes( );
		std::cout << "nodes = " << number_of_nodes << std::endl;
		total_number_of_nodes += number_of_nodes + 1;
		cluster.combine_cluster( );
		new_list.push_back( cluster.get_center( ) );
	}
	std::cout << " total num of nodes = " << total_number_of_nodes << std::endl;
	compression_blocks = new_list;
}

size_t clustering_algorithm_scattered::cluster_cbs( )
{
	std::list< cluster > clusters;
	std::list< compression_block * > others;

	static size_t last_expected_code_length = 0;
	static size_t last_expected_overhead = 0;

	struct timespec start, end, diff;

	START_PROFILING;

	size_t expected_code_length = 0;
	size_t expected_overhead = 0;

	for( auto cb : compression_blocks ) {
		expected_code_length += cb->get_expected_length( );
		expected_overhead += cb->get_num_of_unique_values( );
		if( clusters.size( ) == 0 ) {
			cluster c( cb, dmeasure );
			clusters.push_back( c );
			continue;
		}
		bool push = true;
		for( const auto &cluster : clusters ) {
			if( cb->is_kullback_leibler_distance_infinity( *( cluster.get_center( ) ) ) == false
					|| cluster.get_center( )->is_kullback_leibler_distance_infinity( *cb ) == false ) {
				push = false;
				break;
			}
		}
		if( push ) {
			cluster c( cb, dmeasure );
			clusters.push_back( c );
		} else {
			others.push_back( cb );
		}
	}
	print_length( "expected code length before ", expected_code_length );
	std::cout << "expected overhead = " << expected_overhead << " " << compression_blocks.size( )
			<< std::endl;
	if( last_expected_code_length == 0 && last_expected_code_length ) {
		last_expected_code_length = expected_code_length;
		last_expected_overhead = expected_overhead;
	} else {
		size_t last_total = last_expected_code_length + last_expected_overhead;
		size_t total = expected_code_length + expected_overhead;
		print_length( "total ", total );
		if( last_total > total ) {
			std::cout << "last > current... continue" << std::endl;
		} else {
			std::cout << "last <= current... should stop here, before it gets worse" << std::endl;
		}
	}
	assert( clusters.size( ) + others.size( ) == compression_blocks.size( ) );
	END_PROFILING("centers ");

	START_PROFILING;
	cluster_using_minimum_divergence( clusters, others );
	END_PROFILING( "clustering divergence" );

	START_PROFILING;
	combine_clusters_and_assign_to_cb_list( clusters );
	END_PROFILING( "combining " );

	return compression_blocks.size( );
}

void cluster_using_kl_and_jsd::do_clustering( )
{
	unsigned int num_of_clusters_old = 0;
	while( 1 ) {
		unsigned int num_of_clusters_new = cluster_cbs( );
		if( num_of_clusters_old == num_of_clusters_new )
			break;
		num_of_clusters_old = num_of_clusters_new;
	}
}

void modified_cluster_cbs::do_clustering( )
{
	std::list< cluster > clusters;
	std::list< compression_block * > others;

	static size_t last_expected_code_length = 0;
	static size_t last_expected_overhead = 0;

	struct timespec start, end, diff;

	START_PROFILING;

	size_t expected_code_length = 0;
	size_t expected_overhead = 0;

	for( auto cb : compression_blocks ) {
		expected_code_length += cb->get_expected_length( );
		expected_overhead += cb->get_num_of_unique_values( );
		if( clusters.size( ) == 0 ) {
			cluster c( cb, dmeasure );
			clusters.push_back( c );
			continue;
		}
		bool push = true;
		for( const auto &cluster : clusters ) {
			if( cb->is_kullback_leibler_distance_infinity( *( cluster.get_center( ) ) ) == false
					|| cluster.get_center( )->is_kullback_leibler_distance_infinity( *cb ) == false ) {
				push = false;
				break;
			}
		}
		if( push ) {
			cluster c( cb, dmeasure );
			clusters.push_back( c );
		} else {
			others.push_back( cb );
		}
	}

	for( auto cit = clusters.begin( ); cit != clusters.end( ); cit++ ) {
		double total_jsd = 0.0;
		for( auto c = clusters.begin( ); c != clusters.end( ); c++ ) {
			if( cit == c ) {
				continue;
			}
			total_jsd += dissimilarity( ( *cit ).get_center( ), ( *c ).get_center( ) );
		}
		( *cit ).set_total_jsd( total_jsd );
	}
	clusters.sort( cluster::highest_jsd_first );
	while( clusters.size( ) > 6 ) {
		auto rear = clusters.rbegin( );
		others.push_back( ( *rear ).get_center( ) );
		clusters.pop_back( );
	}

	print_length( "expected code length before ", expected_code_length );
	std::cout << "expected overhead = " << expected_overhead << " " << compression_blocks.size( )
			<< std::endl;
	if( last_expected_code_length == 0 && last_expected_code_length ) {
		last_expected_code_length = expected_code_length;
		last_expected_overhead = expected_overhead;
	} else {
		size_t last_total = last_expected_code_length + last_expected_overhead;
		size_t total = expected_code_length + expected_overhead;
		print_length( "total ", total );
		if( last_total > total ) {
			std::cout << "last > current... continue" << std::endl;
		} else {
			std::cout << "last <= current... should stop here, before it gets worse" << std::endl;
		}
	}
	assert( clusters.size( ) + others.size( ) == compression_blocks.size( ) );
	END_PROFILING("centers ");

	START_PROFILING;
	cluster_using_minimum_divergence( clusters, others );
	END_PROFILING( "clustering divergence" );

	START_PROFILING;
	combine_clusters_and_assign_to_cb_list( clusters );
	END_PROFILING( "combining " );

	//return compression_blocks.size( );
	return;
}


void cluster_using_g_center_and_jsd::do_clustering( )
{
	std::list< cluster > clusters;
	std::list< compression_block * > others;
	std::vector< compression_block * > k_vec[8];
	for( auto it : compression_blocks ) {
		unsigned int min_k;
		min_k = it->calculate_min_k_golomb_rice( );
		if( k_vec[min_k].size( ) == 0 ) {
			k_vec[min_k].push_back( it );
		} else {
			others.push_back( it );
		}
	}

	for( auto c : k_vec ) {
		assert( c.size( ) <= 1 );
		if( c.size( ) == 1 ) {
			clusters.push_back( cluster( c[0], dmeasure ) );
		}
	}

	assert( clusters.size() != 0 );

//		std::list< cluster > clist;
//		for( auto cb : compression_blocks ) {
//			cluster c( cb );
//			double jsd_total = 0;
//			for( auto o : compression_blocks ) {
//				if( o == cb ) {
//					continue;
//				}
//				std::vector<pmf*> pvec;
//				pvec.push_back( o );
//				pvec.push_back( cb );
//				jsd_total += pmf::calculate_jensen_shannon_divergence( pvec );
//
//			}
//			c.set_total_jsd( jsd_total );
//			clist.push_back( c );
//			clist.sort( cluster::highest_jsd_first );
//		}

//		std::list< cluster > clusters;
//		auto c = clist.begin( );
//		for( int i = 0; i < 8; i++ ) {
//			c++;
//		}
//		clusters.insert( clusters.begin( ), clist.begin( ), c );
//		assert( clusters.size( ) == 8 );
//		while( c != clist.end( ) ) {
//			others.push_back( ( *c ).get_center( ) );
//			c++;
//		}
//
	cluster_using_minimum_divergence( clusters, others );
//		cluster_using_minimum_divergence_for_edges( clusters, others );

	combine_clusters_and_assign_to_cb_list( clusters );
}

void cluster_for_golomb_codes::do_clustering( )
{
	//remove_cbs_with_duplicate_histograms( );

	std::list< compression_block * > new_compression_blocks;

	std::vector< compression_block * > k_vec[8];

	for( auto it : compression_blocks ) {
		unsigned int min_k;
		if( coding == EXP_GOLOMB ) {
			min_k = it->calculate_min_k_exponential_golomb( );
		} else {
			min_k = it->calculate_min_k_golomb_rice( );
		}
		k_vec[min_k].push_back( it );
	}

	for( int k = 0; k < 8; k++ ) {
		if( k_vec[k].size( ) > 0 ) {
			std::vector< compression_block * >::iterator first, second;
			first = k_vec[k].begin( );
			second = first + 1;
			while( second != k_vec[k].end( ) ) {
				( *first )->add_cb( *( *second ) );
				delete *second;
				second++;
			}
			( *first )->set_k( k );
			new_compression_blocks.push_back( *first );
		}
	}

	std::cout << "size of new CB list " << new_compression_blocks.size( ) << std::endl;
	compression_blocks = new_compression_blocks;
}

static bool are_cluster_lists_the_same( const std::list< cluster > &l1, const std::list< cluster > &l2, unsigned int *c )
{
	assert( l1.size( ) == l2.size( ) );
	unsigned int counter = 0;

	for( auto &c1 : l1 ) {
		compression_block *center1 = c1.get_center( );
		bool found = false;
		for( auto &c2 : l2 ) {
			compression_block *center2 = c2.get_center( );
			if( center1 == center2 ) {
				found = true;
				counter++;
				break;
			}
		}
		if( found == false ) {
			std::cerr << "found " << counter << " out of " << l1.size( ) << std::endl;
			*c = counter;
			return false;
		}
	}
	std::cerr << "all centers are found " << std::endl;
	return true;
}

void k_medoids_clustering::do_clustering( )
{
	if( nclusters == 0 )
		abort( );

	unsigned int cluster_size = ( compression_blocks.size( ) / nclusters );
	unsigned int remainder = compression_blocks.size( ) % nclusters;
	std::list< cluster > clusters;
	auto cb = compression_blocks.begin( );
	std::cout << "cluster size " << cluster_size << ", remainder will be " << remainder << std::endl;
#if 1
	for( unsigned int cluster_counter = 0; cluster_counter < nclusters; cluster_counter++ ) {
		cluster c( *cb, dmeasure );
		cb++;
		for( unsigned int cbcount = 1; cbcount < cluster_size && cb != compression_blocks.end( );
				cbcount++, cb++ ) {
			c.add_node( *cb );
		}
		if( remainder > 0 && cb != compression_blocks.end( ) ) {
			c.add_node( *cb );
			cb++;
			remainder--;
		}

		std::cout << " number of nodes " << c.get_number_of_nodes( ) << std::endl;
		clusters.push_back( c );
	}
#else
	for ( ; cb != compression_blocks.end( ); cb++ ){
		int val = ( *cb )->get_maximum_value( );
		bool found = false;
		for( auto &cl : clusters ) {
			if( val == cl.get_center( )->get_maximum_value( ) ) {
				found = true;
				cl.add_node( *cb );
				break;
			}
		}
		if( !found ) {
			cluster cl( *cb );
			clusters.push_back( cl );
		}
	}
	assert( clusters.size( ) == size );
#endif

	struct timespec start, end, diff;
	unsigned int found = 0;
	unsigned int last_found = std::numeric_limits<unsigned int>::max( );
	int cluster_differences = 0;
	while( 1 ) {
		std::list< cluster > new_clusters;
		std::list< compression_block * > others( compression_blocks );

		for( auto &c : clusters ) {
			//START_PROFILING;
			compression_block *center = c.find_best_center( );
			//END_PROFILING( "find best center" );
			new_clusters.push_back( cluster( center, dmeasure ) );
			for( auto o = others.begin( ); o != others.end( ); o++ ) {
				if( ( *o ) == center ) {
					others.erase( o );
					break;
				}
			}
		}
		START_PROFILING;
		cluster_using_minimum_divergence( new_clusters, others );
		END_PROFILING( "clustering divergence" );

		if( are_cluster_lists_the_same( clusters, new_clusters, &found ) /*counter-- == 0*/ ) {
			break;
		}

		if( found == last_found ) {
			cluster_differences++;
		} else {
			last_found = found;
		}

		if( cluster_differences == 20 )
			break;

		clusters = new_clusters;
	}

	START_PROFILING;
	combine_clusters_and_assign_to_cb_list( clusters );
	END_PROFILING( "combining " );
}

void removeRow( Eigen::MatrixXd& matrix, unsigned int rowToRemove )
{
	unsigned int numRows = matrix.rows( ) - 1;
	unsigned int numCols = matrix.cols( );

	if( rowToRemove < numRows )
		matrix.block( rowToRemove, 0, numRows - rowToRemove, numCols ) = matrix.block( rowToRemove + 1, 0,
				numRows - rowToRemove, numCols );

	matrix.conservativeResize( numRows, numCols );
}

void removeColumn( Eigen::MatrixXd& matrix, unsigned int colToRemove )
{
	unsigned int numRows = matrix.rows( );
	unsigned int numCols = matrix.cols( ) - 1;

	if( colToRemove < numCols )
		matrix.block( 0, colToRemove, numRows, numCols - colToRemove ) = matrix.block( 0, colToRemove + 1,
				numRows, numCols - colToRemove );

	matrix.conservativeResize( numRows, numCols );
}

double agglomerative_clustering::get_proximity_matrix_entry( enum agglomerative_clustering::linkage linkage,
		compression_block * it,
		compression_block * first,
#ifdef USE_PROXIMITY_MATRIX
		Eigen::MatrixXd& proximity_matrix
#else
		double (*dissimilarity)( const pmf * const p1, const pmf * const p2 )
#endif
)
{
	double d;
	switch( linkage ) {
		case LINKAGE_SINGLE:
#ifdef USE_PROXIMITY_MATRIX
			d = compression_block::find_min_dissimilarity( it, first, proximity_matrix );
#endif
			break;

		case LINKAGE_COMPLETE:
#ifdef USE_PROXIMITY_MATRIX
			d = compression_block::find_max_dissimilarity( it, first, proximity_matrix );
#endif
			break;

		case LINKAGE_AVERAGE:
#ifdef USE_PROXIMITY_MATRIX
			d = compression_block::find_average_dissimilarity( it, first, proximity_matrix );
#else
#error "find_average_dissimilarity() needs a dissimilarity version"
#endif
			break;

		case LINKAGE_MODIFIED_COMPLETE:
			d = compression_block::find_modified_max_dissimilarity( it, first,
#ifdef USE_PROXIMITY_MATRIX
					proximity_matrix
#else
					dissimilarity
#endif
					);
			break;

		case LINKAGE_MODIFIED_AVERAGE:
			d = compression_block::find_modified_average_dissimilarity( it, first,
#ifdef USE_PROXIMITY_MATRIX
					proximity_matrix
#else
					dissimilarity
#endif
					);
			break;

		case LINKAGE_CENTROID:
#ifdef USE_PROXIMITY_MATRIX
			d = compression_block::find_centroid_dissimilarity( it, first, proximity_matrix );
#endif
			break;

		case LINKAGE_WARD:
#ifdef USE_PROXIMITY_MATRIX
			d = compression_block::find_ward_dissimilarity( it, first, proximity_matrix );
#endif
			break;

		default:
			abort( );
			break;
	}

	return d;
}

void agglomerative_clustering::merge_closest_pair(
		compression_block *first,
		compression_block *to_be_deleted
		#ifdef USE_PROXIMITY_MATRIX
							,Eigen::MatrixXd& proximity_matrix
		#endif
)
{
	switch( linkage ) {
		case LINKAGE_MODIFIED_COMPLETE:
			( first )->add_cb_and_update_max_dissimilarity( *to_be_deleted,
#ifdef USE_PROXIMITY_MATRIX
					proximity_matrix
#else
					dissimilarity
#endif
					);
			break;

		case LINKAGE_MODIFIED_AVERAGE:
			( first )->add_cb_and_update_sum_dissimilarity( *to_be_deleted,
#ifdef USE_PROXIMITY_MATRIX
					proximity_matrix
#else
					dissimilarity
#endif
					);
			break;

#ifdef USE_PROXIMITY_MATRIX
		case LINKAGE_CENTROID:
		case LINKAGE_WARD:
			first->add_cb_and_update_centroid( *to_be_deleted, proximity_matrix );
			break;
#endif

		default:
			first->add_cb( *to_be_deleted );
			break;
	}
}

static std::mutex barrier;

void agglomerative_clustering::update_combined_cb_with_other_entries( enum agglomerative_clustering::linkage linkage,
		compression_block * it,
		compression_block * first,
		unsigned int min,
		unsigned int current_order,
#ifdef USE_PROXIMITY_MATRIX
		Eigen::MatrixXd& proximity_matrix,
#else
		double (*dissimilarity)( const pmf * const p1, const pmf * const p2 ),
#endif
		Eigen::MatrixXd& proximity_matrix_updated
)
{
	assert( min != current_order );

	const double d = get_proximity_matrix_entry( linkage, it, first,
#ifdef USE_PROXIMITY_MATRIX
			proximity_matrix
#else
			dissimilarity
#endif
			);

	std::lock_guard<std::mutex> block_threads_until_finish_this_job(barrier);

	proximity_matrix_updated( min, current_order ) = d;
	proximity_matrix_updated( current_order, min ) = d;
}

#ifdef USE_PROXIMITY_MATRIX
void agglomerative_clustering::fill_proximity_matrix( Eigen::MatrixXd &proximity_matrix )
{
	for( auto it = compression_blocks.begin( ); it != compression_blocks.end( ); it++ ) {
		int order = ( *it )->get_order( );
		assert( ( *it )->get_number_of_blocks( ) == 1 );
		proximity_matrix( order, order ) = std::numeric_limits<double>::infinity( );
		auto other = it;
		other++;
		while( other != compression_blocks.end( ) ){
			assert( ( *other )->get_number_of_blocks( ) == 1 );
			square_block *sq1 = ( * ( *it )->get_blocks( ).begin( ) );
			square_block *sq2 = ( * ( *other )->get_blocks( ).begin( ) );
			double jsd = dissimilarity( sq1, sq2 );
			int other_order = (*other)->get_order( );
			proximity_matrix( order, other_order ) = jsd;
			proximity_matrix( other_order, order ) = jsd;
			other++;
		}
	}
	std::cout << "computed proximity matrix" << std::endl;
}
#endif

void agglomerative_clustering::do_clustering( )
{
	unsigned int cluster_size = compression_blocks.size( );
	auto cb = compression_blocks.begin( );
	std::cout << "cluster size " << cluster_size << std::endl;
	for( unsigned int cluster_counter = 0; cluster_counter < cluster_size; cluster_counter++ ) {
		( *cb )->set_order( cluster_counter );
		//std::cout << "order " << ( *cb )->get_order( ) << std::endl;
		cb++;
	}

#ifdef USE_PROXIMITY_MATRIX
	Eigen::Matrix< double, Eigen::Dynamic, Eigen::Dynamic > proximity_matrix( cluster_size, cluster_size );
	std::cout << "allocated proximity matrix" << std::endl;

	fill_proximity_matrix( proximity_matrix );
#endif

	remove_cbs_with_duplicate_histograms(
#ifdef USE_PROXIMITY_MATRIX
			proximity_matrix
#endif
	);

	cluster_size = compression_blocks.size( );
	std::cout << "cluster size now " << cluster_size << std::endl;

	/*if( compression_blocks.size( ) > 100 ) {
		k_medoids_clustering k_medoid_clustering( compression_blocks, 50 );
		k_medoid_clustering.do_clustering( );
		clustering_algorithm *algo = &k_medoid_clustering;
		compression_blocks = algo->compression_blocks;

		for( auto it : compression_blocks ) {
			if( linkage == LINKAGE_COMPLETE )
				it->calculate_max_jsd( dissimilarity );
			else
				it->calculate_sum_jsd( dissimilarity );
		}
		cluster_size = compression_blocks.size( );
		std::cout << "cluster size (after k-medoids) " << cluster_size << std::endl;
	}*/

	Eigen::Matrix< double, Eigen::Dynamic, Eigen::Dynamic > proximity_matrix_updated( cluster_size, cluster_size );
	std::cout << "allocated updated proximity matrix" << std::endl;

	unsigned int current_order = 0;
	std::cout << "pushing rows ";
	for( auto it = compression_blocks.begin( ); it != compression_blocks.end( ); it++ ) {
		auto other_it = compression_blocks.begin( );
		int other_order = 0;

		while( other_it != it ) {
			double d = get_proximity_matrix_entry( linkage, *it, *other_it,
#ifdef USE_PROXIMITY_MATRIX
					proximity_matrix
#else
					dissimilarity
#endif
					);

			proximity_matrix_updated( current_order, other_order ) = d;
			proximity_matrix_updated( other_order, current_order ) = d;
			other_it++;
			other_order++;
		}

		proximity_matrix_updated( current_order, current_order ) = std::numeric_limits<double>::infinity( );

		if( ( current_order % 100 ) == 0 )
			std::cout << " " << current_order << " ";
		else
			std::cout << ".";

		current_order++;
	}
	std::cout << std::endl;

	std::cout << "computed updated proximity matrix... now for main loop" << std::endl;

	unsigned int k = nclusters;
	if( k == 0 )
		k = 1;

	while( compression_blocks.size( ) > k ) {
	//while( 0 ) {

		size_t cluster_size = compression_blocks.size( );
		std::cout << cluster_size << " clusters" << std::endl;

		Eigen::MatrixXd::Index minRow, minCol;

		double minJSD = proximity_matrix_updated.minCoeff( &minRow, &minCol );

		std::cout << "Min: " << minJSD << ", at: " << minRow << "," << minCol << std::endl;

		unsigned int min;
		unsigned int max;

		if( minRow == minCol )
			std::cout << "proximity matrix " << proximity_matrix_updated << std::endl;

		assert( minRow != minCol );

		if( minRow < minCol ) {
			min = minRow;
			max = minCol;
		} else {
			min = minCol;
			max = minRow;
		}

		removeColumn( proximity_matrix_updated, max );
		removeRow( proximity_matrix_updated, max );

		std::list< compression_block * >::iterator first = std::next( compression_blocks.begin( ), min );
		std::list< compression_block * >::iterator second = std::next( compression_blocks.begin( ), max );

		compression_block *to_be_deleted = *second;
		merge_closest_pair( *first, to_be_deleted
#ifdef USE_PROXIMITY_MATRIX
					, proximity_matrix
#endif
		);
		std::cout << "combined" << std::endl;
		second = compression_blocks.erase( second );
		delete to_be_deleted;

		current_order = 0;
		auto it = compression_blocks.begin( );
		bool done = false;
		while( !done  ) {

			unsigned int thread_counter = 0;
			std::thread threads[8];
			while( thread_counter < 8 ) {
				if( it != compression_blocks.end( ) ) {
					if( likely( current_order != min ) ) {
						threads[thread_counter] = std::thread(
								agglomerative_clustering::update_combined_cb_with_other_entries,
								linkage, *it, *first, min, current_order,
#ifdef USE_PROXIMITY_MATRIX
								std::ref( proximity_matrix ),
#else
								dissimilarity,
#endif
								std::ref( proximity_matrix_updated ) );
						//std::cout << "thread " << thread_counter << std::endl;
						thread_counter++;
					}
					current_order++;
					it++;
				} else {
					done = true;
					break;
				}
			}

			//std::cout << "joining " << thread_counter << " threads" << std::endl;
			for( unsigned int i = 0; i < thread_counter; i++ ) {
				threads[i].join( );
			}
		}

		print_expected_length( );
	}

	//std::cout << "proximity matrix " << proximity_matrix << std::endl;
}

void agglomerative_clustering::remove_cbs_with_duplicate_histograms(
#ifdef USE_PROXIMITY_MATRIX
			Eigen::MatrixXd& proximity_matrix
#endif
)
{
	std::cout << "number of CBs before removing " << compression_blocks.size( ) << std::endl;

	for( auto first = compression_blocks.begin( ); first != compression_blocks.end( ); ++first ) {
		auto second = first;
		second++;
		while( second != compression_blocks.end( ) ) {
			square_block *sq1 = ( *( *first )->get_blocks( ).begin( ) );
			square_block *sq2 = ( *( *second )->get_blocks( ).begin( ) );
#ifdef USE_PROXIMITY_MATRIX
			if( proximity_matrix( sq1->get_order( ), sq2->get_order( ) ) == 0.0 ) {
#else
			if( are_similar( *sq1, *sq2 ) ) {
#endif
				compression_block *to_be_deleted = *second;
				merge_closest_pair( *first, to_be_deleted
#ifdef USE_PROXIMITY_MATRIX
							, proximity_matrix
#endif
				);
				//std::cout << "combined" << std::endl;
				delete to_be_deleted;

				second = compression_blocks.erase( second );
			} else {
				second++;
			}
		}
	}

	std::cout << "number of CBs after removing " << compression_blocks.size( ) << std::endl;
}

void agglomerative_clustering::find_required_edges( )
{
	unsigned int num_of_edges = 0;
	for( auto first = compression_blocks.begin( ); first != compression_blocks.end( ); first++ ) {
		auto second = first;
		for( ++second; second != compression_blocks.end( ); second++ ) {
			num_of_edges += ( *first )->get_number_of_blocks( ) * ( *second )->get_number_of_blocks( );
		}
	}

	std::cout << "number of required edges " << num_of_edges << std::endl;
}

void divisive_clustering::merge_all( )
{
	std::cout << "number of CBs before merging " << compression_blocks.size( ) << std::endl;

	for( auto first = compression_blocks.begin( ); first != compression_blocks.end( ); ++first ) {
		std::list< compression_block * >::iterator second = first;
		second++;
		for( ; second != compression_blocks.end( ); second = compression_blocks.erase( second ) ) {
			compression_block *to_be_deleted = *second;
			( *first )->add_cb( *to_be_deleted );
			delete to_be_deleted;
		}
	}
	std::cout << "number of CBs after merging " << compression_blocks.size( ) << std::endl;
}

compression_block *divisive_clustering::find_cluster_to_split( )
{
	if( compression_blocks.size( ) == 1 )
		return ( *compression_blocks.begin( ) );
	else {
		size_t max_size = 0;
		compression_block *new_cluster = NULL;
		for( auto cb : compression_blocks ) {
			if( cb->get_number_of_blocks( ) > max_size ) {
				new_cluster = cb;
				max_size = cb->get_number_of_blocks( );
			}
		}
		return new_cluster;
	}
}

void divisive_clustering::do_clustering( )
{
	merge_all( );

	while( compression_blocks.size( ) < 25 ) {

		compression_block *cluster = find_cluster_to_split( );

		std::cout << "clusters " << compression_blocks.size( ) << std::endl;

		cluster->set_internal_order( );
		unsigned int cluster_size = cluster->get_number_of_blocks( );
		std::cout << "cluster size " << cluster_size << std::endl;

		Eigen::Matrix< double, Eigen::Dynamic, Eigen::Dynamic > proximity_matrix( cluster_size,
				cluster_size );
		std::cout << "allocated proximity matrix" << std::endl;

		cluster->fill_proximity_matrix_for_blocks( proximity_matrix, dissimilarity );
		compression_block *new_cluster = cluster->split_node_with_highest_average_dissimilarity( proximity_matrix );

		cluster->migrate_nodes_macnaughton_smith( new_cluster, proximity_matrix, dissimilarity );
		std::cout << "split into " << cluster->get_number_of_blocks( ) << " + "
				<< new_cluster->get_number_of_blocks( ) << std::endl;

		compression_blocks.push_back( new_cluster );

		print_expected_length( );
	}
}

void clustering_algorithm_scattered::remove_cbs_with_duplicate_histograms( )
{
	std::cout << "number of CBs before removing " << compression_blocks.size( ) << std::endl;

	double avg_entropy = 0.0;
	for( auto c : compression_blocks ) {
		avg_entropy += c->get_entropy( );
	}
	std::cout << "total entropy before " << avg_entropy << std::endl;
	std::cout << "average entropy before " << avg_entropy / compression_blocks.size( ) << std::endl;

	for( auto first = compression_blocks.begin( ); first != compression_blocks.end( ); ++first ) {
		std::list< compression_block * >::iterator second = first;
		second++;
		while( second != compression_blocks.end( ) ) {
			//if( pmf::do_tables_match( **first, **second ) ) {
			//if( pmf::do_pmfs_exactly_match( **first, **second ) ) {
			if( dissimilarity( *first, *second ) == 0.0 ) {
				compression_block *to_be_deleted = *second;
				( *first )->add_cb( *to_be_deleted );
				//std::cout << "combined" << std::endl;
				second = compression_blocks.erase( second );
				delete to_be_deleted;
			} else {
				second++;
			}
		}
	}
	std::cout << "number of CBs after removing " << compression_blocks.size( ) << std::endl;

	avg_entropy = 0.0;
	for( auto c : compression_blocks ) {
		avg_entropy += c->get_entropy( );
	}
	std::cout << "total entropy after " << avg_entropy << std::endl;
	std::cout << "average entropy after " << avg_entropy / compression_blocks.size( ) << std::endl;
}

