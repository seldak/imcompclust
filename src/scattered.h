/*
 * MIT License
 *
 * Copyright (c) 2014 Sameh Samir Hassan
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef SCATTERED_H_
#define SCATTERED_H_

#include <map>
#include <vector>

class square_block;
class compression_block;
class cluster;
class complete_graph;
enum __dissimilarity_measure;

extern void print_length_with_bpp( const char *str, size_t length, size_t pixels );

class clustering_algorithm_scattered : public clustering_algorithm {
public:
	clustering_algorithm_scattered( std::list< compression_block * > &compression_blocks,
			enum __dissimilarity_measure dmeasure,
			unsigned int k = 0 ) :
			clustering_algorithm( compression_blocks, dmeasure ), nclusters( k )
	{

	}

	virtual ~clustering_algorithm_scattered( )
	{
	}

	virtual void do_clustering( ) = 0;

	size_t print_expected_length( )
	{
		size_t length = 0;
		size_t num_of_cbs = compression_blocks.size( );
		int num_of_needed_bits_for_cbs = ceil( log2( num_of_cbs ) );
		for( auto cb : compression_blocks ) {
			size_t expected_length = cb->get_expected_length( );
			size_t huffman_tree_length = 9 * cb->get_num_of_unique_values( ) + 1;
			size_t map_bits = num_of_needed_bits_for_cbs * cb->get_number_of_blocks( );
			length += ( expected_length + huffman_tree_length + map_bits );
		}
		char buf[100];
		sprintf( buf, "expected_length for %lu blocks", num_of_cbs );
		print_length( buf, length );

		return length;
	}

	static void find_best_cluster( std::list< cluster > &cluster_list, compression_block *node );
	void combine_clusters_and_assign_to_cb_list( std::list< cluster > &clusters );
	size_t cluster_cbs( );
	void remove_cbs_with_duplicate_histograms( );

protected:
	unsigned int nclusters;
};

class cluster_for_golomb_codes: public clustering_algorithm_scattered {
public:
	cluster_for_golomb_codes( std::list< compression_block * > &compression_blocks, entropy_coding coding ) :
			clustering_algorithm_scattered( compression_blocks, NO_DISSIMILARITY_MEASURE ), coding( coding )
	{
	}
	void do_clustering( );

private:
	entropy_coding coding;
};

class k_medoids_clustering: public clustering_algorithm_scattered {
public:
	k_medoids_clustering( std::list< compression_block * > &compression_blocks,
			enum __dissimilarity_measure dmeasure,
			unsigned int k ) :
			clustering_algorithm_scattered( compression_blocks, dmeasure, k )
	{
	}
	void do_clustering( );
};

class cluster_using_kl_and_jsd : public clustering_algorithm_scattered {
public:
	cluster_using_kl_and_jsd( std::list< compression_block * > &compression_blocks ) :
			clustering_algorithm_scattered( compression_blocks, DISSIMILARITY_JENSEN_SHANNON )
	{
	}
	void do_clustering( );
};

class cluster_using_g_center_and_jsd : public clustering_algorithm_scattered {
public:
	cluster_using_g_center_and_jsd( std::list< compression_block * > &compression_blocks ) :
			clustering_algorithm_scattered( compression_blocks, DISSIMILARITY_JENSEN_SHANNON )
	{
	}
	void do_clustering( );
};

#define USE_PROXIMITY_MATRIX 1

class agglomerative_clustering: public clustering_algorithm_scattered {
public:
	enum linkage {
		LINKAGE_SINGLE,
		LINKAGE_COMPLETE,
		LINKAGE_AVERAGE,
		LINKAGE_MODIFIED_COMPLETE,
		LINKAGE_MODIFIED_AVERAGE,
		LINKAGE_CENTROID,
		LINKAGE_WARD,
	};

	agglomerative_clustering( std::list< compression_block * > &compression_blocks,
			enum linkage linkage,
			enum __dissimilarity_measure dmeasure,
			unsigned int k ) :
			clustering_algorithm_scattered( compression_blocks, dmeasure, k ), linkage( linkage )
	{
	}
	void do_clustering( );
#ifdef USE_PROXIMITY_MATRIX
	void fill_proximity_matrix( Eigen::MatrixXd& proximity_matrix );
#endif
	void remove_cbs_with_duplicate_histograms(
#ifdef USE_PROXIMITY_MATRIX
			Eigen::MatrixXd& proximity_matrix
#endif
			);
	void find_required_edges( );

private:
	void merge_closest_pair( compression_block *first, compression_block *to_be_deleted
#ifdef USE_PROXIMITY_MATRIX
			, Eigen::MatrixXd& proximity_matrix
#endif

			);
	static double get_proximity_matrix_entry( enum agglomerative_clustering::linkage linkage,
			compression_block * it,
			compression_block * first,
#ifdef USE_PROXIMITY_MATRIX
			Eigen::MatrixXd& proximity_matrix
#else
			double (*dissimilarity)( const pmf * const p1, const pmf * const p2 )
#endif
			);
	static void update_combined_cb_with_other_entries( enum agglomerative_clustering::linkage linkage,
			compression_block *it,
			compression_block *first,
			unsigned int min,
			unsigned int current_order,
#ifdef USE_PROXIMITY_MATRIX
			Eigen::MatrixXd& proximity_matrix,
#else
			double (*dissimilarity)( const pmf * const p1, const pmf * const p2 ),
#endif
			Eigen::MatrixXd& proximity_matrix_updated );

	enum linkage linkage;
};

static std::ostream& operator<<( std::ostream & os, enum agglomerative_clustering::linkage link )
{
	switch( link ) {
		case agglomerative_clustering::LINKAGE_SINGLE:
			return os << "SINGLE";
		case agglomerative_clustering::LINKAGE_COMPLETE:
			return os << "COMPLETE";
		case agglomerative_clustering::LINKAGE_AVERAGE:
			return os << "AVERAGE";
		case agglomerative_clustering::LINKAGE_MODIFIED_COMPLETE:
			return os << "MODIFIED_COMPLETE";
		case agglomerative_clustering::LINKAGE_MODIFIED_AVERAGE:
			return os << "MODIFIED_AVERAGE";
		case agglomerative_clustering::LINKAGE_CENTROID:
			return os << "CENTROID";
		case agglomerative_clustering::LINKAGE_WARD:
			return os << "WARD";
			// omit default case to trigger compiler warning for missing cases
	};
	return os << "Unknown";
}

class divisive_clustering: public clustering_algorithm_scattered {
public:
	divisive_clustering( std::list< compression_block * > &compression_blocks,
			enum __dissimilarity_measure dmeasure ) :
			clustering_algorithm_scattered( compression_blocks, dmeasure )
	{
	}
	void merge_all( );
	compression_block *find_cluster_to_split( );
	void do_clustering( );
};

class modified_cluster_cbs: public clustering_algorithm_scattered {
public:
	modified_cluster_cbs( std::list< compression_block * > &compression_blocks,
			enum __dissimilarity_measure dmeasure ) :
			clustering_algorithm_scattered( compression_blocks, dmeasure )
	{
	}
	void do_clustering( );
};

class compression_algorithm_scattered: public compression_algorithm_under_test {
public:
	compression_algorithm_scattered( cv::Mat &in_mat,
			block_filtering_method bfiltering_method,
			int fixed_scale,
			bool separate_dict_compression,
			unsigned int k,
			compression_filter pre_filter,
			entropy_coding coding,
			unsigned int algorithm,
			enum agglomerative_clustering::linkage linkage,
			enum __dissimilarity_measure dmeasure ) :
			compression_algorithm_under_test( in_mat, bfiltering_method, fixed_scale,
					separate_dict_compression, pre_filter, coding,
					compression_algorithm_under_test::AUT_SCATTERED )
	{
		switch( algorithm ) {
			case 0:
				algo = new cluster_for_golomb_codes( compression_blocks, coding );
				break;
			case 1:
				algo = new cluster_using_kl_and_jsd( compression_blocks );
				break;
			case 2:
				algo = new cluster_using_g_center_and_jsd( compression_blocks );
				break;
			case 3:
				algo = new modified_cluster_cbs( compression_blocks, dmeasure );
				break;
			case 4:
				/* K was usually 6 here*/
				algo = new k_medoids_clustering( compression_blocks, dmeasure, k );
				break;
			case 5:
				algo = new agglomerative_clustering( compression_blocks, linkage, dmeasure, k );
				break;
			case 6:
				algo = new divisive_clustering( compression_blocks, dmeasure );
				break;
			default:
				abort( );
		}
	}
	~compression_algorithm_scattered( )
	{
		delete algo;
	}

	void cluster( )
	{
		algo->do_clustering( );
	}

private:
	clustering_algorithm_scattered *algo;
};

#endif /* SCATTERED_H_ */
