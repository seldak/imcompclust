/*
 * MIT License
 *
 * Copyright (c) 2014 Sameh Samir Hassan
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <sstream>
#include <string>
#include <stdio.h>
#include <list>
#include <map>
#include <iomanip>
#include <vector>
#include <queue>
#include <assert.h>
#include <algorithm>
#include <iterator>
#include <stdint.h>
#include <fcntl.h>
#include <sys/types.h>
#include <fstream>
#include <bitset>
#include <unistd.h>
#include <getopt.h>
#include <pthread.h>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <boost/dynamic_bitset.hpp>
#include <boost/iostreams/filtering_streambuf.hpp>
#include <boost/iostreams/filter/zlib.hpp>
#include <boost/iostreams/filter/gzip.hpp>
#include <boost/iostreams/filter/bzip2.hpp>
#include <boost/iostreams/write.hpp>

#include <boost/filesystem.hpp>

#include "pmf.h"
#include "square_block.h"
#include "edge.h"
#include "compression_block.h"
#include "enums.h"
#include "utils.h"

#include "algo.h"
#include "aut.h"
#include "adjacent.h"
#include "scattered.h"
#include "multi_scattered.h"

#define _XOPEN_SOURCE 700

#define ROW_NUM 40
#define COL_NUM 40

#define MAX1    52
#define MIN1    50
#define NUM_OF_VALUES1  (MAX1-MIN1+1)

#define MAX2    72
#define MIN2    70
#define NUM_OF_VALUES2  (MAX2-MIN2+1)

#define TOTAL_NUM_OF_PIXELS   (40*40)
#define NUM_OF_PIXELS_IN_SEG  (TOTAL_NUM_OF_PIXELS/2)
#define NUM_OF_VALUES         6

#define SCALE_DIFF            (MAX_SCALE-MIN_SCALE)

static pmf table_total;
static pmf table_first;
static pmf table_second;

void print_length( const char *str, size_t length )
{
	std::cout << str << length << " bits\t(" << length / 8 << " bytes)" << std::endl;
}

void print_length_with_bpp( const char *str, size_t length, size_t pixels )
{
	std::cout << str << length << " bits\t(" << length / 8 << " bytes) " << 
		length / double( pixels ) << " bpp" << std::endl;
}

template< typename bset >
void set_in_range( bset& b, unsigned int value, int bits_needed )
{
	//b.clear( );
	for( int i = 0; i < bits_needed; ++i, value >>= 1 ) {
		std::cout << "value " << value << " " << ( value & 1 ) << std::endl;
		b[i] = ( value & 1 );
		std::cout << "b " << b[i] << std::endl;
	}
	std::cout << b.to_ulong( ) << std::endl;
}

static void print_entropy_tables( )
{
	std::cout << "Total probability/entropy table" << std::endl;
	std::cout << table_total;
	std::cout << "Total probability/entropy (local) = " << table_total.get_entropy( ) << std::endl;

	std::cout << "First probability/entropy table" << std::endl;
	std::cout << table_first;

	std::cout << "Second probability/entropy table" << std::endl;
	std::cout << table_second;
}

void compute_entropies( struct matrix *matrix )
{
	rectangle rect;
	rect.point.x = 0;
	rect.point.y = 0;
	rect.w = matrix->cols;
	rect.h = matrix->rows;
	table_total.compute_pmf( matrix, rect, false );
	rect.point.x = 0;
	rect.point.y = 0;
	rect.w = matrix->cols / 2;
	rect.h = matrix->rows;
	table_first.compute_pmf( matrix, rect, false );
	rect.point.x = matrix->cols / 2;
	rect.point.y = 0;
	rect.w = matrix->cols / 2;
	rect.h = matrix->rows;
	table_second.compute_pmf( matrix, rect, false );

	print_entropy_tables( );
}

void compress( struct matrix *image )
{
	std::string codeword1, codeword2;

	print_entropy_tables( );

	rectangle rect_total;
	rect_total.point.x = rect_total.point.y = 0;
	rect_total.w = image->cols;
	rect_total.h = image->rows;

	codeword1 = table_total.compress_huffman( image, rect_total );

	std::cout << codeword1 << std::endl << "length = " << codeword1.length( ) << " ("
			<< codeword1.length( ) / 8 << " bytes)" << std::endl;

	std::cout << "first" << std::endl;
	rectangle rect1;
	rect1.point.x = rect1.point.y = 0;
	rect1.w = image->cols / 2;
	rect1.h = image->rows;
	codeword2 = table_first.compress_huffman( image, rect1 );

	std::cout << "second" << std::endl;
	rectangle rect2;
	rect2.point.x = image->cols / 2;
	rect2.point.y = 0;
	rect2.w = image->cols / 2;
	rect2.h = image->rows;
	codeword2 += table_second.compress_huffman( image, rect2 );

	std::cout << codeword2 << std::endl << "length = " << codeword2.length( ) << " ("
			<< codeword2.length( ) / 8 << " bytes)" << std::endl;

	struct matrix decompressed = matrix( image->rows, image->cols );
	pmf::decompress_huffman( &decompressed, codeword1, 0, rect_total );

	assert( memcmp(image, decompressed.data, image->rows * image->cols ) == 0 );
}

template< class type >
static void print_saliency_matrix( type *matrix, unsigned int rows, unsigned int cols )
{
	size_t x, y;
	std::cout << "\t";
	for( x = 0; x < cols; x++ )
		std::cout << std::setw( 6 ) << x << " ";
	std::cout << std::endl << std::endl;
	for( y = 0; y < rows; y++ ) {
		std::cout << y << "\t";
		for( x = 0; x < cols; x++ ) {
			std::cout << std::setw( 4 ) << std::fixed << std::setprecision( 4 ) << matrix[y * rows + x]
					<< " ";
		}
		std::cout << std::endl;
	}
}

void compute_kadir_brady_sparse_matrix( struct matrix *mat )
{
	unsigned int x, y;
	int s;
	std::map< int, double > H; /* map from scale to entropy of a given pixel */
	bool first_time = true;

	std::map< int, pmf > scale_entropy_table;

	std::list< int >::const_iterator lit;
	int *scale_matrix = static_cast< int * >( calloc( mat->rows * mat->cols, sizeof(int) ) );
	double *weight_matrix = static_cast< double * >( calloc( mat->rows * mat->cols, sizeof(double) ) );

	double threshold = 1.6;
	int *scale_matrix_threshold = static_cast< int * >( calloc( mat->rows * mat->cols, sizeof(int) ) );
	double *weight_matrix_threshold = static_cast< double * >( calloc( mat->rows * mat->cols, sizeof(double) ) );

	//for( i = 10; i <= 30; i++ ) {
	//for( j = 10; j <= 30; j++ ) {
	for( y = 0; y < mat->rows; y++ ) {
		for( x = 0; x < mat->cols; x++ ) {

			//i = 20;
			//j = 10;
			for( s = MIN_SCALE - 1; s <= MAX_SCALE; s++ ) {
				rectangle rect;
				rect.point.x = x;
				rect.point.y = y;
				rect.w = s;
				rect.h = s;

				scale_entropy_table[s].compute_pmf( mat, rect, true );
				H[s] = scale_entropy_table[s].get_entropy( );

				if( first_time )
					std::cout << "scale: " << s << ", entropy: " << H[s] << std::endl;
			}

			std::list< int > Sp;

			if( H[MIN_SCALE] > H[MIN_SCALE + 1] )
				Sp.push_back( MIN_SCALE );
			for( s = MIN_SCALE + 1; s < MAX_SCALE; s++ ) {
				if( ( H[s - 1] < H[s] ) && ( H[s] > H[s + 1] ) ) {
					Sp.push_back( s );
				}
			}
			if( H[MAX_SCALE] > H[MAX_SCALE - 1] )
				Sp.push_back( MAX_SCALE );

			if( first_time ) {
				for( lit = Sp.begin( ); lit != Sp.end( ); lit++ ) {
					std::cout << ( *lit ) << std::endl;
				}
			}

			double max_weight = 0;
			int max_weight_scale = 0;

			for( lit = Sp.begin( ); lit != Sp.end( ); lit++ ) {
				s = *lit;

				double prob_diff = 0.0;
				double Weight_d = 0.0;
				double Entropy_weight = 0.0;

				pmf &first_entropy_table = scale_entropy_table[s];
				pmf &second_entropy_table = scale_entropy_table[s - 1];

				if( first_time )
					std::cout << "scale = " << s << std::endl;

				prob_diff = first_entropy_table.calculate_probability_diff( second_entropy_table,
						first_time );

				Weight_d = ( s * s ) / ( 2 * s - 1 ) * prob_diff;
				Entropy_weight = H[s] * Weight_d;

				if( first_time )
					std::cout << "s: " << s << ", Y_D(s,x) = " << Entropy_weight << std::endl;
				if( Entropy_weight > max_weight ) {
					max_weight = Entropy_weight;
					max_weight_scale = s;
				}

			}
			if( first_time )
				std::cout << "maximum: s: " << max_weight_scale << ", Y_D(s,x) = " << max_weight << std::endl;
			scale_matrix[x * mat->rows + y] = max_weight_scale;
			weight_matrix[x * mat->rows + y] = max_weight;

			if( max_weight > threshold ) {
				scale_matrix_threshold[x * mat->rows + y] = max_weight_scale;
				weight_matrix_threshold[x * mat->rows + y] = max_weight;
			}

			first_time = false;
		}
	}

	print_matrix( std::cout, scale_matrix, mat->rows, mat->cols );

	std::cout << std::endl;

	print_saliency_matrix( weight_matrix, mat->rows, mat->cols );

	std::cout << "\t";
	for( x = 0; x < mat->cols; x++ )
		std::cout << std::setw( 2 ) << x << " ";
	std::cout << std::endl << std::endl;
	for( y = 0; y < mat->rows; y++ ) {
		std::cout << y << "\t";
		for( x = 0; x < mat->cols; x++ ) {
			std::ostringstream os;
			if( scale_matrix_threshold[y * mat->rows + x] > 0 )
				os << scale_matrix_threshold[y * mat->rows + x];
			std::cout << std::setw( 2 ) << os.str( ) << " ";
		}
		std::cout << std::endl;
	}

	std::cout << std::endl;

	print_saliency_matrix( weight_matrix_threshold, mat->rows, mat->cols );

	free( scale_matrix );
	free( weight_matrix );

	free( scale_matrix_threshold );
	free( weight_matrix_threshold );
}

void compute_minimum_kadir_brady_sparse_matrix( struct matrix *matrix )
{
	unsigned int x, y;
	int s;
	std::map< int, double > H; /* map from scale to entropy of a given pixel */
	bool first_time = true;

	std::map< int, pmf > scale_entropy_table;

	std::list< int >::const_iterator lit;
	int *scale_matrix = static_cast< int * >( calloc( matrix->rows * matrix->cols, sizeof(int) ) );
	double *weight_matrix = static_cast< double * >( calloc( matrix->rows * matrix->cols, sizeof(double) ) );

	const double threshold = 0.8;
	int *scale_matrix_threshold = static_cast< int * >( calloc( matrix->rows * matrix->cols, sizeof(int) ) );
	double *weight_matrix_threshold = static_cast< double * >( calloc( matrix->rows * matrix->cols, sizeof(double) ) );

	for( y = 0; y < matrix->rows; y++ ) {
		for( x = 0; x < matrix->cols; x++ ) {

			for( s = MIN_SCALE - 1; s <= MAX_SCALE; s++ ) {
				rectangle rect;
				rect.point.x = x;
				rect.point.y = y;
				rect.w = s;
				rect.h = s;

				scale_entropy_table[s].compute_pmf( matrix, rect, true );
				H[s] = scale_entropy_table[s].get_entropy( );

				if( first_time )
					std::cout << "scale: " << s << ", entropy: " << H[s] << std::endl;
			}

			std::list< int > Sp;

			if( H[MIN_SCALE] < H[MIN_SCALE + 1] )
				Sp.push_back( MIN_SCALE );

			for( s = MIN_SCALE + 1; s < MAX_SCALE; s++ ) {
				if( first_time )
					std::cout << "   scale - 1:" << s - 1 << ", scale" << s << ", scale + 1: " << s + 1
							<< std::endl;
				if( ( H[s - 1] > H[s] ) && ( H[s] < H[s + 1] ) ) {
					Sp.push_back( s );
				}
			}
			if( H[MAX_SCALE] < H[MAX_SCALE - 1] )
				Sp.push_back( MAX_SCALE );

			if( first_time ) {
				for( lit = Sp.begin( ); lit != Sp.end( ); lit++ ) {
					std::cout << ( *lit ) << std::endl;
				}
			}

			double min_weight = INFINITY;
			int min_weight_scale = 0;

			for( lit = Sp.begin( ); lit != Sp.end( ); lit++ ) {
				s = *lit;

				double prob_diff = 0.0;
				double Weight_d = 0.0;
				double Entropy_weight = 0.0;

				pmf &first_entropy_table = scale_entropy_table[s];
				pmf &second_entropy_table = scale_entropy_table[s - 1];

				if( first_time )
					std::cout << "scale = " << s << std::endl;

				prob_diff = first_entropy_table.calculate_probability_diff( second_entropy_table,
						first_time );

				Weight_d = ( s * s ) / ( 2 * s - 1 ) * prob_diff;
				Entropy_weight = H[s] * Weight_d;

				if( first_time )
					std::cout << "s: " << s << ", Y_D(s,x) = " << Entropy_weight << std::endl;
				if( Entropy_weight < min_weight ) {
					min_weight = Entropy_weight;
					min_weight_scale = s;
				}

			}
			if( first_time )
				std::cout << "minimum: s: " << min_weight_scale << ", Y_D(s,x) = " << min_weight << std::endl;
			scale_matrix[y * matrix->rows + x] = min_weight_scale;
			weight_matrix[y * matrix->rows + x] = min_weight;

			if( min_weight < threshold ) {
				scale_matrix_threshold[y * matrix->rows + x] = min_weight_scale;
				weight_matrix_threshold[y * matrix->rows + x] = min_weight;
			}

			first_time = false;
		}
	}

	print_matrix( std::cout, scale_matrix, matrix->rows, matrix->cols );

	std::cout << std::endl;

	print_saliency_matrix( weight_matrix, matrix->rows, matrix->cols );

	std::cout << "\t";
	for( x = 0; x < matrix->cols; x++ )
		std::cout << std::setw( 2 ) << x << " ";
	std::cout << std::endl << std::endl;
	for( y = 0; y < matrix->rows; y++ ) {
		std::cout << y << "\t";
		for( x = 0; x < matrix->cols; x++ ) {
			std::ostringstream os;
			if( scale_matrix_threshold[y * matrix->rows + x] > 0 )
				os << scale_matrix_threshold[y * matrix->rows + x];

			std::string str( os.str( ) );
			std::cout << std::setw( 2 ) << str << " ";
		}
		std::cout << std::endl;
	}

	std::cout << std::endl;

	print_saliency_matrix( weight_matrix_threshold, matrix->rows, matrix->cols );

	free( scale_matrix );
	free( weight_matrix );

	free( scale_matrix_threshold );
	free( weight_matrix_threshold );
}

double kb_for_block( struct matrix *matrix, unsigned int x, unsigned int y, int s )
{
	pmf scale_entropy_table;
	double H;

	pmf scale_entropy_table_prev;

	rectangle rect_current_scale;
	rect_current_scale.point.x = x;
	rect_current_scale.point.y = y;
	rect_current_scale.w = s;
	rect_current_scale.h = s;
	scale_entropy_table.compute_pmf( matrix, rect_current_scale, false );
	H = scale_entropy_table.get_entropy( );

	std::cout << "Entropy = " << H << std::endl;

	rectangle rect_previous_scale;
	rect_previous_scale.point.x = x;
	rect_previous_scale.point.y = y;
	rect_previous_scale.w = s - 1;
	rect_previous_scale.h = s - 1;
	scale_entropy_table_prev.compute_pmf( matrix, rect_previous_scale, false );

	double prob_diff = 0.0;
	double Weight_d = 0.0;
	double Entropy_weight = 0.0;

	pmf &first_entropy_table = scale_entropy_table;
	pmf &second_entropy_table = scale_entropy_table_prev;

	prob_diff = first_entropy_table.calculate_probability_diff( second_entropy_table, false );

	Weight_d = ( s * s ) / ( 2 * s - 1 ) * prob_diff;
	Entropy_weight = H * Weight_d;

	return Entropy_weight;
}

double calculate_s_weight( std::vector< std::vector< square_block * > > & es_grid,
		unsigned int s,
		struct matrix *matrix )
{
	double s_weight;

	double total_entropy = 0;
	int total_blocks = 0;
	int total_bins = 0;

	unsigned int x;
	unsigned int y;

	//std::vector< pmf * > pvec;

	/*char buf[100];
	sprintf( buf, "weight_%d.txt", s );
	std::ofstream stream;
	stream.open( buf, std::ofstream::out | std::ofstream::trunc );*/

	for( y = 0; y != es_grid.size( ); y++ ) {
		for( x = 0; x != es_grid[y].size( ); x++ ) {
			double H = es_grid[y][x]->get_entropy( );
			total_entropy += H;
			//std::cerr << H << std::endl;
			//stream << H << std::endl;
			total_blocks++;
			//pvec.push_back( &es_grid[s][y][x] );

#if 0
			stream << "s = " << es_grid[y][x]->get_scale( ) << ", x = " << es_grid[y][x]->get_x( )
					<< ", y = " << es_grid[y][x]->get_y( ) << std::endl;
			stream << "kb-weight is = "
					<< kb_for_block( mat_ptr, rows, cols, es_grid[y][x]->get_x( ),
							es_grid[y][x]->get_y( ), es_grid[y][x]->get_scale( ) )
					<< ", number of values = " << es_grid[y][x]->get_histogram_bins( ) << std::endl;

			total_bins += es_grid[y][x]->get_histogram_bins( );
			stream << ", neighbours [" << es_grid[y][x]->get_number_of_neighbours( ) << "] are: "
					<< std::endl;

			for( unsigned int it = 0; it != es_grid[y][x]->get_number_of_neighbours( ); it++ ) {
				square_block *neighbour = es_grid[y][x]->get_neighbour( it );

				stream << "\t x = " << neighbour->get_x( ) << "\t y = " << neighbour->get_y( )
						<< "\t\tjsd-divergence is = "
						<< neighbour->get_neighbour_jensen_shannon_divergence( it ) << std::endl;

			}
#endif
		}
	}

	//stream.close( );

	//std::cout << "mixture entropy = " << pmf::get_mixture_entropy( pvec ) << std::endl;

	std::cerr << "total entropy = " << total_entropy << " and total blocks = " << total_blocks
			<< ", E/blocks = " << total_entropy / total_blocks << ", avg Histogram bins = "
			<< total_bins / total_blocks << std::endl;

	//s_weight = total_entropy * total_bins / ( total_blocks * total_blocks ) / ( ( s * s ) / ( 2 * s - 1 ) );
	s_weight = total_entropy / double( total_blocks );
	std::cout << "S_weight = " << s_weight << std::endl;
	return s_weight;
}

int get_new_value_for_geometric_distribution( int old_value )
{
	int new_value;
	if( old_value == 0 )
		new_value = old_value;
	else if( old_value <= 128 )
		new_value = 2 * old_value - 1;
	else if( old_value <= 255 )
		new_value = 2 * ( 256 - old_value );
	else
		abort( );

	assert( 0 <= new_value && new_value <= 255 );
	return new_value;
}

int reverse_geometric_distribution_altering( uint8_t coded_value )
{
	int old_value;
	if( coded_value == 0 )
		old_value = coded_value;
	else if( coded_value % 2 != 0 ) // odd, <= 128
		old_value = ( coded_value + 1 ) / 2;
	else
		// even
		old_value = 256 - coded_value / 2;

	assert( 0 <= old_value && old_value <= 255 );

	return old_value;
}

void subtract_left( struct matrix *mat, bool encode_for_geometric_distribution )
{
	unsigned int i;
	unsigned int j;
	unsigned int x_start = 0;
	unsigned int y_start = 0;
	unsigned int x_end = mat->cols;
	unsigned int y_end = mat->rows;
	uint8_t last_val = 0;

	for( j = y_start; j < y_end; j++ ) {
		last_val = 0;

		for( i = x_start; i < x_end; i++ ) {
			int index = j * mat->cols + i;
			uint8_t val = mat->data[index];
			uint8_t error = ( uint8_t )( val - last_val );
			if( encode_for_geometric_distribution ) {
				mat->data[index] = ( uint8_t )get_new_value_for_geometric_distribution( error );
			} else {
				mat->data[index] = error;
			}
			last_val = val;
		}
	}
}

void reverse_subtract_left( struct matrix *mat, bool revert_for_geometric_distribution )
{
	unsigned int i;
	unsigned int j;
	unsigned int x_start = 0;
	unsigned int y_start = 0;
	unsigned int x_end = mat->cols;
	unsigned int y_end = mat->rows;
	unsigned int last_val;

	for( j = y_start; j < y_end; j++ ) {
		last_val = 0;
		for( i = x_start; i < x_end; i++ ) {
			int index = j * mat->cols + i;
			uint8_t val = mat->data[index];
			if( revert_for_geometric_distribution ) {
				val = reverse_geometric_distribution_altering( mat->data[j * mat->cols + i] );
			} else {
				val = ( uint8_t )mat->data[j * mat->cols + i];
			}
			mat->data[index] = val + last_val;
			last_val = mat->data[index];
		}
	}
}

void print_histogram( cv::Mat &matrix, const char *str )
{
	unsigned int histogram[256] = { };

	std::ofstream stream;

	remove( str );

	stream.open( str, std::ios_base::app );

	for( int j = 0; j < matrix.rows; j++ ) {
		uchar *row = matrix.ptr< uchar >( j );

		for( int i = 0; i < matrix.cols; i++ ) {
			histogram[int( row[i] )]++;
		}
	}

	for( int i = 0; i < 256; i++ ) {
		stream << i << " " << histogram[i] << std::endl;
	}
	stream.close( );
}

void a_b_average_filter( struct matrix *mat, bool encode_for_geometric_distribution )
{
	int a;
	int b;

	uint8_t *new_matrix = static_cast< uint8_t * >( calloc( sizeof(uint8_t), mat->rows * mat->cols ) );

	int i, j;
	int x_start = 0;
	int y_start = 0;
	int x_end = mat->cols;
	int y_end = mat->rows;

	for( j = y_start; j < y_end; j++ ) {
		for( i = x_start; i < x_end; i++ ) {
			int val = mat->data[j * mat->cols + i];

			//     b
			//  a  x
			if( j == y_start && i == x_start ) {
				if( encode_for_geometric_distribution ) {
					new_matrix[j * mat->cols + i] = get_new_value_for_geometric_distribution( val );
				} else {
					new_matrix[j * mat->cols + i] = val;
				}
				continue;
			} else if( j == y_start ) {
				b = 0;
				a = mat->data[j * mat->cols + ( i - 1 )];
			} else if( i == x_start ) {
				a = 0;
				b = mat->data[( j - 1 ) * mat->cols + i];
			} else {
				a = mat->data[j * mat->cols + ( i - 1 )];
				b = mat->data[( j - 1 ) * mat->cols + i];
			}
			//std::cout << "a=" << a << ",b=" << b << "c=," << c;

			int avg = ( a + b ) / 2;
			//std::cout << ",val = " << val << std::endl;

			uint8_t pre_val = ( uint8_t )( val - avg );
			if( encode_for_geometric_distribution ) {
				new_matrix[j * mat->cols + i] = get_new_value_for_geometric_distribution( pre_val );
			} else {
				new_matrix[j * mat->cols + i] = pre_val;
			}
		}
	}

	memcpy( mat->data, new_matrix, sizeof(uint8_t) * mat->rows * mat->cols );

	free( static_cast< void * >( new_matrix ) );
}

void reverse_a_b_average_filter( struct matrix *mat,
		bool revert_for_geometric_distribution )
{
	int a;
	int b;

	int i, j;
	int x_start = 0;
	int y_start = 0;
	int x_end = mat->cols;
	int y_end = mat->rows;

	for( j = y_start; j < y_end; j++ ) {
		for( i = x_start; i < x_end; i++ ) {
			int val;
			if( revert_for_geometric_distribution ) {
				val = reverse_geometric_distribution_altering( mat->data[j * mat->cols + i] );
			} else {
				val = ( uint8_t )mat->data[j * mat->cols + i];
			}

			//     b
			//  a  x
			if( j == y_start && i == x_start ) {
				mat->data[j * mat->cols + i] = ( uint8_t )val;
				continue;
			} else if( j == y_start ) {
				b = 0;
				a = mat->data[j * mat->cols + ( i - 1 )];
			} else if( i == x_start ) {
				a = 0;
				b = mat->data[( j - 1 ) * mat->cols + i];
			} else {
				a = mat->data[j * mat->cols + ( i - 1 )];
				b = mat->data[( j - 1 ) * mat->cols + i];
			}
			//std::cout << "a=" << a << ",b=" << b << "c=," << c;

			int avg = ( a + b ) / 2;
			//std::cout << ",val = " << val << std::endl;

			mat->data[j * mat->cols + i] = ( uint8_t )( val + avg );
		}
	}
}

uint8_t paeth_predictor( uint8_t a, uint8_t b, uint8_t c )
{
	int p = a + b - c;
	int pa = abs( p - a );
	int pb = abs( p - b );
	int pc = abs( p - c );
	if( pa <= pb && pa <= pc )
		return a;
	else if( pb <= pc )
		return b;
	else
		return c;
}

void paeth_filter( struct matrix *mat, bool encode_for_geometric_distribution )
{
	int a;
	int b;
	int c;

	uint8_t *new_matrix = static_cast< uint8_t * >( calloc( sizeof(uint8_t), mat->rows * mat->cols ) );

	int i, j;
	int x_start = 0;
	int y_start = 0;
	int x_end = mat->cols;
	int y_end = mat->rows;

	for( j = y_start; j < y_end; j++ ) {
		for( i = x_start; i < x_end; i++ ) {
			int val = mat->data[j * mat->cols + i];
			uint8_t p;

			//  c  b  d
			//  a  x
			if( j == y_start && i == x_start ) {
				if( encode_for_geometric_distribution ) {
					new_matrix[j * mat->cols + i] = get_new_value_for_geometric_distribution( val );
				} else {
					new_matrix[j * mat->cols + i] = val;
				}
				continue;
			} else if( j == y_start ) {
				c = b = 0;
				a = mat->data[j * mat->cols + ( i - 1 )];
			} else if( i == x_start ) {
				c = a = 0;
				b = mat->data[( j - 1 ) * mat->cols + i];
			} else {
				a = mat->data[j * mat->cols + ( i - 1 )];
				b = mat->data[( j - 1 ) * mat->cols + i];
				c = mat->data[( j - 1 ) * mat->cols + ( i - 1 )];
			}
			//std::cout << "a=" << a << ",b=" << b << "c=," << c;

			p = paeth_predictor( a, b, c );
			//std::cout << ",val = " << val << std::endl;

			uint8_t pre_val = ( uint8_t )( val - p );
			if( encode_for_geometric_distribution ) {
				new_matrix[j * mat->cols + i] = get_new_value_for_geometric_distribution( pre_val );
			} else {
				new_matrix[j * mat->cols + i] = pre_val;
			}
		}
	}

	memcpy( mat->data, new_matrix, sizeof(uint8_t) * mat->rows * mat->cols );

	free( static_cast< void * >( new_matrix ) );
}

void reverse_paeth_filter( struct matrix *mat, bool revert_for_geometric_distribution )
{
	int a;
	int b;
	int c;

	int i, j;
	int x_start = 0;
	int y_start = 0;
	int x_end = mat->cols;
	int y_end = mat->rows;

	for( j = y_start; j < y_end; j++ ) {
		for( i = x_start; i < x_end; i++ ) {
			int val;
			if( revert_for_geometric_distribution ) {
				val = reverse_geometric_distribution_altering( mat->data[j * mat->cols + i] );
			} else {
				val = ( uint8_t )mat->data[j * mat->cols + i];
			}
			uint8_t p;

			//  c  b  d
			//  a  x
			if( j == y_start && i == x_start ) {
				mat->data[j * mat->cols + i] = val;
				continue;
			} else if( j == y_start ) {
				c = b = 0;
				a = mat->data[j * mat->cols + ( i - 1 )];
			} else if( i == x_start ) {
				c = a = 0;
				b = mat->data[( j - 1 ) * mat->cols + i];
			} else {
				a = mat->data[j * mat->cols + ( i - 1 )];
				b = mat->data[( j - 1 ) * mat->cols + i];
				c = mat->data[( j - 1 ) * mat->cols + ( i - 1 )];
			}
			//std::cout << "a=" << a << ",b=" << b << "c=," << c;

			p = paeth_predictor( a, b, c );
			//std::cout << ",val = " << val << std::endl;

			mat->data[j * mat->cols + i] = ( uint8_t )( val + p );
		}
	}
}

void pre_process( compression_filter pre_filter, struct matrix *mat, bool encode_for_geometric_distribution )
{
	if( pre_filter == FILTER_NONE ) {
		return;
	} else if( pre_filter == FILTER_SUBTRACT_LEFT ) {
		subtract_left( mat, encode_for_geometric_distribution );
	} else if( pre_filter == FILTER_MEAN ) {
		a_b_average_filter( mat, encode_for_geometric_distribution );
	} else if( pre_filter == FILTER_PAETH ) {
		paeth_filter( mat, encode_for_geometric_distribution );
	}
}

void revert_pre_process( compression_filter pre_filter,
		struct matrix *mat,
		bool revert_for_geometric_distribution )
{
	if( pre_filter == FILTER_NONE ) {
		return;
	} else if( pre_filter == FILTER_SUBTRACT_LEFT ) {
		reverse_subtract_left( mat, revert_for_geometric_distribution );
	} else if( pre_filter == FILTER_MEAN ) {
		reverse_a_b_average_filter( mat, revert_for_geometric_distribution );
	} else if( pre_filter == FILTER_PAETH ) {
		reverse_paeth_filter( mat, revert_for_geometric_distribution );
	}
}


void copy_original_matrix_contents( uint8_t *dest_matrix,
		uint8_t *src_matrix,
		unsigned int rows,
		unsigned int cols )
{
	memset( dest_matrix, 0, rows * cols * sizeof(uint8_t) );
	memcpy( dest_matrix, src_matrix, rows * cols * sizeof(uint8_t) );
}

void copy_array_into_opencv_matrix( cv::Mat &mat, struct matrix *matrix )
{

	unsigned int i;
	unsigned int j;

	std::cout << "rows = " << matrix->rows << std::endl;
	std::cout << "cols = " << matrix->cols << std::endl;

	if( mat.isContinuous( ) ) {
		std::cout << __func__ << " continuous " << std::endl;
		size_t datalen = matrix->rows * matrix->cols * mat.elemSize( );
		memcpy( mat.data, ( uint8_t * )matrix->data, datalen );
	} else {
		std::cout << __func__ << " non continuous " << std::endl;

		for( i = 0; i < matrix->rows; i++ ) {
			uint8_t *p = mat.ptr< uint8_t >( i );
			for( j = 0; j < matrix->cols; j++ ) {
				p[j] = matrix->data[i * matrix->rows + j];
			}
		}
	}
}

void copy_opencv_matrix_into_array( struct matrix *matrix, cv::Mat &input )
{
	int i;
	int j;

	std::cout << "rows = " << input.rows << std::endl;
	std::cout << "cols = " << input.cols << std::endl;

	for( i = 0; i < input.rows; i++ ) {
		uint8_t *p = input.ptr< uint8_t >( i );
		for( j = 0; j < input.cols; j++ ) {
			matrix->data[i * input.rows + j] = p[j];
		}
	}
}

typedef enum test_compressor {
	TEST_COMPRESSOR_ZLIB,
	TEST_COMPRESSOR_GZIP,
	TEST_COMPRESSOR_BZIP2,
} test_compressor;

class test_compression_algorithm: public compression_algorithm {
public:

	test_compression_algorithm( cv::Mat in_mat, test_compressor compressor, compression_filter pre_filter ) :
			compression_algorithm( in_mat, pre_filter ), compressor( compressor )
	{
	}

	~test_compression_algorithm( )
	{
	}

	std::string write_to_file( uint8_t *matrix, unsigned int rows, unsigned int cols )
	{
		int lastindex = filename.find_last_of(".");
		std::string rawname = filename.substr(0, lastindex);
		std::ofstream stream;
		stream.open( rawname.c_str( ), std::ofstream::out | std::ofstream::trunc );

		unsigned int i, j;
		for( j = 0; j < rows; j++ ) {
			for( i = 0; i < cols; i++ ) {

				uint8_t val = matrix[j * cols + i];

				stream << ( uint8_t )val;
			}
		}
		stream.close( );
		return rawname;
	}


	std::string compress( )
	{
		if( !filename.empty( ) ) {
			image.pre_process_filter( );

			std::string rawname = write_to_file( image.mat.data, image.mat.rows, image.mat.cols );

			std::ofstream zdatfile( filename, std::ios::binary );
			boost::iostreams::filtering_ostreambuf zdat;

			if( compressor == TEST_COMPRESSOR_ZLIB ) {
				boost::iostreams::zlib_params params;
				params.level = boost::iostreams::zlib::best_compression;
				zdat.push( boost::iostreams::zlib_compressor( params ) );
			} else if( compressor == TEST_COMPRESSOR_GZIP ) {
				boost::iostreams::gzip_params params;
				params.level = boost::iostreams::gzip::best_compression;
				zdat.push( boost::iostreams::gzip_compressor( params ) );
				//char buf[200];
				//snprintf( buf, sizeof(buf), "gzip -f -9 %s", rawname.c_str( ) );
				//system( buf );
			} else if( compressor == TEST_COMPRESSOR_BZIP2 ) {
				// bzip2_params already has 9 as block size
				zdat.push( boost::iostreams::bzip2_compressor( ) );
				//char buf[200];
				//snprintf( buf, sizeof(buf), "bzip2 -f -z -9 %s", rawname.c_str( ) );
				//system( buf );
			} else {
				abort( );
			}
			zdat.push( zdatfile );
			size_t datalen = image.mat.rows * image.mat.cols * sizeof(uint8_t);
			boost::iostreams::write( zdat, ( const char * )image.mat.data, datalen );

		}
		return "";
	}

	void decompress( )
	{

	}

private:
	test_compressor compressor;
};

static bool is_memory_equal( uint8_t *left, uint8_t *right, size_t size )
{
	if( memcmp( left, right, size ) != 0 ) {
		std::cerr << "images does not match" << std::endl;
		int mis = 0;
		for( unsigned int z = 0; z < size; z++ ) {
			if( left[z] != right[z] ) {
				std::cout << "mismatch at " << z << std::endl;
				mis++;
			}
		}
		std::cerr << "# of mismatches = " << mis << std::endl;
		return false;
	}
	std::cout << "images match" << std::endl;
	return true;
}

void print_usage( const char *filename )
{
	std::cout << filename << " [-i input image] [-o output] [-g force grayscale] [-f filter]"
			" [-b block-filter] [-s] [-n do not demonstrate algorithm #]" << std::endl;
}

std::vector< std::string > &split( const std::string &s, char delim, std::vector< std::string > &elems )
{
	std::stringstream ss( s );
	std::string item;
	while( std::getline( ss, item, delim ) ) {
		if( !item.empty( ) )
			elems.push_back( item );
	}
	return elems;
}

std::vector< std::string > split( const std::string &s, char delim )
{
	std::vector< std::string > elems;
	split( s, delim, elems );
	return elems;
}

int main( int argc, char **argv )
{
	unsigned int i, j;
	double ran;
	std::string in_filename;
	std::string out_filename;
	bool output_to_file = false;
	bool force_greyscale = true;
	compression_filter pre_filter = FILTER_NONE;
	block_filtering_method block_filtering = NO_BLOCK_FILTERING;
	int scale = 0;
	int output_fd = -1;
	bool separate_dict_compression = false;
	entropy_coding coding = HUFFMAN;
	compression_algorithm *algo;
	compression_algorithm_under_test *aut;
	bool test_comparison = false;
	bool test_entropy = false;
	bool display = true;
	unsigned int k = 0;
	unsigned int algorithm = 5;
	enum agglomerative_clustering::linkage linkage = agglomerative_clustering::LINKAGE_WARD;
	dissimilarity_measure dmeasure = DISSIMILARITY_JENSEN_SHANNON;

	std::vector< cv::Mat > input;

	const struct option longopts[] = {	{ "input", required_argument, NULL, 'i' },
										{ "output", required_argument, NULL, 'o' },
										{ "force-greyscale", no_argument, NULL, 'g' },
										{ "filter", required_argument, NULL, 'f' },
										{ "block-filtering", required_argument, NULL, 'b' },
										{ "do-not-display", no_argument, NULL, 'p' },
										{ "separate-dict-comp", no_argument, NULL, 'd' },
										{ "nclusters", required_argument, NULL, 'k' },
										{ "scattered-algorithm", required_argument, NULL, 'a' },
										{ "entropy-coding", required_argument, NULL, 'e' },
										{ "no-algorithm", required_argument, NULL, 'n' },
										{ "test-comparison", no_argument, NULL, 't' },
										{ "test-entropy", no_argument, NULL, 'u' },
										{ "dissimilarity-measure", required_argument, NULL, 'm' },
										{ "help", no_argument, NULL, 'h' },
										{ 0, 0, NULL, 0 } };

	int index;
	int iarg = 0;

	//turn off getopt error message
	opterr = 1;

	while( iarg != -1 ) {
		iarg = getopt_long( argc, argv, "i:o:gc:e:hb:s:f:dtupk:a:l:m:", longopts, &index );

		switch( iarg ) {
			case 'i':
				std::cout << "input: " << optarg << std::endl;
				in_filename = optarg;
				break;

			case 'h':
				print_usage( argv[0] );
				return 0;

			case 'a':
				algorithm = atoi( optarg );
				std::cout << "Algorithm " << algorithm << std::endl;
				break;

			case 'k':
				k = atoi( optarg );
				std::cout << "Number of clusters k " << k << std::endl;
				break;

			case 'e':
				std::cout << "Entropy coding" << std::endl;
				coding = static_cast< entropy_coding >( atoi( optarg ) );
				break;

			case 'g':
				std::cout << "Force greyscale" << std::endl;
				force_greyscale = true;
				break;

			case 'b':
				std::cout << "Use block filtering" << std::endl;
				block_filtering = static_cast< block_filtering_method >( atoi( optarg ) );
				break;

			case 'f':
				std::cout << "Do pre-compression filtering" << std::endl;
				pre_filter = static_cast< compression_filter >( atoi( optarg ) );
				break;

			case 'o':
				std::cout << "output: " << optarg << std::endl;
				out_filename = optarg;
				output_to_file = true;
				break;

			case 's':
				std::cout << "scale: " << optarg << std::endl;
				scale = atoi( optarg );
				break;

			case 'd':
				std::cout << "separate dictionary compression" << std::endl;
				separate_dict_compression = true;
				break;

			case 't':
				test_comparison = true;
				break;

			case 'u':
				test_entropy = true;
				break;

			case 'p':
				display = false;
				break;

			case 'l':
				linkage = static_cast< enum agglomerative_clustering::linkage >( atoi( optarg ) );
				std::cout << "linkage " << linkage << std::endl;
				break;

			case 'm':
				std::cout << "dissimilarity measure " << optarg << std::endl;
				dmeasure = static_cast< dissimilarity_measure >( atoi( optarg ) );
				break;
		}
	}

	srand( 123 );

	std::vector< std::string > in_files;
	if( !in_filename.empty( ) ) {
		in_files = split( in_filename, ',' );
		std::cout << "input files" << std::endl;
		for( auto f : in_files ) {
			std::cout << f << std::endl;
			if( !boost::filesystem::exists( f ) ) {
				std::cout << f << " does not exist, aborting ..." << std::endl;
				abort( );
			}
			if( force_greyscale ) {
				input.push_back( cv::imread( f, CV_LOAD_IMAGE_GRAYSCALE ) );
			} else {
				input.push_back( cv::imread( f, CV_LOAD_IMAGE_COLOR ) );
			}
		}
	} else {
		/* Populate the image. First rectangle 40x20 has pixels ranging from 50 to 52.
		 * Second rectangle 40x20 has pixels ranging from 70 to 72.
		 */

		input.push_back( cv::Mat::zeros( ROW_NUM, COL_NUM, CV_8UC1 ) );

		for( j = 0; j < ROW_NUM; j++ ) {

			for( i = 0; i < COL_NUM; i++ ) {
				if( i < ( COL_NUM / 2 ) ) {
					ran = ( ( double )rand( ) / ( ( double )RAND_MAX + ( double )MIN1 ) )
							* ( MAX1 - MIN1 + 1 );
					input[0].at< uchar >( j, i ) = ( uchar )ran + MIN1;
				} else {
					ran = ( ( double )rand( ) / ( ( double )RAND_MAX + ( double )MIN2 ) )
							* ( MAX2 - MIN2 + 1 );
					input[0].at< uchar >( j, i ) = ( uchar )ran + MIN2;
				}
			}
		}

		std::cout << " Generated matrix = " << input[0] << std::endl;

		cv::namedWindow( "Display window", CV_WINDOW_AUTOSIZE );
		cv::imshow( "Display window", input[0] );

		cv::waitKey( 0 );
	}

	if( output_to_file ) {
		output_fd = open( out_filename.c_str( ), O_WRONLY | O_CREAT );
		table_total.set_output_file( output_fd );
	}

	if( algorithm < 7 ) {
		if( input.size( ) == 1 ) {

			aut = new compression_algorithm_scattered( input[0], block_filtering, scale,
					separate_dict_compression, k, pre_filter, coding, algorithm, linkage, dmeasure );
			algo = aut;
			if( !in_filename.empty( ) )
				algo->set_filename( in_filename );
		} else if( input.size( ) > 1 ) {
			aut = new ca_multi_scattered( input, block_filtering, scale, separate_dict_compression, k, pre_filter,
					coding, algorithm, linkage, dmeasure );
			algo = aut;
		}

	} else {
		if( in_files.size( ) > 1 )
			abort( );
		aut = new compression_algorithm_adjacent( input[0], block_filtering, scale,
				separate_dict_compression, pre_filter, coding );
		algo = aut;
		if( !in_filename.empty( ) )
			algo->set_filename( in_filename );
	}

	std::string compressed_output = algo->compress( );

	if( display && in_files.size( ) == 1 )
		aut->display_image_with_cbs( );

	/*decompression_algorithm_under_test dealgo( compressed_output );
	uint8_t *decomp = dealgo.decompress( );
	dealgo.compare_cbs( aut->get_compression_blocks( ) );
	is_memory_equal( input[0].data, decomp, input[0].rows * input[0].cols );*/

	std::cout << "elemSize = " << input[0].elemSize( ) << std::endl;
	std::cout << "elemSize1 = " << input[0].elemSize1( ) << std::endl;

	// Output
	size_t datalen = input[0].rows * input[0].cols * input[0].elemSize( );
	std::cout << "data length = " << datalen << std::endl;

	if( test_comparison ) {
		test_compression_algorithm zlib( input[0], TEST_COMPRESSOR_ZLIB, pre_filter );
		test_compression_algorithm gzip( input[0], TEST_COMPRESSOR_GZIP, pre_filter );
		test_compression_algorithm bzip2( input[0], TEST_COMPRESSOR_BZIP2, pre_filter );
		zlib.set_filename( "comp.Z" );
		gzip.set_filename( "comp.gz" );
		bzip2.set_filename( "comp.bz2" );

		zlib.compress( );
		std::cout << "length " << zlib.get_filename( ) << " = "
				<< boost::filesystem::file_size( zlib.get_filename( ) ) << std::endl;
		gzip.compress( );
		std::cout << "length " << gzip.get_filename( ) << " = "
				<< boost::filesystem::file_size( gzip.get_filename( ) ) << std::endl;
		bzip2.compress( );
		std::cout << "length " << bzip2.get_filename( ) << " = "
				<< boost::filesystem::file_size( bzip2.get_filename( ) ) << std::endl;
	}

	if( input.size( ) == 1 && test_entropy ) {
		struct matrix image( input[0].rows, input[0].cols );

		copy_opencv_matrix_into_array( &image, input[0] );

//#if 0
		compute_entropies( &image );

		//compress( &image );

		double kl1, kl2;
		double jsd;

		table_total.print_histogram( "hist_orig" );
		std::cout << "hist_orig E[X] " << table_total.get_expected_value( ) << std::endl;

#if 0
		/* kl1 and kl2 are the KL-Divergence for the two histograms. They should differ since
		 * KL-Distance is not symmetric.
		 */
		kl1 = table_first.calculate_kullback_leibler_distance( table_second );
		kl2 = table_second.calculate_kullback_leibler_distance( table_first );
		std::cout << "D( p||q ) = " << kl1 << ", D( q||p ) = " << kl2 << std::endl;

		/* Symmetric, no need calculate the JSD the other way around */
		jsd = table_first.calculate_jensen_shannon_divergence( table_second );
		std::cout << "JSD( p||q ) = " << jsd << std::endl;

		pmf t1;
		double sum = 0.0;
		unsigned int count[8] = {62124, 47279, 28485, 15880, 9538, 6618, 4942, 4072};
		for( int i = 0; i < 8; i++ )
		sum += count[i];
		for( int i = 0; i < 8; i++ ) {
			double probability = count[i] / ( double )sum;
			t1.etable[i] = probability_record( count[i], probability );
			t1.entropy += t1.etable[i].get_information( );
		}

		sum = 0;

		pmf t2;
		int count2[8] = {47279, 28485, 15880, 9538, 6618, 4942, 4072, 3384};
		for( int i = 0; i < 8; i++ )
		sum += count2[i];
		for( int i = 0; i < 8; i++ ) {
			double probability = count2[i] / ( double )sum;
			t2.etable[i+1] = probability_record( count2[i], probability );
			t2.entropy += t2.etable[i+1].get_information( );
		}

		std::cout << "t1 " << t1 << std::endl;
		std::cout << "t2 " << t2 << std::endl;

		kl1 = t1.calculate_kullback_leibler_distance( t2 );
		kl2 = t2.calculate_kullback_leibler_distance( t1 );
		std::cout << "D( p||q ) = " << kl1 << ", D( q||p ) = " << kl2 << std::endl;

		std::vector< pmf *> distribution_vector;
		distribution_vector.push_back( &t1 );
		distribution_vector.push_back( &t2 );
		/* Symmetric, no need calculate the JSD the other way around */
		jsd = pmf::calculate_jensen_shannon_divergence( distribution_vector );
		std::cout << "JSD( p||q ) = " << jsd << std::endl;
		abort( );

		/*
		compute_kadir_brady_sparse_matrix( &image );
		compute_minimum_kadir_brady_sparse_matrix( &image );
		*/
#endif
		const size_t pixels = image.rows * image.cols;

		pre_process( pre_filter, &image, true );

		rectangle rect;
		rect.point.x = 0;
		rect.point.y = 0;
		rect.w = image.cols;
		rect.h = image.rows;

		pmf ts;
		ts.compute_pmf( &image, rect, false );
		ts.print_histogram( "hist_subt" );
		std::cout << "hist_subt E[X] " << ts.get_expected_value( ) << std::endl;
		ts.write_values_to_file( &image, rect, "filtered_file" );
		std::cout << "hist_mod E[X] " << ts.get_expected_value( ) << std::endl;

		int min_k;

		struct matrix new_image( image.rows, image.cols );
		std::string compressed_output;

		switch( coding ) {
			case GOLOMB_RICE: {
				min_k = ts.calculate_min_k_golomb_rice( );
				std::cout << "minimum K for GOLOMB-RICE is " << min_k << std::endl;
				unsigned int min_M = pow( 2, min_k );
				compressed_output = ts.compress_golomb_rice( &image, min_M, rect );
				print_length_with_bpp( "GOLOMB-RICE compressed_output length ", compressed_output.length( ),
						pixels );
				assert( compressed_output.length( ) == ts.get_num_of_golomb_rice_compressed_bits( min_M ) );

				ts.decompress_golomb_rice( &new_image, compressed_output, 0, min_k, rect );
			}
				break;

			case EXP_GOLOMB: {
				min_k = ts.calculate_min_k_exponential_golomb( );
				std::cout << "minimum K for EXP-GOLOMB is " << min_k << std::endl;
				compressed_output = ts.compress_exponential_golomb( &image, min_k, rect );
				print_length_with_bpp( "EXP-GOLOMB compressed_output length ", compressed_output.length( ),
						pixels );

				ts.decompress_exponential_golomb( &new_image, compressed_output, 0, min_k, rect );
			}
				break;

			case HUFFMAN:
				compressed_output = ts.compress_huffman( &image, rect );
				print_length_with_bpp( "HUFFMAN compressed_output length ", compressed_output.length( ),
						pixels );
				ts.decompress_huffman( &new_image, compressed_output, 0, rect );
				break;

			default:
				abort( );
				break;
		}
		is_memory_equal( image.data, new_image.data, pixels );

		revert_pre_process( pre_filter, &new_image, true );
		assert( input[0].isContinuous( ) );
		is_memory_equal( input[0].data, new_image.data, pixels );
	}

	if( output_to_file ) {
		close( output_fd );
	}

	return 0;
}
