/*
 * MIT License
 *
 * Copyright (c) 2013 Sameh Samir Hassan
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef SQUARE_BLOCK_H_
#define SQUARE_BLOCK_H_

#include <stdint.h>

#include "pos_2d.h"

struct pos_2d;
class pmf;

class square_block: public pmf {
public:
	square_block( rectangle &shape, struct matrix *matrix, bool combine = false );
	square_block( rectangle &shape );

	int calculate_mean( );
	void recalculate_pmf( );

	void subtract_mean( );
	void subtract_mode( );
	void subtract_median( );

	void subtract_diff( );

	void encode_bwt( );

	void verify( unsigned int *verification_matrix );

#ifdef ENABLE_SQUARE_NEIGHBOUR
	void add_neighbour( square_block *square );
#endif

	rectangle &get_shape( );
	struct matrix *get_matrix( );

	void set_combined( bool combine );
	bool get_combined( ) const;

	void set_container( void *ptr );
	void *get_container( );

	void set_order( unsigned int o )
	{
		order = o;
	}
	unsigned int get_order( )
	{
		return order;
	}

	static inline bool closer_to_origin( const square_block *l, const square_block *r )
	{
		if( l->shape.point.y < r->shape.point.y )
			return true;
		else if( l->shape.point.y == r->shape.point.y && l->shape.point.x < r->shape.point.x )
			return true;
		return false;
	}

	struct entropy_neighbour {
		square_block *neighbour;
		double jsd;
	};

private:
	bool is_combined;
	rectangle shape;
	struct matrix *matrix;

#ifdef ENABLE_SQUARE_NEIGHBOUR
	std::vector< entropy_neighbour > neighbours;
#endif

	unsigned int order;

	void *container; // pointer to compression_block
};

#endif /* ENTROPY_SQUARE_H_ */
