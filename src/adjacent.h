/*
 * MIT License
 *
 * Copyright (c) 2014 Sameh Samir Hassan
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef ADJACENT_H_
#define ADJACENT_H_

class compression_algorithm_adjacent: public compression_algorithm_under_test {
public:
	compression_algorithm_adjacent( cv::Mat in_mat,
			block_filtering_method bfiltering_method,
			int fixed_scale,
			bool separate_dict_compression,
			compression_filter pre_filter,
			entropy_coding coding ) :
			compression_algorithm_under_test( in_mat, bfiltering_method, fixed_scale,
					separate_dict_compression, pre_filter, coding, compression_algorithm_under_test::AUT_ADJACENT )
	{

	}
	~compression_algorithm_adjacent( )
	{
	}

	void cluster( );
	void cluster_squares_raster( );

private:
	void add_cb_to_list_if_unique( std::list< compression_block * > &cb_list, compression_block *cb );

	void attempt_square_combining( compression_block *cb,
			struct matrix *matrix,
			unsigned int i,
			unsigned int j );
};

#endif /* ADJACENT_H_ */
