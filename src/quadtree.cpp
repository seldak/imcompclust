/*
 * MIT License
 *
 * Copyright (c) 2013 Sameh Samir Hassan
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <algorithm>
#include <assert.h>
#include <iostream>
#include <list>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "enums.h"
#include "pos_2d.h"
#include "pmf.h"
#include "compression_block.h"
#include "quadtree.h"

quadtree *quadtree::copy_node( quadtree *node )
{
	quadtree *new_node = new quadtree( node->pos, node->span );

	new_node->is_leaf = node->is_leaf;

	new_node->cb = node->cb;

	if( !node->is_leaf ) {
		assert( node->cb == NULL );
		if( node->nw ) {
			new_node->nw = copy_node( node->nw );
			new_node->nw->parent = new_node;
		}
		if( node->ne ) {
			new_node->ne = copy_node( node->ne );
			new_node->ne->parent = new_node;
		}
		if( node->sw ) {
			new_node->sw = copy_node( node->sw );
			new_node->sw->parent = new_node;
		}
		if( node->se ) {
			new_node->se = copy_node( node->se );
			new_node->se->parent = new_node;
		}
	}

	return new_node;
}

unsigned int quadtree::get_max_quadtree_span( unsigned int scale, unsigned int rows, unsigned cols )
{
	unsigned int max_span = scale;
	unsigned int max_pixels = std::max( rows, cols );
	while( max_span < max_pixels ) {
		max_span *= 2;
	}
	std::cout << "max span for scale " << scale << " and rows = " << rows << ", cols = " << cols << " equals "
			<< max_span << std::endl;

	return max_span;
}

void quadtree::initialize_tree_down_to_scale( std::vector< std::vector< square_block * > > &best_es_grid,
		const unsigned int scale,
		const unsigned int rows,
		const unsigned int cols )
{
	assert( span >= scale );

	if( span == scale ) {
		unsigned int y = pos.y / scale;
		unsigned int x = pos.x / scale;
		cb = static_cast< compression_block * >( best_es_grid[y][x]->get_container( ) );
		assert( cb != NULL );
		is_leaf = true;
		return;
	}

	struct pos_2d position;

	position.y = pos.y;
	position.x = pos.x;
	unsigned int new_span = span / 2;

	if( position.x < cols && position.y < rows ) {
		nw = new quadtree( position, new_span );
		nw->parent = this;
		nw->initialize_tree_down_to_scale( best_es_grid, scale, rows, cols );
	}

	position.x += new_span;
	if( position.x < cols && position.y < rows ) {
		ne = new quadtree( position, new_span );
		ne->parent = this;
		ne->initialize_tree_down_to_scale( best_es_grid, scale, rows, cols );
	}

	position.y += new_span;
	if( position.x < cols && position.y < rows ) {
		se = new quadtree( position, new_span );
		se->parent = this;
		se->initialize_tree_down_to_scale( best_es_grid, scale, rows, cols );
	}

	position.x -= new_span;
	if( position.x < cols && position.y < rows ) {
		sw = new quadtree( position, new_span );
		sw->parent = this;
		sw->initialize_tree_down_to_scale( best_es_grid, scale, rows, cols );
	}
}

bool quadtree::divergence_check( quadtree *tree )
{
	return ( tree->check_divergence_of_children( ) < JSD_THRESHOLD );
}

bool quadtree::scale_check( quadtree *tree )
{
	assert( tree->nw != NULL && tree->nw->cb != NULL );
	size_t num = tree->nw->cb->get_number_of_blocks( );
	bool check = true;
	if( tree->ne ) {
		assert( tree->ne->cb != NULL );
		if( tree->ne->cb->get_number_of_blocks( ) != num )
			check = false;
	}
	if( tree->sw ) {
		assert( tree->sw->cb != NULL );
		if( tree->sw->cb->get_number_of_blocks( ) != num )
			check = false;
	}
	if( tree->se && tree->se->cb ) {
		if( tree->se->cb->get_number_of_blocks( ) != num )
			check = false;
	}
	return check;
}

bool quadtree::k_check( quadtree *tree )
{
	assert( tree->nw != NULL && tree->nw->cb != NULL );
	size_t num = tree->nw->cb->get_k( );
	bool check = true;
	if( tree->ne ) {
		assert( tree->ne->cb != NULL );
		if( tree->ne->cb->get_k( ) != num )
			check = false;
	}
	if( tree->sw ) {
		assert( tree->sw->cb != NULL );
		if( tree->sw->cb->get_k( ) != num )
			check = false;
	}
	if( tree->se && tree->se->cb ) {
		if( tree->se->cb->get_k( ) != num )
			check = false;
	}
	return check;
}

bool quadtree::cb_check( quadtree *tree )
{
	assert( tree->nw != NULL && tree->nw->cb != NULL );
	compression_block *cb = tree->nw->cb;
	bool check = true;
	if( tree->ne ) {
		assert( tree->ne->cb != NULL );
		if( tree->ne->cb != cb )
			check = false;
	}
	if( tree->sw ) {
		assert( tree->sw->cb != NULL );
		if( tree->sw->cb != cb )
			check = false;
	}
	if( tree->se && tree->se->cb ) {
		if( tree->se->cb != cb )
			check = false;
	}
	return check;
}

void quadtree::attempt_node_combining( quadtree *tree,
		bool (*combination_check)( quadtree * ),
		void (*combination_action)( quadtree * ) )
{
	if( !tree )
		return;
	if( tree->is_leaf ) {
		assert( tree->nw == NULL );
		assert( tree->ne == NULL );
		assert( tree->sw == NULL );
		assert( tree->se == NULL );
		return;
	}

	attempt_node_combining( tree->nw, combination_check, combination_action );
	attempt_node_combining( tree->ne, combination_check, combination_action );
	attempt_node_combining( tree->sw, combination_check, combination_action );
	attempt_node_combining( tree->se, combination_check, combination_action );

	bool do_divergence_check = true;

	if( tree->nw ) {
		if( !tree->nw->is_leaf )
			do_divergence_check = false;
	}

	if( tree->ne ){
		if( !tree->ne->is_leaf ){
			do_divergence_check = false;
		}
	}
	if( tree->sw ) {
		if( !tree->sw->is_leaf )
			do_divergence_check = false;
	}

	if( tree->se ){
		if( !tree->se->is_leaf ){
			do_divergence_check = false;
		}
	}

	unsigned int children = tree->get_number_of_children( );

	assert( children != 0 );

	if( children == 1 ) {
		if( tree->nw ) {
			if( tree->nw->is_leaf ) {
				assert( tree->nw->cb != NULL );
				combination_action( tree );
			}
		}
	} else if( do_divergence_check ) {
		if( combination_check( tree ) ) {
			combination_action( tree );
		}
	}
}

unsigned int quadtree::get_number_of_children( )
{
	unsigned int children = 0;
	if( nw )
		children ++;
	if( ne )
		children ++;
	if( sw )
		children ++;
	if( se )
		children ++;
	return children;
}

double quadtree::check_divergence_of_children( )
{
	std::vector< pmf * > vec;

	if( nw && nw->cb ) vec.push_back( nw->cb );
	if( ne && ne->cb ) vec.push_back( ne->cb );
	if( sw && sw->cb ) vec.push_back( sw->cb );
	if( se && sw->cb ) vec.push_back( se->cb );
	return pmf::calculate_normalized_jensen_shannon_divergence( vec );
}

void quadtree::combine_nodes( quadtree *node )
{
	assert( node->nw != NULL );
	assert( node->nw->cb != NULL );
	node->cb = node->nw->cb;
	node->nw->cb = NULL;
	delete node->nw;
	node->nw = NULL;

	if( node->ne ) {
		delete node->ne;
		node->ne = NULL;
	}

	if( node->sw ) {
		delete node->sw;
		node->sw = NULL;
	}

	if( node->se ) {
		delete node->se;
		node->se = NULL;
	}

	node->is_leaf = true;
}


void quadtree::combine_cbs( quadtree *node )
{
	assert( node->nw != NULL );
	//assert( node->nw->cb != NULL );

	if( node->nw )
	node->cb = node->nw->cb;
	node->nw->cb = NULL;
	delete node->nw;
	node->nw = NULL;

	if( node->nw ) {
		node->cb->add_cb( *( node->nw->cb ) );
		node->nw->delete_cb( );
		delete node->nw;
		node->nw = NULL;
	}

	if( node->ne ) {
		node->cb->add_cb( *( node->ne->cb ) );
		node->ne->delete_cb( );
		delete node->ne;
		node->ne = NULL;
	}

	if( node->sw ) {
		node->cb->add_cb( *( node->sw->cb ) );
		node->sw->delete_cb( );
		delete node->sw;
		node->sw = NULL;
	}

	if( node->se ) {
		node->cb->add_cb( *( node->se->cb ) );
		node->se->delete_cb( );
		delete node->se;
		node->se = NULL;
	}

	//assert( node->cb != NULL );
	node->is_leaf = true;
}

void quadtree::delete_cb( )
{
	assert( cb != NULL );
	delete cb;
	cb = NULL;
}

void quadtree::add_leafs_to_list( quadtree const *node, std::list<compression_block *> &list )
{
	if( !node )
		return;
	if( node->is_leaf && node->cb ) {
		list.push_back( node->cb );
		return;
	}
	if( node->nw )
		add_leafs_to_list( node->nw, list );
	if( node->ne )
		add_leafs_to_list( node->ne, list );
	if( node->sw )
		add_leafs_to_list( node->sw, list );
	if( node->se )
		add_leafs_to_list( node->se, list );
}

void quadtree::draw_quadtree_node( cv::Mat &mat, quadtree *node )
{
	if( !node )
		return;

	if( !node->is_leaf ) {
		draw_quadtree_node( mat, node->nw );
		draw_quadtree_node( mat, node->ne );
		draw_quadtree_node( mat, node->sw );
		draw_quadtree_node( mat, node->se );
	} else {
		{
			std::cout << " X = " << node->pos.x << " Y = " << node->pos.y << " span" << node->span << std::endl;
			cv::Point start = cv::Point( node->pos.x, node->pos.y );
			cv::Point end = cv::Point( node->pos.x + node->span, node->pos.y );

			cv::line( mat, start, end, cv::Scalar( 0, 0, 255 ), 1, 8 );

			start = end;
			end = cv::Point( node->pos.x + node->span, node->pos.y + node->span );
			cv::line( mat, start, end, cv::Scalar( 0, 0, 255 ), 1, 8 );

			start = end;
			end = cv::Point( node->pos.x, node->pos.y + node->span );
			cv::line( mat, start, end, cv::Scalar( 0, 0, 255 ), 1, 8 );

			start = end;
			end = cv::Point( node->pos.x, node->pos.y );
			cv::line( mat, start, end, cv::Scalar( 0, 0, 255 ), 1, 8 );
		}
	}
}

unsigned int quadtree::count_leafs( )
{
	if( is_leaf )
		return 1;

	unsigned int nodes = 0;
	if( nw )
		nodes += nw->count_leafs( );
	if( ne )
		nodes += ne->count_leafs( );
	if( sw )
		nodes += sw->count_leafs( );
	if( se )
		nodes += se->count_leafs( );

	return nodes;
}

unsigned int quadtree::count_total_nodes( )
{
	if( is_leaf )
		return 1;

	unsigned int nodes = 0;

	if( nw )
		nodes += nw->count_total_nodes( );
	if( ne )
		nodes += ne->count_total_nodes( );
	if( sw )
		nodes += sw->count_total_nodes( );
	if( se )
		nodes += se->count_total_nodes( );
	nodes++;

	return nodes;
}

unsigned int quadtree::count_bits_required_for_quadtree( unsigned int bits_per_leaf )
{
	unsigned int leaf_count = count_leafs( );
	std::cout << "new leafs = " << leaf_count << std::endl;
	unsigned int total_nodes = count_total_nodes( );
	std::cout << "total nodes =" << total_nodes << std::endl;
	return ( bits_per_leaf + 1 ) * leaf_count + total_nodes;
}

unsigned int quadtree::find_maximum_node_span( unsigned int current_max_span )
{
	unsigned int max_span = 0;
	if( is_leaf ) {
		if( current_max_span < span )
			max_span = span;
	} else {
		max_span = current_max_span;
		if( nw )
			max_span = nw->find_maximum_node_span( max_span );
		if( ne )
			max_span = ne->find_maximum_node_span( max_span );
		if( sw )
			max_span = sw->find_maximum_node_span( max_span );
		if( se )
			max_span = se->find_maximum_node_span( max_span );
	}
	return max_span;
}
