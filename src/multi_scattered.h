/*
 * MIT License
 *
 * Copyright (c) 2015 Sameh Samir Hassan
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef MULTI_SCATTERED_H_
#define MULTI_SCATTERED_H_

#include <vector>
#include <boost/shared_ptr.hpp>
#include <opencv2/core/core.hpp>

enum __dissimilarity_measure;

class ca_multi_scattered: public compression_algorithm_under_test {
public:

	ca_multi_scattered( std::vector< cv::Mat > &in_mats,
			block_filtering_method bfiltering_method,
			int fixed_scale,
			bool separate_dict_compression,
			unsigned int k,
			compression_filter pre_filter,
			entropy_coding coding,
			unsigned int algorithm,
			enum agglomerative_clustering::linkage linkage,
			enum __dissimilarity_measure dmeasure ) :
			compression_algorithm_under_test( in_mats[0], bfiltering_method, fixed_scale,
					separate_dict_compression, pre_filter, coding,
					compression_algorithm_under_test::AUT_SCATTERED )

	{
		for( auto m : in_mats ) {
			boost::shared_ptr< image_data > mat( new image_data( m, pre_filter ) );
			mats.push_back( mat );
			std::cout << "adding image" << std::endl;
		}

		switch( algorithm ) {
			case 0:
				algo = new cluster_for_golomb_codes( compression_blocks, coding );
				break;
			case 1:
				algo = new cluster_using_kl_and_jsd( compression_blocks );
				break;
			case 2:
				algo = new cluster_using_g_center_and_jsd( compression_blocks );
				break;
			case 3:
				algo = new modified_cluster_cbs( compression_blocks, dmeasure );
				break;
			case 4:
				/* K was usually 6 here*/
				algo = new k_medoids_clustering( compression_blocks, dmeasure, k );
				break;
			case 5:
				algo = new agglomerative_clustering( compression_blocks, linkage, dmeasure, k );
				break;
			case 6:
				algo = new divisive_clustering( compression_blocks, dmeasure );
				break;
			default:
				abort( );
		}
	}

	~ca_multi_scattered( )
	{
		//delete algo;
	}

	void pre_process_filter( );
	void find_best_scale_and_initialize_grid( );
	void initialize_compression_blocks( );
	std::string write_square_to_cb_map( );

	void cluster( )
	{
		algo->do_clustering( );
	}
	void verify( );
	void encode( );
	void print_length_of_compressed_output( )
	{
		size_t total_pixels = 0;
		for( auto image : mats ) {
			total_pixels += image->mat.rows * image->mat.cols;
		}
		std::cout << "total_pixels " << total_pixels << std::endl;
		print_length_with_bpp( "total length of output = ", compressed_length, total_pixels );
		print_length_with_bpp( "real length of output = ", compressed_output.length( ), total_pixels );
	}

private:

	std::vector< boost::shared_ptr< image_data > > mats;
	clustering_algorithm_scattered *algo;
};

#endif /* MULTI_SCATTERED_H_ */
