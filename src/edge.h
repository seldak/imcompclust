/*
 * MIT License
 *
 * Copyright (c) 2014 Sameh Samir Hassan
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef EDGE_H_
#define EDGE_H_

#include <iostream>
#include <assert.h>

#include "pos_2d.h"

class edge {
public:
	edge( const pos_2d &start, const pos_2d &end )
	{
		this->start = pos_2d( start );
		this->end = pos_2d( end );
	}
	pos_2d start;
	pos_2d end;

	inline bool operator==( const edge& other )
	{
		return ( ( start == other.start ) && ( end == other.end ) );
	}

	inline bool operator<( const edge& other ) const
	{
		if( this->start < other.start )
			return true;
		if( this->end < other.end )
			return true;
		return false;
	}

	edge& operator+=( const edge& other )
	{
		if( this->start != other.end && this->end != other.start ) {
			std::cout << "cannot add edge since there's no common point" << std::endl;
			abort( );
		}
		if( this->start == other.end )
			this->start = other.start;
		else if( this->end == other.start )
			this->end = other.end;
		// make sure the result is a straight line
		assert( ( this->start.x == this->end.x ) || ( this->start.y == this->end.y ) );
		return *this;
	}
};

#endif /* EDGE_H_ */
