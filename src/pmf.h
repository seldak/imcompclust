/*
 * MIT License
 *
 * Copyright (c) 2013 Sameh Samir Hassan
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef ENTROPY_TABLE_H_
#define ENTROPY_TABLE_H_

#include <math.h>
#include <vector>
#include <map>
#include <assert.h>
#include <string.h>

#include "utils.h"

class pos_2d;
struct rectangle;

struct matrix {
	uint8_t *data;
	unsigned int rows;
	unsigned int cols;

	matrix( unsigned int r, unsigned int c ) :
			rows( r ), cols( c )
	{
		size_t size = rows * cols * sizeof(uint8_t);
		assert( posix_memalign( (void **)&data, 4096, size ) == 0 );
		memset( data, 0, size );
	}
	matrix( )
	{
		rows = cols = 0;
		data = NULL;
	}

	~matrix( )
	{
		if( data ) {
			free( static_cast< void * >( data ) );
			data = NULL;
		}
	}
};

class probability_record {
private:
	double probability;
	double information;
	unsigned int count;

public:
	probability_record( unsigned int c = 0, double p = 0.0 )
	{
		count = c;
		assert( likely( p <= 1.0 ) );
		probability = p;
		if( p > 0.0 )
			information = -1.0 * probability * log2( probability );
		else
			information = -0.0;
	}

	inline unsigned int get_count( ) const
	{
		return count;
	}
	inline void set_count( int c )
	{
		count = c;
	}
	inline double get_probability( ) const
	{
		return probability;
	}
	inline void set_probability( const double p )
	{
		assert( likely( p <= 1.0 ) );
		if( likely( p > 0.0 ) ) {
			probability = p;
			information = -p * log2( p );
		} else {
			probability = -0.0;
			information = -0.0;
		}
	}
	inline double get_information( ) const
	{
		return information;
	}

	friend bool operator<( const probability_record &e1, const probability_record &e2 )
	{
		return e1.count > e2.count;
	}
};

template< typename type >
class huffman_node {
	friend class HCTree; // so an HCTree can access huffman_node fields
public:
	huffman_node< type >* parent; // pointer to parent;null if root
	bool is_left;  // true if this is "0" child of its parent
	huffman_node< type >* left; // pointer to "0" child;null if leaf
	huffman_node< type >* right; // pointer to "1" child;null if leaf
	type symb; // symbol
	int count; // count/frequency of symbols in subtree
	bool is_leaf;
	huffman_node( int c, type s, bool is_leaf ) :
			is_left( false ), symb( s ), count( c ), is_leaf( is_leaf )
	{
		parent = left = right = NULL;
	}
	huffman_node( type s, bool is_leaf ) :
			is_left( false ), symb( s ), count( 0 ), is_leaf( is_leaf )
	{
		parent = left = right = NULL;
	}
	huffman_node( bool is_leaf ) :
			parent( NULL ), is_left( false ), left( NULL ), right( NULL ), symb( 0 ), count( 0 ), is_leaf(
					is_leaf )
	{
	}
	// for less-than comparisons between huffman_nodes
	bool operator<( huffman_node< type > const & ) const;
};

#define GOLOMB_M       8
#define GOLOMB_K       3

class pmf {
public:
	pmf( ) :
			entropy( 0 ), total_pixel_count( 0 ), write_to_file( false ), output_fd( -1 ), write_huffman_tree_with_coded_bits(
					true )
	{
		etable.clear( );
	}
	pmf( const pmf &other ) :
			entropy( other.entropy ), total_pixel_count( other.total_pixel_count ), write_to_file(
					other.write_to_file ), output_fd( other.output_fd ), write_huffman_tree_with_coded_bits(
					other.write_huffman_tree_with_coded_bits )
	{
		etable = other.etable;
	}

	void compute_pmf( const struct matrix *mat, const rectangle &rect, bool center );

	void compute_pmf( const std::vector< uint8_t > &vector );

	void set_output_file( int fd )
	{
		write_to_file = true;
		output_fd = fd;
	}

	void reset_output_file( )
	{
		write_to_file = false;
	}

	static double calculate_squared_euclidean_distance( const pmf * const p1, const pmf * const p2 );

	static double calculate_kullback_leibler_distance( const pmf * const p, const pmf * const q );
	bool is_kullback_leibler_distance_infinity( const pmf &other );

	static double calculate_jensen_shannon_divergence( std::vector< pmf * > &distribution_vector );
	static double calculate_jensen_shannon_divergence( const pmf * const p1, const pmf * const p2 );
	static double calculate_normalized_jensen_shannon_divergence( std::vector< pmf * > &distribution_vector );
	static double calculate_single_jensen_shannon_divergence( const pmf * const p1, const pmf * const p2 );

	static double calculate_bhattacharyya_distance( const pmf * const p1, const pmf * const p2 );

	static bool do_blocks_coincide( const pmf &table1, const pmf &table2 );
	static bool do_tables_match( const pmf &table1, const pmf &table2 );
	static bool do_pmfs_exactly_match( const pmf &table1, const pmf &table2 );

	double get_entropy( ) const
	{
		return entropy;
	}

	double calculate_probability_diff( pmf &table2, bool first_time );
	double get_probability( int value )
	{
		return etable[value].get_probability( );
	}
	size_t get_histogram_bins( ) const
	{
		return etable.size( );
	}
	unsigned int get_total_pixel_count( ) const
	{
		return total_pixel_count;
	}

	unsigned int get_number_of_modes( );
	double get_expected_value( ) const;
	int get_median( );
	int get_mode( );
	int get_maximum_value( );

	std::map< uint8_t, std::string > compute_huffman_codes( );

	std::string compress_huffman( const struct matrix *image, rectangle &rect );

	static std::string encode( const struct matrix *image,
			std::map< uint8_t, std::string > &code_map,
			rectangle &rect );

	static std::string compress_huffman( const std::vector< uint8_t > &vector );

	static std::map< uint8_t, std::string > read_huffman_codes( const std::string &codeword, size_t &pos );

	static size_t decompress_huffman( struct matrix *image,
			std::string &codeword,
			size_t pos,
			rectangle &rect );
	size_t decompress_huffman( struct matrix *image,
			std::map< uint8_t, std::string > &code_map,
			std::string &codeword,
			size_t p,
			rectangle &rect );

	std::string compress_golomb_rice( const struct matrix *image, unsigned int M, rectangle &rect );

	static size_t decompress_golomb_rice( struct matrix *image,
			std::string &codeword,
			size_t pos,
			unsigned int K,
			rectangle &rect );
	unsigned int get_num_of_golomb_rice_compressed_bits( unsigned int M );

	std::map< uint8_t, std::string > compute_exponential_golomb_codes( int k );
	std::string compress_exponential_golomb( const struct matrix *m, unsigned int k, rectangle &rect );
	size_t decompress_exponential_golomb( struct matrix *image,
			std::string &codeword,
			size_t p,
			unsigned int K,
			rectangle &rect );
	unsigned int get_num_of_exponential_golomb_compressed_bits( unsigned int k );

	std::string compress_burrows_wheeler_transform( struct matrix *matrix, rectangle &rect );

	void combine_probability_distributions( const pmf &table2 );
	void combine_dummy_probability_distribution( const pmf &table2 );

	std::multimap< probability_record, int > flip_entropy_table( );

	void append_huffman_symbols_to_vector( std::vector< uint8_t > &vector );
	void append_huffman_nodes_to_vector( std::vector< bool > &vector );

	void write_huffman_tree( bool flag )
	{
		write_huffman_tree_with_coded_bits = flag;
	}

	friend std::ostream& operator<<( std::ostream& os, const pmf& etable );
	void write_values_to_file( struct matrix *matrix, rectangle &rect, const char *filename );
	void print_histogram( const char * filename );
	static void print_histogram_from_vector( const char *filename, const std::vector< uint8_t > &vector );

	std::vector< uint8_t > get_huffman_symbol_vector( );

	std::string get_huffman_tree_codeword( )
	{
		return huffman_tree_string;
	}

	static double get_mixture_entropy( std::vector< pmf * > &distribution_vector );

	double get_expected_length( )
	{
		return entropy * total_pixel_count;
	}

	size_t get_num_of_unique_values( )
	{
		return etable.size( );
	}

	unsigned int calculate_min_k_golomb_rice( );
	unsigned int calculate_min_k_exponential_golomb( );

protected:
	std::vector< int > vector;

private:
	static double get_mixture_map( std::vector< pmf * > &distribution_vector,
			std::map< int, probability_record > *mixture );

	static double get_mixture_entropy( pmf const * const p1, pmf const * const p2 );

	std::string huffman_private( const std::vector< uint8_t > &vector );

	void store_huffman_tree_in_separate_vector( huffman_node< uint8_t > *node );

	std::map< uint8_t, std::string > compute_golomb_rice_codes( int M );

	std::map< int, probability_record > etable;
	double entropy;
	unsigned int total_pixel_count;
	bool write_to_file;
	int output_fd;
	bool write_huffman_tree_with_coded_bits;
	std::string huffman_tree_string; // used only for separate dictionary compression
	std::vector< uint8_t > huffman_symbol_vector;
	std::vector< bool > huffman_node_vector;
};

#endif
