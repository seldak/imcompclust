/*
 * MIT License
 *
 * Copyright (c) 2014 Sameh Samir Hassan
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef AUT_H_
#define AUT_H_

#include "enums.h"
#include "algo.h"

typedef struct compression_header {
	uint16_t rows;
	uint16_t cols;
	uint8_t scale;
	uint8_t pre_filter;
	uint8_t coding;
	uint16_t num_of_cbs;
} compression_header;

class clustering_algorithm {
public:
	clustering_algorithm( std::list< compression_block * > &compression_blocks, dissimilarity_measure dmeasure ) :
			compression_blocks( compression_blocks ), dmeasure( dmeasure )
	{
		switch( dmeasure ) {
			case DISSIMILARITY_SQUARED_EUCLIDEAN:
				dissimilarity = pmf::calculate_squared_euclidean_distance;
				are_similar = pmf::do_blocks_coincide;
				break;

			case DISSIMILARITY_JENSEN_SHANNON:
				dissimilarity = pmf::calculate_jensen_shannon_divergence;
				are_similar = pmf::do_pmfs_exactly_match;
				break;

			case DISSIMILARITY_KULLBACK_LEIBLER:
				/* XXX: not symmetric. (Replace proximity matrix
				 * with (D+D^T)/2
				 */
				dissimilarity = pmf::calculate_kullback_leibler_distance;
				are_similar = pmf::do_pmfs_exactly_match;
				break;

			case DISSIMILARITY_BHATTACHARYYA:
				dissimilarity = pmf::calculate_bhattacharyya_distance;
				are_similar = pmf::do_pmfs_exactly_match;
				break;

				/* TODO: add the others */
			default:
				break;
		}
	}
	virtual void do_clustering( ) = 0;
	std::list< compression_block * > &compression_blocks;

	dissimilarity_measure dmeasure;
	double (*dissimilarity)( const pmf * const p1, const pmf * const p2 );
	bool (*are_similar)( const pmf &p1, const pmf &p2 );
};

class compression_algorithm_under_test: public compression_algorithm {
public:
	typedef enum __algorithm_under_test_type {
		AUT_ADJACENT,
		AUT_SCATTERED,
	} algorithm_under_test_type;

	compression_algorithm_under_test( cv::Mat &in_mat,
			block_filtering_method bfiltering_method,
			int fixed_scale,
			bool separate_dict_compression,
			compression_filter pre_filter,
			entropy_coding coding,
			algorithm_under_test_type type ) :
			compression_algorithm( in_mat, pre_filter ), bfiltering_method( bfiltering_method ), separate_dict_compression(
					separate_dict_compression ), best_scale( fixed_scale ), coding( coding ), type( type )
	{
	}

	~compression_algorithm_under_test( )
	{
		for( std::list< compression_block * >::iterator it = compression_blocks.begin( );
				it != compression_blocks.end( ); it++ ) {
			delete *it;
		}
	}

	virtual void pre_process_filter( )
	{
		image.pre_process_filter( );
	}

	virtual void find_best_scale_and_initialize_grid( );

	virtual void initialize_compression_blocks( );

	virtual std::string write_square_to_cb_map( );

	std::string encode_huffman_symbols( std::vector< bool > &huffman_nodes );

	virtual void verify( );
	std::string compress( );

	std::string encode_header( );

	void verify_cb_traversing( );

	void display_image_with_cbs( );

	virtual void cluster( ) = 0;
	virtual void encode( );

	virtual void print_length_of_compressed_output( );

	std::list< compression_block * > get_compression_blocks( )
	{
		return compression_blocks;
	}

private:
	void draw_cbs_on_mat( cv::Mat &mat, compression_block *largest_cb, cv::Scalar &line_col, bool draw_largest_cb );
	void draw_colored_cbs_on_mat( cv::Mat &mat, cv::Scalar &line_col );

protected:
	void create_grid_for_scale( image_data &image,
			std::vector< std::vector< square_block* > > & es_grid,
			const unsigned int s );

	block_filtering_method bfiltering_method;
	bool separate_dict_compression;
	std::list< compression_block * > compression_blocks;

	unsigned int best_scale;
	entropy_coding coding;
	std::string cb_map;
	std::string compressed_output;
	size_t compressed_length;

	algorithm_under_test_type type;
};

class decompression_algorithm_under_test {
public:
	decompression_algorithm_under_test( const std::string &str ) :
			decompressed_image( )
	{
		compressed_output = str;
		std::cout << "compressed output length " << std::endl;
		decompressed_image = NULL;
	}
	~decompression_algorithm_under_test( )
	{
		if( !decompressed_image ) {
			delete decompressed_image;
			decompressed_image = NULL;
		}
	}

	void decode_header( );
	void read_cbs_to_square_map( );
	void decode_cbs( );

	uint8_t *decompress( );
	bool compare_cbs( const std::list< compression_block * > &list );

	void set_filename( std::string name )
	{
		filename = name;
	}
	const std::string &get_filename( )
	{
		return filename;
	}

protected:
	std::string filename;

	block_filtering_method bfiltering_method;
	bool separate_dict_compression;
	std::vector< compression_block > compression_blocks;
	std::vector< unsigned int > order_vector;

	unsigned int best_scale;
	entropy_coding coding;
	unsigned int num_of_cbs;
	std::string compressed_output;

	size_t pos;

	class image_data *decompressed_image;
};

#endif /* AUT_H_ */
