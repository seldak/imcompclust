# imcompclust #

Image compression using Clustering. I wrote this project for my Masters thesis, titled "Lossless Image Compression using Statistical Combinatorial Clustering". The code is still under development.

# Build

You will need Eigen3, boost and OpenCV to build. The following works on Debian Stretch.
```
sudo apt install libboost-all-dev libeigen3-dev libopencv-dev libopencv-contrib-dev libopencv-legacy-dev
```
And run `make`
```
make
```
Optionally if you want to use latest Eigen set 
```
EIGEN_INCPATH=/path/to/eigen make
```

# License
This project is licensed under the terms of the MIT license.

