
plotCRatioVsScale <- function( x, path, width, height, file, col ) {
  m <- vector()
  for( i in x ) {
    m <- append( m, min( read.table( sprintf( path, i ) ) ) )
  }
  size = width * height
  cr <- size / m
  #pdf( file,7, 5 )
  #plot( x, cr, type='line', xlab="block size (pixels)", ylab="compression ratio", col=col );
  d <- data.frame(x, cr)
  qplot( x, cr, data=d, geom=c("point", "smooth"),method="lm", formula=y~poly(x,6) ) 
  #points( x, cr, pch=0, col=col )
  #grid(col="gray")
  #dev.off( )
  #lines( x, m, col='red' ); points( x, m, pch=2, col='red' )
  #lines( x, m, col='red')
  cr
}

create_seq <- function( start, end ) {
  x <- vector( )
  for( i in seq( start, end ) ) { 
    x = append( x, i )
  }
  x
}

plotClusterVsScale <- function( x, path, file, col, append=FALSE ) {
  m <- vector()
  for( i in x ) {
    tab <- read.table( sprintf( path, i ) )
    m <- append( m, which.min( tab[,1] ) )
  }
  if( !append ) {
    pdf( file,7, 5 )
    plot( x, m, type='line', xlab="block size (pixels)", ylab="Best cluster", col=col )
  } else
    lines( x, m, col=col );
  points( x, m, pch=0, col=col )
  grid( col="gray" )
  #dev.off( )
  print(m)
}

plotLinkage <- function( x, path, file ) {
  m <- vector()
  for( i in x ) {
    tab <- read.table( sprintf( path, i ) )
    m <- append( m, which.min( tab[,1] ) )
  }
  if( !append ) {
    pdf( file,7, 5 )
    plot( x, m, type='line', xlab="block size (pixels)", ylab="Best cluster", col=col )
  } else
    lines( x, m, col=col );
  points( x, m, pch=0, col=col )
  grid( col="gray" )
  #dev.off( )
  print(m)
}

plotLinkagePlot <- function(){
  pdf( "linkage-plot.pdf",7, 5 )
  
  x<-seq(1,100)
  x2<-seq(1,100,3)
  plot( x, seats_ward[1:100,]*8, type='line', ylab ="L (bits)", xlab="clusters" )
  #b <- seats_ward[ seq(1, 100, 3) ]
  points( x2, seats_ward[ x2, ]*8, pch=0)

  lines( x, seats_average[1:100,]*8, col='red', lty=2 )
  points( x2, seats_average[ x2,]*8, col='red', pch=1 )

  lines( x, seats_complete[1:100,]*8, col='green', lty=3 )
  points( x2, seats_complete[ x2, ]*8, col='green', pch=2 )
  
  lines( x, seats_modified_average[1:100,]*8, col='blue', lty=4 )
  points( x2, seats_modified_average[ x2, ]*8, col='blue', pch=3 )
  
  lines( x, seats_centroid[1:100,]*8, col='violet', lty=5 )
  points( x2, seats_centroid[ x2, ]*8, col='violet', pch=4 )

  grid( col="black" )
  
  legend( x="bottomright", legend=c("AVG","COMP","CENT","D/N","MW"), lty=c(2,3,5,4,1),
          col = c('red','green','violet','blue','black'), pch = c(1,2,4,3,0) )

  dev.off()
  
  pdf( "linkage-plot-euc-jsd.pdf",7, 5 )
  
  x<-seq(1,100)
  x2<-seq(1,100,3)
  plot( x, seats_ward[1:100,]*8, type='line', ylab ="L (bits)", xlab="clusters" )
  #b <- seats_ward[ seq(1, 100, 3) ]
  points( x2, seats_ward[ x2, ]*8, pch=0)
  
  lines( x, seats_ward_sqeuc[1:100,]*8, col='orange', lty=3 )
  points( x2, seats_ward_sqeuc[ x2, ]*8, col='orange', pch=2 )
  
  lines( x, seats_modified_average[1:100,]*8, col='blue', lty=4 )
  points( x2, seats_modified_average[ x2, ]*8, col='blue', pch=3 )
  
  lines( x, seats_modified_average_sqeuc[1:100,]*8, col='magenta', lty=5 )
  points( x2, seats_modified_average_sqeuc[ x2, ]*8, col='magenta', pch=4 )
  
  grid( col="black" )
  
  legend( x="bottomright", legend=c("EUC D/N","EUC W","D/N","MW"), lty=c(5,3,4,1),
          col = c('magenta','orange','blue','black'), pch = c(4,2,3,0) )
  
  dev.off()
}
