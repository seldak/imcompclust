reset
set term png truecolor
set term epslatex
set output outfile

load 'grid.cfg'

set xrange [47:55]
set yrange [0:*]
set xlabel "Pixel intensity"
set ylabel "Count"
#set grid
set boxwidth 0.90 relative
set style fill transparent solid 0.9 noborder
plot infile u 1:2 w boxes lc rgb "midnight-blue" notitle
