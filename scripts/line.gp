reset
#set term png truecolor
set term pdf
set output outfile
#set xrange [0:*]
set xlabel "Scale"
set ylabel "Jensen-Shannon divergence"
set grid
set boxwidth 0.95 relative
set style fill transparent solid 0.9 noborder
plot infile u 1:2 with linespoints lc rgb"red" notitle
