reset
#set term png truecolor
set term pdf
set output outfile
set xrange [0:50]
set xlabel "Pixel intensity"
set ylabel "Count"
set grid
plot "filtered_left" u 1:2 smooth bezier title "Left", \
     "filtered_average" u 1:2 smooth bezier title "Average", \
     "filtered_paeth" u 1:2 smooth bezier title "Paeth"
