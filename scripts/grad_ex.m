A=randi( [50 52], 40, 20 );
B=randi( [70 72], 40, 20 );
C = [A B];
[gx, gy] = gradient(C);
s = 1:40;
zer = zeros(40);
figure;
hold on, grid on, quiver(s,s,gx,gy), quiver(s,s,gx,zer,'-r'), quiver(s,s,zer,gy,'-g'), hold off;

