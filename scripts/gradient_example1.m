v = -2:0.2:2;
[x,y] = meshgrid(v);
z = x .* exp(-x.^2 - y.^2);
[px,py] = gradient(z,.2,.2);
zer = zeros(21);
grid on, contour(v,v,z), hold on, quiver(v,v,px,py), quiver(v,v,px,zer,'-r'), quiver(v,v,zer,py,'-g'), hold off

