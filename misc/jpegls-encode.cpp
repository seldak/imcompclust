/*
 * jpeg-ls-encode.c
 *
 *  Created on: Apr 20, 2015
 *      Author: sameh
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <iostream>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <CharLS/interface.h>

#include <libgen.h>

static void copy_opencv_matrix_into_array( uint8_t *image,
		unsigned int nrows,
		unsigned int ncols,
		cv::Mat &input )
{

	unsigned int i;
	unsigned int j;

	std::cout << "rows = " << nrows << std::endl;
	std::cout << "cols = " << ncols << std::endl;

	for( i = 0; i < nrows; i++ ) {
		uint8_t *p = input.ptr< uint8_t >( i );
		for( j = 0; j < ncols; j++ ) {
			image[i * nrows + j] = p[j];
		}
	}
}

int main( int argc, char **argv )
{
	uint8_t *image = NULL;
	std::string in_filename;
	std::string out_filename;
	unsigned int nrows, ncols;
	int output_fd = -1;

	cv::Mat input;

	std::cout << "input: " << argv[1] << std::endl;
	in_filename = argv[1];

	input = cv::imread( in_filename, CV_LOAD_IMAGE_GRAYSCALE );
	nrows = input.rows;
	ncols = input.cols;

	uint8_t *uncompressed_input = static_cast< uint8_t * >( calloc( nrows * ncols, sizeof(uint8_t) ) );
	uint8_t *compressed_data = static_cast< uint8_t * >( calloc( nrows * ncols, sizeof(uint8_t) ) );
	size_t compressed_bytes = 0;
	JlsParameters parameters = { 0 };
	parameters.width = ncols;
	parameters.height = nrows;
	parameters.bitspersample = 8;
	parameters.components = 1;

	copy_opencv_matrix_into_array( uncompressed_input, nrows, ncols, input );

	enum JLS_ERROR error;
	error = JpegLsEncode( compressed_data, nrows * ncols, &compressed_bytes, uncompressed_input,
			nrows * ncols, &parameters );
	std::cout << "Return is " << error << std::endl;
	std::cout << "JPEG_LS length " << compressed_bytes << std::endl;

	std::string fName( in_filename );
	size_t pos = fName.rfind( "." );

	std::string outname = fName.substr(0, pos)  + ".jls";
	std::cout << "output filename " << outname << std::endl;
	output_fd = open( outname.c_str( ), O_WRONLY| O_CREAT, S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH );

	assert( output_fd != -1 );

	write( output_fd, compressed_data, compressed_bytes );

	close( output_fd );
	free( compressed_data );
	free( uncompressed_input );

}

